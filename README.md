```
.---.                               _            .-----.                         .-.
: .; :                             :_;           `-. .-'                         : :
:   .' .--. .-..-. .--. .--.  .--. .-.   _____     : : .--.  .--.  ,-.,-.,-.   _ : : .--. .--.  .--.  .--.
: :.`.' '_.': `; :' '_.': ..'`._-.': :  :_____:    : :' '_.'' .; ; : ,. ,. :  : :; :' .; :: ..'' .; :' '_.'
:_;:_;`.__.'`.__.'`.__.':_;  `.__.':_;             :_;`.__.'`.__,_;:_;:_;:_;  `.__.'`.__.':_;  `._. ;`.__.'
                                                                                                .-. :
                                                                                                `._.'
```

# Reversi

Reversi is a two-player strategy board game. It is played on a 8x8 board.
Both players have the same amount of stones available.
They can place a stone one by one. The rule for placing a stone is very simple:
If the newly placed stone together with another already placed stone of the
same player encloses one or more stones of the opponent, all their stones get
flipped. The player that has the most stones of his color on the board at the
end of the game wins. For detailed rules refer to the historical rules in 
[wikipedia](https://en.wikipedia.org/wiki/Reversi).

## Project

This is a Java Project, specifically implementing the Reversi game.
The model is based on a bit board implementation, resulting in better performance, especially for an AI doing many 
operations on the game field. The game has 3 modes in which it could be played:  
First, a hot seat mode where two players can play against each other on a single computer.  
Next, a single player mode where the user plays against an AI. Here, a player can choose between four levels of 
difficulty: BEGINNER, TRAINEE , ADVANCED and EXPERT, which are implemented as Random-AI, Reinforcement-Learning-AI, 
Mini-Max-AI and Alpha-Beta-Pruning-AI respectively. The RL-AI was trained with keras-rl, using Py4J for the connection 
between Java and Python. The current status of the RL-AI is comparable to a Random AI since many more iterations are 
needed for its training.  
The final mode is allows gaming over a network with sockets. The user then gets access to a game lobby with the option 
to create new games or join not yet started games. The server is started as a separate process and manages all games and 
clients.  
<img src="images/server.png" alt="The Server" width="400"/>  
The information is transferred using the JSON protocol.

## Definition of Done

* Mostly follows Google Java Style Guide
* SpotBugs
* Code Review
* Tested by the programmer himself / the programmers themselves
* Pass unittests
* Tested and Approved by another team member 
* Code Cleanup

## Dependencies

The project requires Java 11.
It was tested on Arch Linux Linux 5.4.10-arch1-1 no.1 SMP PREEMPT Thu, 09 Jan 2020 10:14:29 +0000 x86_64 GNU/Linux 
and Windows 10 and Mac OS Catalina 10.15.2 with openjdk 11.0.3 2019-04-16. 

The project is built with `gradle`, version 5.6.4. The provided `gradlew` wrapper automatically downloads and uses
the correct gradle version.

### RL-AI

For training and running the RL-AI, Python 3 is needed along with the following dependencies:
```
sudo pacman -S python-tensorflow // on ArchLinux pip3 install does not work since TF does not support Python 3.8 
while ArchLinux has the latest 3.8.1.
git clone https://github.com/wau/keras-rl2.git
cd keras-rl
python3 setup.py install
pip3 install tensorflow py4j
```

## Building the Project

On Linux and Mac OS, run the following command from the project's root directory to compile the program,
run all checks and create an executable jar:  
```
./gradlew build jar
```

On Windows, run the following command from the project's root directory to compile the program,
run all checks and create an executable jar:

```
./gradlew.bat build jar
```
If the command succeeds, the jar is found in `build/libs/reversi.jar`.
 
If you would only like to try out the game, download the latest version of the application which is found in the 
releases folder. Here you find 2 jar files be executed as follows:  
```
# Starting up the Server
 java -jar reversi-server.jar

# Starting a game
 java -jar reversi.jar
```

<img src="images/game_lobby.gif" alt="The Game Lobby" width="400"/>  

### RL-AI

The Python scripts are located in `src/main/python`.
The models are in `src/main/python/models`.

For running an AI game, run `python3 run_model.py`.
Then, after running the jar file you can select TRAINEE mode on the GUI and actually play against the AI. 
It is important to keep the Python script running in the background.

For training, build a jar with the Main-Class set to `reversi.model.ai.Entry` in the build.gradle.
Then, run the jar and `python3 main.py`. You can adjust any parameters you like, such as learning rates, 
the eps value or even change the model.

## Running the Program

To run the program during development without any checks, run `./gradlew run`.

## Game Play Demo
To play a game in Network mode, start up the server and then a new game. Follow the prompts and in the game lobby 
select a new game to start. 

<img src="images/network_game_play.gif" alt="Demo" width="600"/>

## Team Jorge

Team Jorge consists of Andreea, Aurelia, Felix, Klara and Sabine.


