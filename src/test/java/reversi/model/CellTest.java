package reversi.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CellTest {
  private Cell first = new Cell(0,0);
  private Cell second = new Cell(1,0);
  private Cell third = new Cell(0,0);
  private Cell fourth = first;

  @Test
  public void testEquals_true() {
    boolean actual = first.equals(third);
    Assertions.assertTrue(actual);
  }

  @Test
  public void testEquals_false() {
    boolean actual = first.equals(second);
    Assertions.assertFalse(actual);
  }

  @Test
  public void testEquals_sameObject() {
    boolean actual = first.equals(fourth);
    Assertions.assertTrue(actual);
  }

  @Test
  public void testEquals_OutOfBoundsCell() {

    Cell cell = new Cell(-1, 0);
    boolean actual = first.equals(cell);
    Assertions.assertFalse(actual);
  }

}
