package reversi.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Test class for BitBoardState.
 */
public class BitBoardStateTest {
  /**
   * Checks whether a player has been switched correctly.
   *
   * @param state The state instance to test on.
   * @param want The expected player after switching.
   * @param player The player to get switched.
   */
  private void helpSwitchPlayer(BitBoardState state, Player want, Player player) {
    state.setCurrentPlayer(player);
    state.switchPlayer();
    Player have = state.getCurrentPlayer();

    assertTrue(want == have);
  }

  @Test
  public void testSwitchPlayer() {
    BitBoardState state = new BitBoardState();
    
    helpSwitchPlayer(state, Player.BLACK, Player.WHITE);
    
    helpSwitchPlayer(state, Player.WHITE, Player.BLACK);
  }

  /**
   * Checks whether the winner got updated correctly.
   *
   * @param state The state instance to test on.
   * @param phase The phase that the game is in.
   * @param want The expected player to be the winner.
   * @param bitsW The white board encoded as a long.
   * @param bitsB The black board encoded as a long.
   */
  private void helpUpdateWinner(BitBoardState state, Phase phase,
                                Player want, long bitsW, long bitsB) {
    state.getField().setBitsW(bitsW);
    state.getField().setBitsB(bitsB);

    state.setCurrentPhase(phase);
    if (phase == Phase.FINISHED) {
      state.updateWinner();

      Player have = state.getWinner().orElse(null);
      assertTrue(want == have);
    } else {
      assertThrows(IllegalStateException.class, state::updateWinner);
    }
  }

  @Test
  public void testUpdateWinner() {
    BitBoardState state = new BitBoardState();

    helpUpdateWinner(state, Phase.FINISHED, Player.WHITE,
            0b11110111_10001011_10110111_11101011_11101101_11010100_11111100_11111111L,
            0b00001000_01110100_01001000_00010100_00010010_00101011_00000011_00000000L);

    helpUpdateWinner(state, Phase.FINISHED, Player.BLACK,
            0b01000010_01100000_01110000_00010100_00111000_01011100_00101000_00000111L,
            0b10111101_10011111_10001111_11101011_11000111_10100011_11010111_11111000L);

    helpUpdateWinner(state, Phase.RUNNING, Player.WHITE,
        0b11110111_10001011_10110111_11101011_11101101_11010100_11111100_11111111L,
        0b00001000_01110100_01001000_00010100_00010010_00101011_00000011_00000000L);

    helpUpdateWinner(state, Phase.FINISHED, null,
        0b01111110_00111101_00011101_00001101_00000101_00010001_00010001_11111111L,
        0b10000001_11000010_11100010_11110010_11111010_11101110_11101110_00000000L);
  }

  @Test
  public void testGetWinner_ExpectedException() {
    BitBoardState state = new BitBoardState();
    state.setCurrentPhase(Phase.RUNNING);

    Assertions.assertThrows(IllegalStateException.class, state::getWinner);
  }

  private void helpGetAllCellsOfPlayer(BitBoardState state, Player player,
                                       int expectedCellSize, long bitsW, long bitsB) {
    state.getField().setBitsW(bitsW);
    state.getField().setBitsB(bitsB);

    int actualCellSize = state.getAllCellsOfPlayer(player).size();
    assertEquals(expectedCellSize, actualCellSize);
  }

  @Test
  public void testGetAllCellsOfPlayer() {
    BitBoardState state = new BitBoardState();

    helpGetAllCellsOfPlayer(state, Player.WHITE, 0,
        0L,
        0L);

    helpGetAllCellsOfPlayer(state, Player.BLACK, 0,
        0L,
        0L);

    helpGetAllCellsOfPlayer(state, Player.BLACK,43,
        0b01000010_01100000_01110000_00010100_00111000_01011100_00101000_00000111L,
        0b10111101_10011111_10001111_11101011_11000111_10100011_11010111_11111000L);
  }
}
