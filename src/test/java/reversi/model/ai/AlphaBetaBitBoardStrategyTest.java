package reversi.model.ai;

import java.time.Duration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import reversi.model.BitBoardState;
import reversi.model.Cell;
import reversi.model.Player;


public class AlphaBetaBitBoardStrategyTest {

  private AlphaBetaBitBoardStrategy strategy = new AlphaBetaBitBoardStrategy();
  private BitBoardState state = new BitBoardState();
  private long bitsB = 0L;
  private long bitsW = 0L;

  private void helpSetUpBoard() {
    state.getField().setBitsW(bitsW);
    state.getField().setBitsB(bitsB);
    state.setCurrentPlayer(Player.WHITE);
  }

  @Test
  public void testCalculateMove_openingMove() {
    bitsB = 0b00000000_00000000_00000000_00010000_00000000_00000000_00000000_00000000L;
    helpSetUpBoard();

    Cell actual = strategy.calculateMove(state);
    Assertions.assertEquals(new Cell(4,3), actual);
  }

  @Test
  public void testCalculateMove_MiddlePhaseMove() {
    bitsW = 0b00000000_00000000_00110000_00110010_01001110_10101100_00000000_00000000L;
    bitsB = 0b00000000_00000000_00000100_01001000_00110000_01010010_00111000_00010000L;

    helpSetUpBoard();

    Cell actual = strategy.calculateMove(state);
    Assertions.assertEquals(new Cell(2,7), actual);
  }

  @Test
  public void testCalculateMove_EndGame() {
    bitsW = 0b10000000_00000000_00110000_00111000_00011100_00111000_00011100_10111111L;
    bitsB = 0b01111000_01110101_11001111_11000111_11100011_11000111_11100010_01000000L;
    helpSetUpBoard();

    Cell actual = strategy.calculateMove(state);
    Assertions.assertEquals(new Cell(4,1), actual);
  }

  @Test
  public void testCalculateMove_OpeningPhasePerformance() {
    bitsB = 0b00000000_00000000_00000000_00010000_00000000_00000000_00000000_00000000L;
    helpSetUpBoard();

    Assertions.assertTimeout(Duration.ofMillis(300), () -> {
      strategy.calculateMove(state);
    });
  }

  @Test
  public void testCalculateMove_Performance() {
    bitsW = 0b00000000_00000000_00110000_00110010_01001110_10101100_00000000_00000000L;
    bitsB = 0b00000000_00000000_00000100_01001000_00110000_01010010_00111000_00010000L;

    helpSetUpBoard();

    Assertions.assertTimeout(Duration.ofMillis(30000), () -> {
      strategy.calculateMove(state);
    });
  }
}
