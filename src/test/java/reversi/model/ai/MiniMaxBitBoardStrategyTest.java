package reversi.model.ai;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import reversi.model.BitBoardState;
import reversi.model.Cell;
import reversi.model.Phase;
import reversi.model.Player;

class MiniMaxBitBoardStrategyTest {

  private MiniMaxBitBoardStrategy randomStrategy  = new MiniMaxBitBoardStrategy(4, true);
  private MiniMaxBitBoardStrategy strategy = new MiniMaxBitBoardStrategy(1, false);
  private BitBoardState state = new BitBoardState();
  private long bitsB = 0L;
  private long bitsW = 0L;

  /** A helper method that helps to set up the board. */
  private void helpSetUpBoard() {
    state.getField().setBitsW(bitsW);
    state.getField().setBitsB(bitsB);
    state.setCurrentPlayer(Player.WHITE);
  }

  @Test
  public void testMiniMaxBitBoardStrategy_negativeLookAhead() {

    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      new MiniMaxBitBoardStrategy(-2, true);
    });
  }

  @Test
  public void testCalculateMove_nonBitBoardState() {

    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      strategy.calculateMove(null);
    });
  }

  @Test
  public void testCalculateMove_gameEnded() {
    state.setCurrentPhase(Phase.FINISHED);

    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      strategy.calculateMove(state);
    });
  }

  @Test
  public void testCalculateMove_AiMoveBeforeHumanMove() {
    state.setCurrentPlayer(Player.BLACK);

    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      strategy.calculateMove(state);
    });
  }

  @Test
  public void testCalculateMove_openingMoveDeterministicMiniMax() {
    bitsB = 0b00000000_00000000_00000000_00010000_00000000_00000000_00000000_00000000L;
    helpSetUpBoard();

    Cell actual = strategy.calculateMove(state);
    Assertions.assertEquals(new Cell(4,3), actual);
  }

  @Test
  public void testCalculateMove_openingMoveRandomizedMiniMax() {
    bitsB = 0b00000000_00000000_00000000_00010000_00000000_00000000_00000000_00000000L;
    helpSetUpBoard();

    List<Cell> expected = new ArrayList<>();
    expected.add(new Cell(3,4));
    expected.add(new Cell(4,3));
    expected.add(new Cell(4,4));

    MiniMaxBitBoardStrategy strategy = new MiniMaxBitBoardStrategy(1, true);
    Cell actual = strategy.calculateMove(state);
    assertThat(actual).isIn(expected);

  }

  @Test
  public void testCalculateMove_MiddlePhaseMove() {
    bitsW = 0b00000000_00000000_00110000_00110010_01001110_10101100_00000000_00000000L;
    bitsB = 0b00000000_00000000_00000100_01001000_00110000_01010010_00111000_00010000L;
    helpSetUpBoard();

    Cell actual = randomStrategy.calculateMove(state);
    Assertions.assertEquals(new Cell(2, 7), actual);
  }

  @Test
  public void testCalculateMove_EndGame() {
    bitsW = 0b00011000_00011000_00100100_00011100_00011000_00111100_01000000_10111000L;
    bitsB = 0b01100110_01100110_11011011_11100011_11100111_11000011_00111101_00000100L;
    helpSetUpBoard();

    Cell actual = randomStrategy.calculateMove(state);
    Assertions.assertEquals(new Cell(0,0), actual);
  }

  @Test
  public void testCalculateMove_OpeningPerformance() {
    bitsB = 0b00000000_00000000_00000000_00010000_00000000_00000000_00000000_00000000L;
    helpSetUpBoard();

    Assertions.assertTimeout(Duration.ofMillis(300), () -> {
      randomStrategy.calculateMove(state);
    });
  }

  @Test
  public void testCalculateMove_Performance() {
    bitsW = 0b00000000_00000000_00110000_00110010_01001110_10101100_00000000_00000000L;
    bitsB = 0b00000000_00000000_00000100_01001000_00110000_01010010_00111000_00010000L;
    helpSetUpBoard();

    Assertions.assertTimeout(Duration.ofMillis(1000), () -> {
      randomStrategy.calculateMove(state);
    });
  }
}