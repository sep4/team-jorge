package reversi.model.ai;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import reversi.model.BitBoardState;
import reversi.model.Phase;
import reversi.model.Player;

class WinVelocityAssessorTest {

  private WinVelocityAssessor assessor = new WinVelocityAssessor();
  private BitBoardState state = Mockito.mock(BitBoardState.class);

  /**
   * A helper method that sets up the data to be tested.
   *
   * @param player The Player that has just won the game.
   */
  private void helpTestComputeValue(Player player) {
    Mockito.when(state.getCurrentPhase()).thenReturn(Phase.FINISHED);
    Mockito.when(state.getWinner()).thenReturn(Optional.ofNullable(player));
  }

  @Test
  void testComputeValue_noWinner() {
    helpTestComputeValue(null);

    double actual = assessor.computeValue(state, 1);
    assertEquals(5000.0, actual);
  }

  @Test
  void testComputeValue_HumanPlayerWins() {
    helpTestComputeValue(Player.BLACK);

    double actual = assessor.computeValue(state, 1);
    assertEquals(-15000.0, actual);
  }

  @Test
  void testComputeValue_AiWins() {
    helpTestComputeValue(Player.WHITE);

    double actual = assessor.computeValue(state, 1);
    assertEquals(10000.0, actual);
  }

  @Test
  void testComputeValue_WinIn3Moves() {
    helpTestComputeValue(Player.WHITE);

    double actual = assessor.computeValue(state, 3);
    assertEquals(10000.0 / 3, actual);
  }
}