package reversi.model.ai;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import reversi.model.BitBoard;
import reversi.model.BitBoardState;

class WeightedCellsAssessorTest {
  private WeightedCellsAssessor assessor = new WeightedCellsAssessor();
  private BitBoard bitBoard = new BitBoard();
  private BitBoardState stateMock = Mockito.mock(BitBoardState.class);

  private void helpTestComputeValue(long bitsW, long bitsB) {
    bitBoard.setBitsB(bitsB);
    bitBoard.setBitsW(bitsW);
    Mockito.when(stateMock.getField()).thenReturn(bitBoard);
  }

  @Test
  void testComputeValue_emptyBoard() {
    helpTestComputeValue(0L, 0L);

    double actual = assessor.computeValue(stateMock, 1);
    assertEquals(0.0, actual);
  }

  @Test
  void testComputeValue_cornerCells() {
    helpTestComputeValue(
        0b10000001_00000000_00000000_00011000_00010000_00010000_00000000_00000000L,
        0b00000000_00000000_00000000_00000000_00001000_00000000_00000000_00000001L);

    double actual = assessor.computeValue(stateMock, 1);
    assertEquals(50.0, actual);
  }

  @Test
  void testComputeValue_XCells() {
    helpTestComputeValue(
        0b00000000_01000010_00000000_00011000_00010000_00010000_00000000_00000000L,
        0b00000000_00000010_00000000_00000000_00001000_00000000_00000000_00000000L);

    double actual = assessor.computeValue(stateMock, 1);
    assertEquals(-10.0, actual);
  }

  @Test
  void testComputeValue_CCells() {
    helpTestComputeValue(
        0b01000010_01000010_00000000_00011000_00010000_00010000_00000000_00000000L,
        0b00000000_00000010_00000000_00000000_00001000_00000000_00000000_00000000L);

    double actual = assessor.computeValue(stateMock, 1);
    assertEquals(-90.0, actual);
  }

  @Test
  void testComputeValue_XandCandCornerCells() {
    helpTestComputeValue(
        0b00100000_00010100_00001000_01001000_10001000_00000100_00000000_00000001L,
        0b00000000_01000001_00100010_00010100_00010000_00111000_00000111_00000000L);

    double actual = assessor.computeValue(stateMock, 1);
    assertEquals(280.0, actual);
  }
}
