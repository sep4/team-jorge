package reversi.model.ai;

import static org.junit.jupiter.api.Assertions.assertTrue;

import static reversi.model.ai.PlayBitBoardAlphaBetaTest.getNextMoveForHumanPlayer;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import reversi.model.Cell;
import reversi.model.Phase;
import reversi.model.Player;

/**
 * Test class for a reversi game using the {@link reversi.model.ai.MiniMaxBitBoardStrategy} as AI.
 */
public class PlayBitBoardMiniMaxTest {

  /**
   * Checks whether the {@link BitBoardAI model} works correctly using the {@link
   * MiniMaxBitBoardStrategy MiniMax-algorithm} in its deterministic form with an integration test.
   */
  @Test
  public void testMiniMax_deterministic() {
    BitBoardAI reversi = new BitBoardAI(new MiniMaxBitBoardStrategy(3, false));

    /*
     * Integration test, a reversi game is played here in singleplayer mode with the
     * MiniMax-algorithm.
     */
    reversi.place(new Cell(3, 3));

    for (int i = 0; i < 30; i++) {
      reversi.place(getNextMoveForHumanPlayer(reversi.getState()));

      if (reversi.getState().getCurrentPhase() == Phase.FINISHED) {
        Optional<Player> winner = reversi.getState().getWinner();
        assertTrue(winner.isPresent() && winner.get().equals(Player.WHITE));
      }
    }

    long wantBitsW = 0b1111111110011111100011111110110111010001101110011000000010000000L;
    long wantBitsB = 0b0000000001100000011100000001001000101110010001100111111000000000L;

    assertTrue(reversi.getState().getField().getBitsW() == wantBitsW);
    assertTrue(reversi.getState().getField().getBitsB() == wantBitsB);
  }

  /**
   * Checks whether the {@link BitBoardAI model} works correctly using the {@link
   * MiniMaxBitBoardStrategy MiniMax-algorithm} in its randomized form with an integration test.
   */
  @Test
  public void testMiniMax_randomized() {
    BitBoardAI reversi = new BitBoardAI(new MiniMaxBitBoardStrategy(4, true));

    /*
     * Integration test, a reversi game is played here in singleplayer mode with the
     * MiniMax-algorithm.
     */
    reversi.place(new Cell(3, 3));

    for (int i = 0; i < 30; i++) {
      reversi.place(getNextMoveForHumanPlayer(reversi.getState()));
    }

    /* The game definitely ends within 32 human moves as a player has only 32 stones to push.
     * Would the AI-algorithm suggest an invalid move, BitBoardAI would throw an exception and this
     * test would fail.
     */
    assertTrue(reversi.getState().getCurrentPhase() == Phase.FINISHED);
  }
}

