package reversi.model.ai;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import reversi.model.BitBoard;
import reversi.model.BitBoardState;
import reversi.model.Cell;
import reversi.model.Phase;
import reversi.model.Player;

/** Test class for a reversi game with alpha-beta-pruning as AI. */
public class PlayBitBoardAlphaBetaTest {
  /** Checks whether reversi model works correctly. */
  @Test
  public void testAlphaBetaPruning() {
    BitBoardAI reversi = new BitBoardAI(new AlphaBetaBitBoardStrategy());

    /*
     * Integration Test, a reversi game is played here
     */
    reversi.place(new Cell(4, 3));

    for (int i = 0; i < 30; ++i) {
      reversi.place(getNextMoveForHumanPlayer(reversi.getState()));

      if (reversi.getState().getCurrentPhase() == Phase.FINISHED) {
        Optional<Player> winner = reversi.getState().getWinner();
        if (winner.isPresent()) {
          assertTrue(winner.get().equals(Player.WHITE));
        }
      }
    }

    long wantBitsW = 0b1111111100111111001111110011110101101001001101010001111010011101L;
    long wantBitsB = 0b11000000010000001100001010010110110010101110000101100000L;

    assertTrue(reversi.getState().getField().getBitsW() == wantBitsW);
    assertTrue(reversi.getState().getField().getBitsB() == wantBitsB);
  }

  /**
   * Returns a possible move for the current player of the given game state, if there is one. If a
   * non-null Cell is returned, it is always the one with the smallest index.
   *
   * @param state The current game state.
   * @return The possible move with the smallest index, if there is one, otherwise null.
   */
  static Cell getNextMoveForHumanPlayer(BitBoardState state) {
    long moves = state.getField().getMoves(state.getCurrentPlayer());
    Cell nextMove = null;

    for (int i = 0; i < Math.pow(BitBoard.REQUIRED_SIZE, 2); ++i) {
      if (state.getField().getBit(moves, i) == 1) {
        nextMove = BitBoard.getCellAtIndex(i);
        break;
      }
    }
    return nextMove;
  }
}
