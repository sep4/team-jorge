package reversi.model.ai;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static reversi.model.ai.PlayBitBoardAlphaBetaTest.getNextMoveForHumanPlayer;

import org.junit.jupiter.api.Test;

import reversi.model.Cell;
import reversi.model.Phase;

public class PlayBitBoardRandomStrategyTest {

  @Test
  public void testRandomStrategy() {
    BitBoardAI reversi = new BitBoardAI(new RandomBitBoardStrategy());

    /*
     * Integration test, a reversi game is played here in single player mode with the
     * Random Strategy AI.
     */
    reversi.place(new Cell(3, 3));

    for (int i = 0; i < 31; i++) {
      reversi.place(getNextMoveForHumanPlayer(reversi.getState()));
    }

    /* The game definitely ends within 32 human moves as a player has only 32 stones to push.
     * Would the AI-algorithm suggest an invalid move, BitBoardAI would throw an exception and this
     * test would fail.
     */
    assertTrue(reversi.getState().getCurrentPhase() == Phase.FINISHED);
  }

}
