package reversi.model.ai;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import reversi.model.BitBoard;
import reversi.model.BitBoardState;

class MobilityAssessorTest {

  private MobilityAssessor assessor = new MobilityAssessor();
  private BitBoard bitBoard = new BitBoard();
  private BitBoardState stateMock = Mockito.mock(BitBoardState.class);
  private int depth = 1;

  /**
   * A helper method that sets up the data to be tested.
   *
   * @param bitsB stones belonging to Black encoded as a long.
   * @param bitsW stones belonging to Black encoded as a long.
   */
  void helpTestComputeValue(long bitsW, long bitsB) {
    bitBoard.setBitsB(bitsB);
    bitBoard.setBitsW(bitsW);
    Mockito.when(stateMock.getField()).thenReturn(bitBoard);
  }

  @Test
  void testComputeValue_emptyBoard() {
    Mockito.when(stateMock.getField()).thenReturn(bitBoard);

    double actual = assessor.computeValue(stateMock, depth);
    assertEquals(-2.0, actual);
  }

  @Test
  void testComputeValue_basicScenario() {
    helpTestComputeValue(
        0b00000000_00000000_00000000_00011000_00010000_00010000_00000000_00000000L,
        0b00000000_00000000_00000000_00000000_00001000_00000000_00000000_00000000L
    );

    double actual = assessor.computeValue(stateMock, depth);
    assertEquals(-2.0, actual);
  }

  @Test
  void testComputeValue_whiteHasAdvantage() {
    helpTestComputeValue(
        0b00000000_00000000_00111000_00000100_00000000_00000100_00000000_00000000L,
        0b00000000_00110000_00000000_00111000_00111100_00000000_00000000_00000000L
    );

    double actual = assessor.computeValue(stateMock, depth);
    assertEquals(-4.0, actual);
  }

  @Test
  void testComputeValue_blackHasAdvantage() {
    helpTestComputeValue(
        0b01111110_00111100_11011010_11011110_11000100_0110100_00100100_00001000L,
        0b00000000_00000000_00100100_00100000_00111000_00001000_00011000_00001000L
    );

    double actual = assessor.computeValue(stateMock, depth);
    assertEquals(-18.5, actual);
  }

  @Test
  void testComputeValue_endGame() {
    helpTestComputeValue(
        0b01111110_01111100_01111110_00111110_00011110_00011110_00010100_11111100L,
        0b10000001_10000011_10000001_11000001_11100001_11100001_11101001_00000001L
    );

    double actual = assessor.computeValue(stateMock, depth);
    assertEquals(-3.0, actual);
  }
}