package reversi.model.ai;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import reversi.model.BitBoard;
import reversi.model.BitBoardState;

class StoneCountAssessorTest {

  private StoneCountAssessor assessor = new StoneCountAssessor();
  private BitBoard bitBoard = new BitBoard();
  private BitBoardState stateMock = Mockito.mock(BitBoardState.class);

  private void helpTestComputeValue(long bitsW, long bitsB) {
    bitBoard.setBitsB(bitsB);
    bitBoard.setBitsW(bitsW);
    Mockito.when(stateMock.getField()).thenReturn(bitBoard);
  }

  @Test
  void testComputeValue_basicScenario() {
    helpTestComputeValue(
        0b00000000_00000000_00000000_00000000_00001100_00000000_00000000_00000000L,
        0b00000000_00000000_00010000_00011000_00010000_00000000_00000000_00000000L);

    double actual = assessor.computeValue(stateMock, 1);
    assertEquals(-4.0, actual);
  }

  @Test
  void testComputeValue_emptyBoard() {
    helpTestComputeValue(0L,0L);

    double actual = assessor.computeValue(stateMock, 1);
    assertEquals(0.0, actual);
  }
}