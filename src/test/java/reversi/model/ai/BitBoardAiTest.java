package reversi.model.ai;

import static org.mockito.ArgumentMatchers.any;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import reversi.model.Cell;

class BitBoardAiTest {

  private Strategy strategyMock = Mockito.mock(MiniMaxBitBoardStrategy.class);
  private BitBoardAI bitBoardAI = new BitBoardAI(strategyMock);

  @Test
  void testPlace_illegalAiMove() {
    Mockito.when(strategyMock.calculateMove(any())).thenReturn(new Cell(0,0));

    Assertions.assertThrows(IllegalStateException.class, () -> {
      bitBoardAI.place(new Cell(4,3));
    });
  }

  @Test
  void testPlace_validOpeningAiMove() {
    Mockito.when(strategyMock.calculateMove(any())).thenReturn(new Cell(3,3));
    boolean actual = bitBoardAI.place(new Cell(4,3));

    Assertions.assertTrue(actual);
  }

  @Test
  void testPlace_illegalHumanMove() {
    Cell cellOutside = new Cell(8,3);

    boolean actual = bitBoardAI.place(cellOutside);
    Assertions.assertFalse(actual);
  }

}
