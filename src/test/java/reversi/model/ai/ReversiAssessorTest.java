package reversi.model.ai;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import reversi.model.BitBoard;
import reversi.model.BitBoardState;

class ReversiAssessorTest {
  private BitBoard bitBoard = new BitBoard();
  private BitBoardState stateMock = Mockito.mock(BitBoardState.class);

  private ReversiAssessor assessor = new ReversiAssessor();
  private int depth = 1;

  /**
   * A helper method that sets up the data to be tested.
   *
   * @param bitsW stones belonging to Black encoded as a long.
   * @param bitsB stones belonging to Black encoded as a long.
   */
  private void helpTestComputeValue(long bitsW, long bitsB) {
    bitBoard.setBitsB(bitsB);
    bitBoard.setBitsW(bitsW);
    Mockito.when(stateMock.getField()).thenReturn(bitBoard);
  }

  @Test
  void testComputeValue_emptyBoard() {
    helpTestComputeValue(0L,0L);
    double actual = assessor.computeValue(stateMock, depth);

    assertEquals(-2.0, actual);
  }

  @Test
  void testComputeValue_basicScenario() {
    helpTestComputeValue(
        0b00100000_00010100_00001000_01001000_10001000_00000100_00000000_00000001L,
        0b00000000_01000001_00100010_00010100_00010000_00111000_00000111_00000000L);

    double actual = assessor.computeValue(stateMock, depth);
    assertEquals(274.5, actual);
  }
}
