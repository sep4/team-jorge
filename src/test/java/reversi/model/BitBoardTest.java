package reversi.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;

/**
 * Test class for BitBoard.
 */
public class BitBoardTest {
  
  /**
   * Checks whether a shift function in the function table shifts a long correctly.
   *
   * @param bitBoard The bitBoard instance to test on.
   * @param input A board encoded as a long.
   * @param want The expected board encoded as a long after shifting.
   * @param direction The direction for which to get the corresponding shift function.
   */
  private void helpShiftTable(BitBoard bitBoard, long input, long want, Direction direction) {
    long have = bitBoard.shiftTable.get(direction).call(input);
    
    assertTrue(want == have);
  }

  @Test
  public void testShiftTable() {
    BitBoard bitBoard = new BitBoard();
    
    helpShiftTable(bitBoard,
            0b00010000_10001000_01110000_00000011_10001001_00000000_11110000_11111111L,
            0b10001000_01110000_00000011_10001001_00000000_11110000_11111111_00000000L, 
            Direction.N);
    
    helpShiftTable(bitBoard,
            0b00111111_11100000_00011100_00000011_00001100_00110000_11000000_10000000L,
            0b11100000_00011100_00000011_00001100_00110000_11000000_10000000_00000000L,
            Direction.N);

    helpShiftTable(bitBoard,
            0b00010000_10001000_01110000_00000011_10001001_00000000_11110000_11111111L,
            0b00000000_00010000_10001000_01110000_00000011_10001001_00000000_11110000L,
            Direction.S);
    
    helpShiftTable(bitBoard,
            0b00111111_11100000_00011100_00000011_00001100_00110000_11000000_10000000L,
            0b00000000_00111111_11100000_00011100_00000011_00001100_00110000_11000000L,
            Direction.S);

    helpShiftTable(bitBoard,
            0b10010001_11111011_00011001_10100011_01000001_10110010_10101010_10001001L,
            0b01001000_01111101_00001100_01010001_00100000_01011001_01010101_01000100L,
            Direction.E);

    helpShiftTable(bitBoard,
            0b10010001_11111011_00011001_10100011_01000001_10110010_10101010_10001001L,
            0b00100010_11110110_00110010_01000110_10000010_01100100_01010100_00010010L,
            Direction.W);

    helpShiftTable(bitBoard,
            0b10111001_01000110_10101010_00010001_10010010_00001000_11110111_00111000L,
            0b00100011_01010101_00001000_01001001_00000100_01111011_00011100_00000000L,
            Direction.NE);

    helpShiftTable(bitBoard,
            0b10111001_01000110_10101010_00010001_10010010_00001000_11110111_00111000L,
            0b10001100_01010100_00100010_00100100_00010000_11101110_01110000_00000000L,
            Direction.NW);

    helpShiftTable(bitBoard,
            0b10110110_01001001_10000001_01110011_01010101_10001000_10100011_01111100L,
            0b00000000_01011011_00100100_01000000_00111001_00101010_01000100_01010001L,
            Direction.SE);

    helpShiftTable(bitBoard,
            0b10110110_01001001_10000001_01110011_01010101_10001000_10100011_01111100L,
            0b00000000_01101100_10010010_00000010_11100110_10101010_00010000_01000110L,
            Direction.SW);
  }

  /**
   * Checks whether an index is got correctly.
   *
   * @param want The expected number of the index.
   * @param cell The cell to get the index for.
   */
  private void helpGetIndexOfCell(int want, Cell cell) {
    int have = BitBoard.getIndexOfCell(cell);
    assertTrue(want == have);
  }

  @Test
  public void testGetIndexOfCell() {
    helpGetIndexOfCell(10, new Cell(2, 1));
    
    helpGetIndexOfCell(29, new Cell(5, 3));
    
    helpGetIndexOfCell(56, new Cell(0, 7));
    
    helpGetIndexOfCell(47, new Cell(7, 5));
    
    helpGetIndexOfCell(4, new Cell(4, 0));
    
    helpGetIndexOfCell(62, new Cell(6, 7));
    
    helpGetIndexOfCell(16, new Cell(0, 2));
    
    helpGetIndexOfCell(0, new Cell(0, 0));
    
    helpGetIndexOfCell(7, new Cell(7, 0));
    
    helpGetIndexOfCell(63, new Cell(7, 7));
  }  

  /**
   * Checks whether an index is got correctly.
   *
   * @param want The expected cell for the index..
   * @param index The ind exto get the cell for.
   */
  private void helpGetCellAtIndex(Cell want, int index) {
    Cell have = BitBoard.getCellAtIndex(index);
    assertTrue(want.equals(have));
  }

  @Test
  public void testGetCellAtIndex() {
    helpGetCellAtIndex(new Cell(2, 1), 10);
    
    helpGetCellAtIndex(new Cell(5, 3), 29);
    
    helpGetCellAtIndex(new Cell(0, 7), 56);
    
    helpGetCellAtIndex(new Cell(7, 5), 47);
    
    helpGetCellAtIndex(new Cell(4, 0), 4);
    
    helpGetCellAtIndex(new Cell(6, 7), 62);
    
    helpGetCellAtIndex(new Cell(0, 2), 16);
    
    helpGetCellAtIndex(new Cell(0, 0), 0);
    
    helpGetCellAtIndex(new Cell(7, 0), 7);
    
    helpGetCellAtIndex(new Cell(7, 7), 63);
  }  

  /**
   * Checks whether the possible moves in one direction are found correctly.
   *
   * @param bitBoard The bitBoard instance to test on.
   * @param want The expected moves encoded as a long.
   * @param bitsP The board of the current player encoded as a long.
   * @param bitsO The board of the opponent player encoded as a long.
   * @param direction The direction for which to get the moves.
   */
  private void helpLineCapMoves(
          BitBoard bitBoard, long want, long bitsP, long bitsO, Direction direction) {
    long have = bitBoard.lineCapMoves(bitsP, bitsO, direction);
    
    assertTrue(want == have);
  }

  @Test
  public void testLineCapMoves() {
    BitBoard bitBoard = new BitBoard();
    
    helpLineCapMoves(bitBoard,
            0b00000000_00000000_00001000_00000000_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_00000000_00001000_00000000_00000000_00000000L,
            0b00000000_00000000_00010000_00011000_00010000_00000000_00000000_00000000L,
            Direction.N);

    helpLineCapMoves(bitBoard,
            0b00000000_00000000_00000000_00000000_00100000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_00000000_00001000_00000000_00000000_00000000L,
            0b00000000_00000000_00010000_00011000_00010000_00000000_00000000_00000000L,
            Direction.W);
    
    helpLineCapMoves(bitBoard,
            0b00000000_00000000_00100000_00000000_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_00000000_00001000_00000000_00000000_00000000L,
            0b00000000_00000000_00010000_00011000_00010000_00000000_00000000_00000000L,
            Direction.NW);
    
    helpLineCapMoves(bitBoard,
            0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_00000000_00001000_00000000_00000000_00000000L,
            0b00000000_00000000_00010000_00011000_00010000_00000000_00000000_00000000L,
            Direction.E);

    helpLineCapMoves(bitBoard,
            0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00010000_00011000_00010000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_00000000_00001000_00000000_00000000_00000000L,
            Direction.N);
    
    helpLineCapMoves(bitBoard,
            0b00000000_00000000_00000000_00000000_00000100_00000000_00000000_00000000L,
            0b00000000_00000000_00010000_00011000_00010000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_00000000_00001000_00000000_00000000_00000000L,
            Direction.E);
    
    helpLineCapMoves(bitBoard,
            0b00000000_00000000_00000000_00000000_00000000_00001000_00000000_00000000L,
            0b00000000_00000000_00010000_00011000_00010000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_00000000_00001000_00000000_00000000_00000000L,
            Direction.S);
    
    helpLineCapMoves(bitBoard,
            0b00000000_00000000_00000000_00000000_00000000_00000100_00000000_00000000L,
            0b00000000_00000000_00010000_00011000_00010000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_00000000_00001000_00000000_00000000_00000000L,
            Direction.SE);
  }

  /**
   * Checks whether all possible moves in all directions are found correctly.
   *
   * @param bitBoard The bitBoard instance to test on.
   * @param want The expected board encoded as a long.
   * @param bitsW The board of the white player encoded as a long.
   * @param bitsB The board of the black player encoded as a long.
   * @param player The current Player to get the moves for.
   */
  private void helpGetMoves(BitBoard bitBoard, long want, long bitsW, long bitsB, Player player) {
    bitBoard.setBitsW(bitsW);
    bitBoard.setBitsB(bitsB);
    
    long have = bitBoard.getMoves(player);
    
    assertTrue(want == have);
  }

  @Test
  public void testGetMoves() {
    BitBoard bitBoard = new BitBoard();
    
    helpGetMoves(bitBoard, 
            0b00000000_00100000_00101000_00000000_00100000_00000000_00000000_00000000L, 
            0b00000000_00000000_00000000_00000000_00001100_00000000_00000000_00000000L,
            0b00000000_00000000_00010000_00011000_00010000_00000000_00000000_00000000L,
            Player.WHITE);
    
    helpGetMoves(bitBoard,
            0b00000000_00000000_00000000_00000000_00000010_00001110_00000000_00000000L,
            0b00000000_00000000_00000000_00000000_00001100_00000000_00000000_00000000L,
            0b00000000_00000000_00010000_00011000_00010000_00000000_00000000_00000000L,
            Player.BLACK);

    helpGetMoves(bitBoard,
           0b00000000_00000000_00000000_00011000_00000000_00000000_00000000_00000000L,
           0b00000000_00000000_00000000_00000000_00001000_00000000_00000000_00000000L,
           0b00000000_00000000_00000000_00000000_00010000_00000000_00000000_00000000L,
           Player.BLACK);

    helpGetMoves(bitBoard,
            0b00000000_00000000_00000000_00011000_00011000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000L,
            Player.BLACK);
  }

  /**
   * Checks whether a bit at an index is flipped correctly.
   *
   * @param player The current Player to get the moves for.
   * @param index The index to flip the bit at.
   */
  private void helpFlipBit(long want, Player player, int index) {
    BitBoard bitBoard = new BitBoard();
    
    long have = bitBoard.flipBit(player, index);
    
    assertTrue(want == have);
  }

  @Test
  public void testFlipBit() {
    helpFlipBit(0b00000000_00100000_00000000_00000000_00000000_00000000_00000000_00000000L,
            Player.WHITE, 10);
    
    helpFlipBit(0b00000000_00000000_00000000_00001000_00000000_00000000_00000000_00000000L,
            Player.WHITE, 28);
    
    helpFlipBit(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_10000000L,
            Player.BLACK, 56);
    
    helpFlipBit(0b00000000_00000000_00000000_00000000_00000000_00000001_00000000_00000000L,
            Player.WHITE, 47);
    
    helpFlipBit(0b00001000_00000000_00000000_00000000_00000000_00000000_00000000_00000000L,
            Player.BLACK, 4);
    
    helpFlipBit(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000010L,
            Player.BLACK, 62);
    
    helpFlipBit(0b00000000_00000000_10000000_00000000_00000000_00000000_00000000_00000000L,
            Player.WHITE, 16);
    
    helpFlipBit(0b10000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000L,
            Player.WHITE, 0);
    
    helpFlipBit(0b00000001_00000000_00000000_00000000_00000000_00000000_00000000_00000000L,
            Player.WHITE, 7);
    
    helpFlipBit(0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000001L,
            Player.BLACK, 63);
  }

  /**
   * Checks whether a bit at an index is found correctly.
   *
   * @param bitBoard The bitBoard instance to test on.
   * @param want The expected board encoded as a long.
   * @param input A board encoded as a long.
   * @param index The index to flip the bit at.
   */
  private void helpGetBit(BitBoard bitBoard, long want, long input, int index) {
    long have = bitBoard.getBit(input, index);
    
    assertTrue(want == have);
  }

  @Test
  public void testGetBit() {
    BitBoard bitBoard = new BitBoard();

    helpGetBit(bitBoard, 1L, 
            0b10110110_01001001_11000001_01110011_01010101_10001000_10100011_01111100L, 9);
    
    helpGetBit(bitBoard, 0L, 
            0b10110110_01001001_10000001_01110011_01010101_10001000_10100011_01111101L, 1);
    
  }

  /**
   * Checks whether the enemy neighbours of the last moves are found correctly.
   *
   * @param bitBoard The bitBoard instance to test on.
   * @param wantW The expected board for white encoded as a long.
   * @param wantB The expected board for black encoded as a long.
   * @param bitsW The board of the white player encoded as a long.
   * @param bitsB The board of the black player encoded as a long.
   * @param player The current Player to flip for.
   * @param cell The cell from which to start with collecting flipping candidates,
   * @param direction The direction in which to flip.
   */
  private void helpFlipEnemyNeighboursOfLastMove(BitBoard bitBoard, 
          long wantW, long wantB, long bitsW, long bitsB, 
          Player player, Cell cell, Direction direction) {
    bitBoard.setBitsW(bitsW);
    bitBoard.setBitsB(bitsB);

    bitBoard.flipEnemyNeighboursOfLastMove(player, cell, direction);
    
    long haveW = bitBoard.getBitsW();
    long haveB = bitBoard.getBitsB();
    
    assertTrue(wantW == haveW && wantB == haveB);
  }

  @Test
  public void testFlipEnemyNeighboursOfLastMove() {
    BitBoard bitBoard = new BitBoard();

    helpFlipEnemyNeighboursOfLastMove(bitBoard,
            0b00000000_00000000_00000000_00000000_00000000_00010000_00010000_00000000L,
            0b00000000_00010000_00010000_00010000_00010000_00000000_00000000_00010000L,
            0b00000000_00000000_00010000_00010000_00000000_00010000_00010000_00000000L,
            0b00000000_00010000_00000000_00000000_00000000_00000000_00000000_00010000L,
            Player.BLACK, new Cell(3, 4), Direction.N);

    helpFlipEnemyNeighboursOfLastMove(bitBoard,
            0b00000000_00000000_00010000_00010000_00000000_00000000_00000000_00000000L,
            0b00000000_00010000_00000000_00000000_00010000_00010000_00010000_00010000L,
            0b00000000_00000000_00010000_00010000_00000000_00010000_00010000_00000000L,
            0b00000000_00010000_00000000_00000000_00000000_00000000_00000000_00010000L,
            Player.BLACK, new Cell(3, 4), Direction.S);
    
    helpFlipEnemyNeighboursOfLastMove(bitBoard,
            0b00000000_00000000_00000000_00001110_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_11110001_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_01101110_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_10010001_00000000_00000000_00000000_00000000L,
            Player.BLACK, new Cell(3, 3), Direction.W);

    helpFlipEnemyNeighboursOfLastMove(bitBoard,
            0b00000000_00000000_00000000_01100000_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_10011111_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_01101110_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_10010001_00000000_00000000_00000000_00000000L,
            Player.BLACK, new Cell(3, 3), Direction.E);

    helpFlipEnemyNeighboursOfLastMove(bitBoard,
            0b00000000_00000000_00000000_00000000_00010000_00100000_01000000_00000000L,
            0b00000000_00000010_00000100_00001000_00000000_00000000_00000000_10000000L,
            0b00000000_00000000_00000100_00000000_00010000_00100000_01000000_00000000L,
            0b00000000_00000010_00000000_00000000_00000000_00000000_00000000_10000000L,
            Player.BLACK, new Cell(4, 3), Direction.NE);

    helpFlipEnemyNeighboursOfLastMove(bitBoard,
            0b00000000_00000000_00000100_00000000_00000000_00000000_00000000_00000000L,
            0b00000000_00000010_00000000_00001000_00010000_00100000_01000000_10000000L,
            0b00000000_00000000_00000100_00000000_00010000_00100000_01000000_00000000L,
            0b00000000_00000010_00000000_00000000_00000000_00000000_00000000_10000000L,
            Player.BLACK, new Cell(4, 3), Direction.SW);

    helpFlipEnemyNeighboursOfLastMove(bitBoard,
            0b00000000_00000000_00000000_00000000_00010000_00001000_00000100_00000000L,
            0b00000000_10000000_01000000_00100000_00000000_00000000_00000000_00000010L,
            0b00000000_00000000_01000000_00000000_00010000_00001000_00000100_00000000L,
            0b00000000_10000000_00000000_00000000_00000000_00000000_00000000_00000010L,
            Player.BLACK, new Cell(2, 3), Direction.NW);

    helpFlipEnemyNeighboursOfLastMove(bitBoard,
            0b00000000_00000000_01000000_00000000_00000000_00000000_00000000_00000000L,
            0b00000000_10000000_00000000_00100000_00010000_00001000_00000100_00000010L,
            0b00000000_00000000_01000000_00000000_00010000_00001000_00000100_00000000L,
            0b00000000_10000000_00000000_00000000_00000000_00000000_00000000_00000010L,
            Player.BLACK, new Cell(2, 3), Direction.SE);
  }

  /**
   * Checks whether the enemy neighbours are flipped in all directions correctly.
   *
   * @param bitBoard The bitBoard instance to test on.
   * @param wantW The expected board for white encoded as a long.
   * @param wantB The expected board for black encoded as a long.
   * @param bitsW The board of the white player encoded as a long.
   * @param bitsB The board of the black player encoded as a long.
   * @param player The current Player to flip for.
   * @param cell The cell from which to start with flipping in all directions.
   */
  private void helpFlipInAllDirections(BitBoard bitBoard, long wantW, long wantB,
          long bitsW, long bitsB, Player player, Cell cell) {
    bitBoard.setBitsW(bitsW);
    bitBoard.setBitsB(bitsB);

    bitBoard.flipInAllDirections(player, cell);
    
    long haveW = bitBoard.getBitsW();
    long haveB = bitBoard.getBitsB();
    
    assertTrue(wantW == haveW && wantB == haveB);
  }

  @Test
  public void testFlipInAllDirections() {
    BitBoard bitBoard = new BitBoard();

    helpFlipInAllDirections(bitBoard,
            0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000L,
            0b00000000_00010000_00010000_00010000_00010000_00010000_00010000_00010000L,
            0b00000000_00000000_00010000_00010000_00000000_00010000_00010000_00000000L,
            0b00000000_00010000_00000000_00000000_00000000_00000000_00000000_00010000L,
            Player.BLACK, new Cell(3, 4));

    helpFlipInAllDirections(bitBoard,
            0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_11111111_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_01101110_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_10010001_00000000_00000000_00000000_00000000L,
            Player.BLACK, new Cell(3, 3));

    helpFlipInAllDirections(bitBoard,
            0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000L,
            0b00000000_00000010_00000100_00001000_00010000_00100000_01000000_10000000L,
            0b00000000_00000000_00000100_00000000_00010000_00100000_01000000_00000000L,
            0b00000000_00000010_00000000_00000000_00000000_00000000_00000000_10000000L,
            Player.BLACK, new Cell(4, 3));

    helpFlipInAllDirections(bitBoard,
            0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000L,
            0b00000000_10000000_01000000_00100000_00010000_00001000_00000100_00000010L,
            0b00000000_00000000_01000000_00000000_00010000_00001000_00000100_00000000L,
            0b00000000_10000000_00000000_00000000_00000000_00000000_00000000_00000010L,
            Player.BLACK, new Cell(2, 3));
  }

  /**
   * Checks whether the pieces are counted correctly.
   *
   * @param bitBoard The bitBoard instance to test on.
   * @param want The expected number of pieces.
   * @param bitsP The board of the current player encoded as a long.
   * @param player The current Player to count the pieces for.
   */
  private void helpCountPieces(BitBoard bitBoard, int want, long bitsP, Player player) {
    if (player == Player.WHITE) {
      bitBoard.setBitsW(bitsP);
    } else {
      bitBoard.setBitsB(bitsP);
    }

    int have = bitBoard.countPieces(player);

    assertTrue(want == have);
  }

  @Test
  public void testCountPieces() {
    BitBoard bitBoard = new BitBoard();

    helpCountPieces(bitBoard, 21,
            0b11100100_00110100_00110100_00110011_00100000_01100000_10000000_11100000L,
            Player.WHITE);
    
    helpCountPieces(bitBoard, 15,
            0b00000010_01000000_00001000_01001100_10011010_00011110_00100000_00000000L,
            Player.BLACK);
  }

  /**
   * Checks whether an Optional Stone is found correctly.
   *
   * @param bitBoard The bitBoard instance to test on.
   * @param want The expected Player encoded as a long.
   * @param bitsW The board of the white player encoded as a long.
   * @param bitsB The board of the black player encoded as a long.
   * @param cell The cell from which to get the Optional Stone.
   */
  private void helpGet(BitBoard bitBoard, Player want, long bitsW, long bitsB, Cell cell) {
    bitBoard.setBitsW(bitsW);
    bitBoard.setBitsB(bitsB);

    Optional<Stone> have = bitBoard.get(cell);
    Player playerHave = null;
    if (have.isPresent()) {
      playerHave = have.get().getPlayer();
    }

    assertTrue(want == playerHave);
  }

  @Test
  public void testGet() {
    BitBoard bitBoard = new BitBoard();
    helpGet(bitBoard, Player.BLACK, 
            0b00000000_01010000_00000000_01000000_10011100_00010000_00000000_00000000L,
            0b00000000_00000000_11111100_00111000_00100000_01100100_00000000_00000000L,
            new Cell(2, 5));
  
    helpGet(bitBoard, Player.WHITE, 
            0b00000000_01010000_00000000_01000000_10011100_00010000_00000000_00000000L,
            0b00000000_00000000_11111100_00111000_00100000_01100100_00000000_00000000L,
            new Cell(1, 1));

    helpGet(bitBoard, null, 
            0b00000000_01010000_00000000_01000000_10011100_00010000_00000000_00000000L,
            0b00000000_00000000_11111100_00111000_00100000_01100100_00000000_00000000L,
            new Cell(0, 0));
  }

  /**
   * Checks whether the cell belongs to a player.
   *
   * @param bitBoard The bitBoard instance to test on.
   * @param want True if the cell is of a player, false otherwise.
   * @param bitsW The board of the white player encoded as a long.
   * @param bitsB The board of the black player encoded as a long.
   * @param player The current Player to check whether the cell belongs to him.
   * @param cell The cell to check whether it belongs to the player.
   */
  private void helpIsCellOfPlayer(BitBoard bitBoard, boolean want, long bitsW, long bitsB, 
          Player player, Cell cell) {
    bitBoard.setBitsW(bitsW);
    bitBoard.setBitsB(bitsB);
  
    boolean have = bitBoard.isCellOfPlayer(player, cell);

    assertTrue(want == have);
  }

  @Test
  public void testIsCellOfPlayer() {
    BitBoard bitBoard = new BitBoard();
  
    helpIsCellOfPlayer(bitBoard, true, 
            0b00001000_00010000_00000000_00000111_00001110_00010000_00000000_00000000L,
            0b00000000_00000000_01111100_00111000_01110000_00000000_00000000_00000000L,
            Player.WHITE, new Cell(4, 0));
    helpIsCellOfPlayer(bitBoard, false, 
            0b00001000_00010000_00000000_00000111_00001110_00010000_00000000_00000000L,
            0b00000000_00000000_01111100_00111000_01110000_00000000_00000000_00000000L,
            Player.WHITE, new Cell(4, 2));
    helpIsCellOfPlayer(bitBoard, true, 
            0b00001000_00010000_00000000_00000111_00001110_00010000_00000000_00000000L,
            0b00000000_00000000_01111100_00111000_01110000_00000000_00000000_00000000L,
            Player.BLACK, new Cell(4, 2));
    helpIsCellOfPlayer(bitBoard, false, 
            0b00001000_00010000_00000000_00000111_00001110_00010000_00000000_00000000L,
            0b00000000_00000000_01111100_00111000_01110000_00000000_00000000_00000000L,
            Player.BLACK, new Cell(0, 0));
    helpIsCellOfPlayer(bitBoard, false, 
            0b00001000_00010000_00000000_00000111_00001110_00010000_00000000_00000000L,
            0b00000000_00000000_01111100_00111000_01110000_00000000_00000000_00000000L,
            Player.BLACK, new Cell(4, 0));
  }

  /**
   * Checks whether exactly the occupied stones are returned.
   *
   * @param bitBoard The bitBoard instance to test on.
   * @param want The expected map with the cells.
   * @param bitsW The board of the white player encoded as a long.
   * @param bitsB The board of the black player encoded as a long.
   */
  private void helpGetCellsOccupiedWithStones(BitBoard bitBoard, Map<Cell, Player> want, 
          long bitsW, long bitsB) {
    bitBoard.setBitsW(bitsW);
    bitBoard.setBitsB(bitsB);

    Map<Cell, Player> have = bitBoard.getCellsOccupiedWithStones();
    for (Map.Entry<Cell, Player> entry : want.entrySet()) {
      assertTrue(entry.getValue() == have.get(entry.getKey()));
    } 
  }

  @Test
  public void testGetCellsOccupiedWithStones() {
    Map<Cell, Player> want = new HashMap<>();
    want.put(new Cell(3, 3), Player.WHITE);
    want.put(new Cell(3, 4), Player.BLACK);
    want.put(new Cell(4, 3), Player.BLACK);
    want.put(new Cell(4, 4), Player.WHITE);

    BitBoard bitBoard = new BitBoard();

    helpGetCellsOccupiedWithStones(bitBoard, want,
            0b00000000_00000000_00000000_00010000_00001000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_00001000_00010000_00000000_00000000_00000000L);
  }

  @Test
  public void testIsWithinBounds_ColumnIsZero() {
    Cell cell = new Cell(0,0);
    boolean actual = GameField.isWithinBounds(cell);
    assertTrue(actual);
  }

  @Test
  public void testIsWithinBounds_RowIsOutOfBounds() {
    Cell cell = new Cell(0,8);
    boolean actual = GameField.isWithinBounds(cell);
    assertFalse(actual);
  }

  @Test
  public void testIsWithinBounds_notWithinBounds() {
    Cell cell = new Cell(0,-1);
    boolean actual = GameField.isWithinBounds(cell);
    assertFalse(actual);
  }

  @Test
  public void testIsWithinBounds_notBounds() {
    Cell cell = new Cell(7,-1);
    boolean actual = GameField.isWithinBounds(cell);
    assertFalse(actual);
  }
}