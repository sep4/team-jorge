package reversi.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * Test class for a reversi game.
 */
public class PlayBitBoardReversiTest {
  /**
   * Checks whether reversi model works correctly.
   */
  @Test
  public void testTest() {
    BitBoardReversi reversi = new BitBoardReversi();

    // Integration Test, a reversi game is played here
    reversi.place(new Cell(4, 3));
    assertTrue(reversi.getState().getField().getBitsW()
            == 0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000L
            && reversi.getState().getField().getBitsB() 
            == 0b00000000_00000000_00000000_00001000_00000000_00000000_00000000_00000000L);

    assertFalse(reversi.place(new Cell(4, 2)));

    reversi.place(new Cell(3, 3));
    assertTrue(reversi.getState().getField().getBitsW()
            == 0b00000000_00000000_00000000_00010000_00000000_00000000_00000000_00000000L
            && reversi.getState().getField().getBitsB() 
            == 0b00000000_00000000_00000000_00001000_00000000_00000000_00000000_00000000L);

    reversi.place(new Cell(3, 4));
    assertTrue(reversi.getState().getField().getBitsW()
            == 0b00000000_00000000_00000000_00010000_00000000_00000000_00000000_00000000L
            && reversi.getState().getField().getBitsB() 
            == 0b00000000_00000000_00000000_00001000_00010000_00000000_00000000_00000000L);
    
    reversi.place(new Cell(4, 4));
    assertTrue(reversi.getState().getField().getBitsW()
            == 0b00000000_00000000_00000000_00010000_00001000_00000000_00000000_00000000L
            && reversi.getState().getField().getBitsB() 
            == 0b00000000_00000000_00000000_00001000_00010000_00000000_00000000_00000000L);
    
    reversi.place(new Cell(3, 2));
    reversi.place(new Cell(2, 4));
    reversi.place(new Cell(4, 5));
    reversi.place(new Cell(5, 4));
    reversi.place(new Cell(3, 5));

    reversi.place(new Cell(2, 6));
    assertTrue(reversi.getState().getField().getBitsW()
            == 0b00000000_00000000_00000000_00000000_00101100_00010000_00100000_00000000L
            && reversi.getState().getField().getBitsB() 
            == 0b00000000_00000000_00010000_00011000_00010000_00001000_00000000_00000000L);

    reversi.place(new Cell(2, 5));
    reversi.place(new Cell(4, 2));
    reversi.place(new Cell(4, 1));
    reversi.place(new Cell(5, 3));
    reversi.place(new Cell(6, 5));

    reversi.place(new Cell(3, 1));
    assertTrue(reversi.getState().getField().getBitsW()
            == 0b00000000_00010000_00011000_00010100_00101000_00010000_00100000_00000000L
            && reversi.getState().getField().getBitsB() 
            == 0b00000000_00001000_00000000_00001000_00010100_00101010_00000000_00000000L);

    reversi.place(new Cell(2, 3));
    reversi.place(new Cell(2, 2));
    reversi.place(new Cell(1, 5));
    reversi.place(new Cell(7, 6));
    reversi.place(new Cell(2, 1));

    reversi.place(new Cell(2, 0));
    assertTrue(reversi.getState().getField().getBitsW()
            == 0b00100000_00110000_00111000_00111100_00101100_00100010_00100001_00000000L
            && reversi.getState().getField().getBitsB() 
            == 0b00000000_00001000_00000000_00000000_00010000_01011000_00000000_00000000L);

    reversi.place(new Cell(1, 4));
    reversi.place(new Cell(0, 4));
    reversi.place(new Cell(6, 3));
    reversi.place(new Cell(1, 2));
    reversi.place(new Cell(3, 0));

    reversi.place(new Cell(4, 0));
    assertTrue(reversi.getState().getField().getBitsW()
            == 0b00111000_00111000_01101000_00100000_11111000_01100010_00100001_00000000L
            && reversi.getState().getField().getBitsB() 
            == 0b00000000_00000000_00010000_00011110_00000100_00011000_00000000_00000000L);

    reversi.place(new Cell(5, 0));
    reversi.place(new Cell(6, 0));
    reversi.place(new Cell(0, 1));
    reversi.place(new Cell(6, 4));
    reversi.place(new Cell(1, 7));

    reversi.place(new Cell(3, 6));
    assertTrue(reversi.getState().getField().getBitsW()
            == 0b00111110_00110000_00111000_00010100_11111110_01111010_00010001_00000000L
            && reversi.getState().getField().getBitsB() 
            == 0b00000000_10001000_01000000_00101010_00000000_00000000_00100000_01000000L);

    reversi.place(new Cell(1, 0));
    reversi.place(new Cell(1, 1));
    reversi.place(new Cell(7, 0));
    reversi.place(new Cell(7, 3));
    reversi.place(new Cell(0, 5));

    reversi.place(new Cell(0, 2));
    assertTrue(reversi.getState().getField().getBitsW()
            == 0b00000000_01110000_11101000_00010111_10111110_01111010_00010001_00000000L
            && reversi.getState().getField().getBitsB() 
            == 0b01111111_10001000_00010000_00101000_01000000_10000000_00100000_01000000L);

    reversi.place(new Cell(0, 3));
    reversi.place(new Cell(1, 6));
    reversi.place(new Cell(1, 3));
    reversi.place(new Cell(5, 1));
    reversi.place(new Cell(0, 0));

    reversi.place(new Cell(5, 2));
    assertTrue(reversi.getState().getField().getBitsW()
            == 0b00000000_00111100_00001100_00011111_00111110_01111010_01110001_00000000L
            && reversi.getState().getField().getBitsB() 
            == 0b11111111_11000000_11110000_11100000_11000000_10000000_00000000_01000000L);

    /*
     * Last move, played by black.
     */
    assertTrue(reversi.place(new Cell(6, 1)));
    assertTrue(reversi.getState().getField().getBitsW()
            == 0b00000000_00000000_00001100_00011111_00111110_01111010_01110001_00000000L
            && reversi.getState().getField().getBitsB() 
            == 0b11111111_11111110_11110000_11100000_11000000_10000000_00000000_01000000L);
    assertTrue(reversi.getState().getWinner().get() == Player.BLACK);
    
    assertFalse(reversi.place(new Cell(6, 2)));
  }

  /**
   * Help function to print the board encoded as a long.
   *
   * @param bits The board encoded as a long.
   */
  private void printLongAsBoard(long bits) {
    String longStr = String.format("%64s", Long.toBinaryString(bits)).replace(" ", "0");
    System.out.println(String.join("\n", longStr.split("(?<=\\G.{8})")));
    System.out.println("\n");
  }
}
