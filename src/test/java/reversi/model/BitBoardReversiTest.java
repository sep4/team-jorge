package reversi.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

/**
 * Test class for BitBoardReversi.
 */
public class BitBoardReversiTest {

  /**
   * Checks whether a stone is placed correctly.
   *
   * @param reversi The reversi instance to test on.
   * @param want The expected board encoded as a long.
   * @param bitsW The white board encoded as a long.
   * @param bitsB The black board encoded as a long.
   * @param player The player who has to place a stone.
   * @param cell The cell to place on.
   */
  private void helpPlace(
          BitBoardReversi reversi, long want, long bitsW, long bitsB, Player player, Cell cell) {
    BitBoard bitBoard = reversi.getState().getField();
    bitBoard.setBitsW(bitsW);
    bitBoard.setBitsB(bitsB);
    
    reversi.getState().setCurrentPlayer(player);

    long have = 0L;

    reversi.place(cell);
    
    /*
     * Reset player
     */
    reversi.getState().setCurrentPlayer(player);

    if (reversi.getState().getCurrentPlayer() == Player.WHITE) {
      have = bitBoard.getBitsW();
    } else {
      have = bitBoard.getBitsB();
    }

    assertTrue(want == have);
  }

  @Test
  public void testPlace() {
    BitBoardReversi reversi = new BitBoardReversi();

    helpPlace(reversi,
            0b00001000_00001000_00000000_00011000_00110110_00101110_00011100_00101000L,
            0b00001000_00001000_00000000_00011000_00110010_00101000_00011000_00101000L,
            0b00000000_00110000_00011000_00000100_00001100_00010100_01100111_00000000L,
            Player.WHITE, new Cell(6, 5));

    helpPlace(reversi,
            0b01110000_00101000_01011110_00101100_00101000_01111000_10100000_00100000L,
            0b00100101_01101010_00100000_00010010_11010111_00000010_00000001_00000000L,
            0b01000000_00000000_01011110_00101100_00101000_01111000_10100000_00100000L,
            Player.BLACK, new Cell(3, 0));

    helpPlace(reversi, 
            0b00000000_01000000_00111000_00011000_00101000_00000100_00000000_00000000L,
            0b00000000_01000000_00100000_00010000_00101000_00000100_00000000_00000000L,
            0b00010000_00010000_01010000_00101000_00010000_00001010_00000010_00000010L,
            Player.WHITE, new Cell(4, 2));
    
    helpPlace(reversi,
            0b00000000_01000000_00100000_00010100_01001100_00100100_00000000_00000000L,
            0b00000000_00010000_00110000_01011000_00111100_00010000_00111000_00000000L,
            0b00000000_01000000_00000000_00000100_01000000_00100000_00000000_00000000L,
            Player.BLACK, new Cell(5, 5));

    helpPlace(reversi,
            0b00000000_00000000_00010000_00011000_00011111_00000001_00000011_00000001L,
            0b00000000_00000000_00000000_00000100_00000110_00000101_00001101_00000100L,
            0b00000000_00000000_00010000_00011000_00011000_00000000_00000010_00000001L,
            Player.BLACK, new Cell(7, 4));

    helpPlace(reversi,
            0b00000000_00000000_00001000_00011000_00101000_00101000_00000100_10000000L,
            0b00000000_00000000_00000000_00000000_00100000_00101000_00000100_10000000L,
            0b00000000_00000000_00010000_00011000_00011000_00010000_11100000_01000000L,
            Player.WHITE, new Cell(4, 2));

  }

  @Test
  public void testPlace_expectedException() {
    BitBoardReversi reversi = new BitBoardReversi();

    boolean have = reversi.place(new Cell(8,3));
    assertFalse(have);
  }
  
  /**
   * Checks whether a winning condition is found correctly.
   *
   * @param bitBoard The bitBoard instance to test on.
   * @param reversi The reversi instance to test on.
   * @param want The expected Boolean if there is a winning condition.
   * @param bitsW The white board encoded as a long.
   * @param bitsB The black board encoded as a long.
   * @param player The player checked for a winning condition.
   */
  private void helpCheckWinningCondition(BitBoard bitBoard, BitBoardReversi reversi,
          boolean want, long bitsW, long bitsB, Player player) {
    bitBoard.setBitsW(bitsW);
    bitBoard.setBitsB(bitsB);

    reversi.getState().setCurrentPlayer(player);

    boolean have = reversi.checkWinningCondition();

    assertTrue(want == have);
  }

  @Test 
  public void testCheckWinningCondition() {
    BitBoardReversi reversi = new BitBoardReversi();
    BitBoard bitBoard = reversi.getState().getField();

    helpCheckWinningCondition(bitBoard, reversi, true,
            0b11110111_10001011_10110111_11101011_11101101_11010100_11111100_11111111L,
            0b00001000_01110100_01001000_00010100_00010010_00101011_00000011_00000000L,
            Player.BLACK);

    helpCheckWinningCondition(bitBoard, reversi, true,
            0b11110111_10001011_10110111_11101011_11101101_11010100_11111100_11111111L,
            0b00001000_01110100_01001000_00010100_00010010_00101011_00000011_00000000L,
            Player.WHITE);

    helpCheckWinningCondition(bitBoard, reversi, true,
            0b11111111_11111111_11111111_11111110_11111100_11111100_11111110_11111111L,
            0b00000000_00000000_00000000_00000000_00000000_00000001_00000000_00000000L,
            Player.WHITE);

    helpCheckWinningCondition(bitBoard, reversi, true,
            0b11111111_11111111_11111111_11111110_11111100_11111100_11111110_11111111L,
            0b00000000_00000000_00000000_00000000_00000000_00000001_00000000_00000000L,
            Player.BLACK);

    helpCheckWinningCondition(bitBoard, reversi, false,
            0b00000000_00000000_00000000_00000000_00101000_00000000_00000000_00000000L,
            0b00000000_00000000_00010000_00011000_00010000_00010000_00000000_00000000L,
            Player.BLACK);
    
    helpCheckWinningCondition(bitBoard, reversi, false,
            0b00000000_00000000_00000000_00000000_00101000_00000000_00000000_00000000L,
            0b00000000_00000000_00010000_00011000_00010000_00010000_00000000_00000000L,
            Player.WHITE);
  }

  /**
   * Checks whether the possible moves are found correctly.
   *
   * @param reversi The model instance to test on.
   * @param want The expected set of cells to move at.
   * @param bitsW The board of the white player encoded as a long.
   * @param bitsB The board of the black player encoded as a long.
   * @param player The player to get the cells for.
   */
  private void helpGetPossibleCellsForPlayer(BitBoardReversi reversi, Set<Cell> want,
          long bitsW, long bitsB, Player player) {
    reversi.getState().getField().setBitsW(bitsW);
    reversi.getState().getField().setBitsB(bitsB);

    Set<Cell> cells = reversi.getPossibleCellsForPlayer(player);
    assertEquals(want, cells);
  }
  
  @Test
  public void testGetPossibleCellsForPlayer() {
    HashSet<Cell> wantB = new HashSet<>();
    wantB.add(new Cell(3, 2));
    wantB.add(new Cell(5, 4));
    wantB.add(new Cell(2, 3));
    wantB.add(new Cell(4, 5));

    BitBoardReversi reversi = new BitBoardReversi();

    helpGetPossibleCellsForPlayer(reversi, wantB,
            0b00000000_00000000_00000000_00010000_00001000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_00001000_00010000_00000000_00000000_00000000L, 
            Player.BLACK);

    HashSet<Cell> wantW = new HashSet<>();
    wantW.add(new Cell(5, 3));
    wantW.add(new Cell(3, 5));
    wantW.add(new Cell(5, 5));

    helpGetPossibleCellsForPlayer(reversi, wantW,
            0b00000000_00000000_00000000_00010000_00000000_00000000_00000000_00000000L,
            0b00000000_00000000_00000000_00001000_00011000_00001000_00000000_00000000L, 
            Player.WHITE);
  }

  @Test
  public void testNotifyListeners_UnhandledPLayer() {
    BitBoardReversi reversi = new BitBoardReversi();
    reversi.getState().setCurrentPhase(Phase.WAITING);

    assertThrows(AssertionError.class, reversi::notifyListeners);
  }

  @Test
  public void testSetState_nonBitBoardState() {
    BitBoardReversi reversi = new BitBoardReversi();

    assertThrows(IllegalArgumentException.class, () -> {
      reversi.setState(null);
    });
  }
}