import numpy as np
from py4j.java_gateway import JavaGateway

REQUIRED_SIZE = 64

WHITE_PIECE = 1
BLACK_PIECE = 2

BLACK = 0
WHITE = 1


def check_player(func):
    """
    A wrapper function for automatically getting the player from the current
    state in case none is given.

    Args:
        func: The function to be wrapped.

    Returns:
        The wrapped function.
    """
    def _wrapped_func(*args, player=None):
        """
        Checks if a player is given.
        If player is None, the current player of the game is taken as player.

        Args:
            *args: The self instance of the class and perhaps a player.
            player: A given player or None as default.

        Returns:
            The return type of the wrapped function.
        """
        if not player:
            new_player = args[0].reversi.getState().getCurrentPlayer()
            return func(*args, player=new_player)
        else:
            return func(*args, player=player)
    return _wrapped_func


class ReversiLogic(object):
    def __init__(self):
        """
        Initializes Py4J, the Reversi game instance and player constants.
        """
        self.init_py4j()
        self.init_reversi()
        ReversiLogic.init_constants(self.gateway)

    @staticmethod
    def init_constants(gateway):
        ReversiLogic.PLAYER_WHITE = gateway.jvm.reversi.model.Player.WHITE
        ReversiLogic.PLAYER_BLACK = gateway.jvm.reversi.model.Player.BLACK

    def init_py4j(self):
        """
        Initializes the main interaction point between the Python VM and 
        the Java VM.
        The JavaGateway instance is connected to the Gateway instance on 
        the Java side.
        """
        self.gateway = JavaGateway()

    def init_reversi(self):
        """
        Creates a reversi.model.BitBoardReversi instance.
        Additionally creates a reversi.model.ai.AlphaBetaBitBoardStrategy
        instance.
        """
        self.reversi = self.gateway.jvm.reversi.model.BitBoardReversi()
        self.strategy = self.gateway.jvm.reversi.model.ai.AlphaBetaBitBoardStrategy()

    @staticmethod
    def _get_board_as_array(bit_board, player):
        """
        Gets the current bit board from the Java implementation. 
        It is then converted to an array, whereby the white pieces are
        represented by 1 and the black pieces by 2. Empty cells are represented
        by 0.
        The game board is of size 65 where the last item denotes the player 
        with 1 for white and 0 for black.

        Returns:
            The game board as an array.
        """
        board_as_array = [0] * (REQUIRED_SIZE + 1)   # additional current player
        
        bits_w = bit_board.getBitsW()
        bits_b = bit_board.getBitsB()

        for cell in range(REQUIRED_SIZE):
            if bit_board.getBit(bits_w, cell) == 1:
                board_as_array[cell] = WHITE_PIECE
            if bit_board.getBit(bits_b, cell) == 1:
                board_as_array[cell] = BLACK_PIECE

        board_as_array[REQUIRED_SIZE] = int(player == ReversiLogic.PLAYER_WHITE)

        return board_as_array

    def get_board_as_array(self):
        bit_board = self.reversi.getState().getField()
        player = self.reversi.getState().getCurrentPlayer()
        return ReversiLogic.get_board_as_array(bit_board, player)

    def set_array_as_board(self, board_as_array):
        """
        Updates the bit board by iterating through the board given as an array.

        Args: 
            board_as_array: The current board as an array.
        """
        self.init_reversi()
        bit_board = self.reversi.getState().getField()

        for cell in range(REQUIRED_SIZE):
            if board_as_array[cell] == WHITE_PIECE:
                bit_board.flipBit(
                    ReversiLogic.PLAYER_WHITE,
                    cell
                )
            elif board_as_array[cell] == BLACK_PIECE:
                bit_board.flipBit(
                    ReversiLogic.PLAYER_BLACK,
                    cell
                )

        player = ReversiLogic.PLAYER_WHITE
        if board_as_array[REQUIRED_SIZE] == BLACK:
            player = ReversiLogic.PLAYER_BLACK

        self.reversi.getState().setCurrentPlayer(player)

    @check_player
    def get_possible_moves(self, player=None):
        """
        Returns the possible moves for a given player.

        Args:
            player: The current player or None as default. The player is
            inferred from the current state if None is given.

        Returns:
            An array of indices of possible moves.
        """
        bit_board = self.reversi.getState().getField()
        bits_moves = bit_board.getMoves(player)

        return [
            cell for cell in range(REQUIRED_SIZE) if bit_board.getBit(
                bits_moves, cell) == 1
        ]


    @check_player
    def get_random_action(self, player):
        """
        Returns a random move from the possible moves.

        Args:
            player: The player to get a random move for.

        Returns:
            -1 if no move possible or a random move out of all possible moves.
        """
        candidates = self.get_possible_moves(player=player)

        if not candidates:
            return -1
        
        return np.random.choice(candidates)

    @check_player
    def get_best_action(self, player):
        """
        Returns the best possible action in the current state for a given
        player.
        Uses AlphaBetaBitBoardStrategy.

        Args:
            player: The player to get a random move for.

        Returns:
            The best move.
        """
        best_move = self.strategy.calculateMove(self.reversi.getState())
        index = self.gateway.jvm.reversi.model.BitBoard.getIndexOfCell(best_move)

        return index
    
    def place(self, index):
        """
        Places a piece on cell if this is valid.

        Args:
            index: The index to place a piece.

        Returns:
            True if the place is valid otherwise False.
        """
        cell = self.gateway.jvm.reversi.model.BitBoard.getCellAtIndex(int(index))
        return self.reversi.place(cell)

    def is_game_over(self):
        """
        Checks if a game is over by checking if phase is finished.

        Returns:
            True if phase is finished otherwise False.
        """
        finished = self.gateway.jvm.reversi.model.Phase.FINISHED
        return self.reversi.getState().getCurrentPhase() == finished

    def get_reward(self, ai_first):
        """
        Gets the reward.

        Args:
            ai_first: Whether the RL Ai is the first player.

        Returns:
            1 as reward if the RL AI wins the game otherwise -1. If remis, -.5.
        """
        ai_player = 'Black' if ai_first else 'White'
        
        remis = not self.reversi.getState().getWinner().isPresent()
        if remis:
            return -.5

        winner = self.reversi.getState().getWinner().get()

        return 1 if ai_player == winner.toString() else -1
