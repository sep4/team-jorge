from reversi_env import ReversiEnv
from reversi_model import ReversiModel
from reversi_model import ReversiModelBAD
from reversi_logic import ReversiLogic

from tensorflow.python.framework.ops import disable_eager_execution
disable_eager_execution()

from py4j.java_gateway import JavaGateway, CallbackServerParameters


class PythonListener(object):
    def __init__(self, gateway):
        self.gateway = gateway
        self.model = None

    def notify(self, obj):
        if not self.model:
            self.model = ReversiModel()
            self.model.dqn.load_weights('models/weights31.h5f')
            ReversiLogic.init_constants(self.gateway)

        bit_board = obj.getField()
        player = obj.getCurrentPlayer()
        board = ReversiLogic._get_board_as_array(bit_board, player)

        return self.model.dqn.forward(board)

    class Java:
        implements = ["reversi.model.Py4JListener"]


def run():
    gateway = JavaGateway(
        callback_server_parameters=CallbackServerParameters())
    listener = PythonListener(gateway)
    gateway.entry_point.registerListener(listener)
    # gateway.entry_point.notifyAllListeners()
    # gateway.shutdown()


if __name__ == '__main__':
    run()
