import numpy as np

from rl.core import Space

from reversi_logic import ReversiLogic


class ActionSpace(Space):
    def sample(self, seed=None):
        """
        Gets a random element of this space.

        Returns:
            Returns a random element of the space.
        """
        return np.random.randint(self.reversi_logic.REQUIRED_SIZE)

    def contains(self, x):
        """
        Checks whether x is a valid member of the space.

        Returns:
            Returns whether if x is a valid member of this space.
        """
        return x >= 0 and x <= self.reversi_logic.REQUIRED_SIZE
