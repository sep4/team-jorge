import tensorflow
from tensorflow.keras.layers import Reshape
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Add
from tensorflow.keras.layers import Concatenate
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import GlobalAveragePooling2D
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Flatten
from tensorflow.keras.models import Model
from tensorflow.keras.regularizers import l2
from tensorflow.keras.optimizers import Adam

from rl.memory import SequentialMemory

from reversi_dqn_agent import ReversiDQNAgent


"""
This file contains two Models.
ModelBAD was our first try, in which a simple fully connected network was used
(with varying number of layers).
Since it did not work out, we used the AlphaZero approach with residual CNNs.
The AlphaZero originates from here: https://github.com/sasaco/tf-dqn-reversi
We adapted it slightly.
"""


DN_FILTERS = 128
DN_RESIDUAL_NUM =  16
DN_INPUT_SHAPE = (8, 8, 1)
DN_OUTPUT_SIZE = 65


def conv(filters):
    """
    Return a Conv2D with prepared parameters.
    In this case same kernel size is 3, padding is same, no biases and a
    kernel regularizer is used.

    Args:
        filters: Number of filters.

    Returns:
        A convolutional 2D neural network.
    """
    return Conv2D(filters, 3, padding='same', use_bias=False,
        kernel_initializer='he_normal', kernel_regularizer=l2(0.0004))


def residual_block():
    def f(x):
        sc = x
        x = conv(DN_FILTERS)(x)
        x = BatchNormalization()(x)
        x = Activation('relu')(x)
        x = conv(DN_FILTERS)(x)
        x = BatchNormalization()(x)
        x = Add()([x, sc])
        x = Activation('relu')(x)
        return x
    return f


class ReversiModel:
    def __init__(self):
        input_ = Input(shape=(1,65,))
        xx = Reshape((65,))(input_)
        # Split into board itself (8x8) and the current player.
        split0, split1 = tensorflow.split(xx, [64, 1], 1)
        y = Activation('relu')(split1)
        x = Reshape((8, 8, 1))(split0)

        x = conv(DN_FILTERS)(x)
        x = BatchNormalization()(x)
        x = Activation('relu')(x)

        for i in range(DN_RESIDUAL_NUM):
            x = residual_block()(x)

        x = GlobalAveragePooling2D()(x)
        x = tensorflow.keras.layers.concatenate([x, y])
        p = Dense(DN_OUTPUT_SIZE, kernel_regularizer=l2(0.0004),
                    activation='softmax', name='pi')(x)

        model = Model(inputs=[input_], outputs=[p])

        memory = SequentialMemory(limit=100000, window_length=1)

        self.dqn = ReversiDQNAgent(
                model=model,
                nb_actions=65,
                memory=memory,
                nb_steps_warmup=500,
                #enable_dueling_network=True,
                #target_model_update=1e-2
                #target_model_update=1e-4
                #target_model_update=1e-5
                target_model_update=1e-3
        )
        Adam._name = 'Adam'
        self.dqn.compile(Adam(lr=1e-4), metrics=['mae'])


class ReversiModelBAD:
    def __init__(self, input_size=(65,), output_size=65):
        model = Sequential()
        model.add(Flatten(input_shape=(1,) + input_size))

        for i in range(10):
            model.add(Dense(65))
            model.add(Activation('relu'))

        # for i in range(10):
        #     model.add(Dense(130))
        #     model.add(Activation('relu'))

        # for i in range(20):
        #     model.add(Dense(65))
        #     model.add(Activation('relu'))

        # for i in range(16):
        #     model.add(Dense(65))
        #     model.add(Activation('relu'))

        model.add(Dense(output_size))
        model.add(Activation('linear'))

        memory = SequentialMemory(limit=100000, window_length=1)

        self.dqn = ReversiDQNAgent(
            model=model,
            nb_actions=output_size,
            memory=memory,
            nb_steps_warmup=200,
            # target_model_update=1e-2
            # target_model_update=1e-4
            # target_model_update=1e-5
            target_model_update=1e-5
        )
        Adam._name = 'Adam'
        self.dqn.compile(Adam(lr=1e-4), metrics=['mae'])
