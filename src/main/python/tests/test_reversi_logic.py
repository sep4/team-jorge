import unittest
import struct
from reversi_logic import ReversiLogic


class TestReversiLogic(unittest.TestCase):
    def help_get_reward(self, 
            reversi_logic, 
            want, 
            white, 
            black, 
            ai_first, 
            winner):
        '''
        Checks whether the reward is calculated right.

        Args:
            reversi_logic: The ReversiLogic instance to test on.
            want: The wanted reward.
            white: The white board encoded as a long.
            black: The black board encoded as a long.
            ai_first: Whether the AI is the first player (True).
            winner: The winner of the current test case.
        '''
        bit_board = reversi_logic.reversi.getState().getField()
        state = reversi_logic.reversi.getState()

        bits_w = struct.unpack('l',struct.pack('P', int(white, 2)))
        bit_board.setBitsW(bits_w[0])
        
        bits_b = struct.unpack('l',struct.pack('P', int(black, 2)))
        bit_board.setBitsB(bits_b[0])
        
        state.setCurrentPhase(reversi_logic.gateway.jvm.reversi.model.Phase.FINISHED);

        if winner:
            state.setWinner(winner)
 
        have = reversi_logic.get_reward(ai_first)
        self.assertEqual(want, have)

    def test_get_reward(self):
        reversi_logic = ReversiLogic()
        
        # A draw
        winner = None
        self.help_get_reward(reversi_logic,
                -.5, 
                '0000000000000000000000000000100000101000110111001110111011111111', 
                '0000000000011100000111101111011011010110001000100001000100000000', 
                False, 
                winner)
        self.help_get_reward(reversi_logic,
                -.5, 
                '0000000000000000000000000000100000101000110111001110111011111111', 
                '0000000000011100000111101111011011010110001000100001000100000000', 
                True, 
                winner)

        # White is the winner
        winner = reversi_logic.gateway.jvm.reversi.model.Player.WHITE
        self.help_get_reward(reversi_logic,
                1, 
                '1111111111111111111110011111001111001111100111111000111111111111', 
                '0000000000000000000001100000110000110000011000000111000000000000', 
                False, 
                winner)
        self.help_get_reward(reversi_logic,
                -1, 
                '1111111111111111111110011111001111001111100111111000111111111111', 
                '0000000000000000000001100000110000110000011000000111000000000000', 
                True, 
                winner)

        # Black is the winner
        winner = reversi_logic.gateway.jvm.reversi.model.Player.BLACK
        self.help_get_reward(reversi_logic,
                -1, 
                '0100000000000000000100000011000000010000100100000000100000000100', 
                '1011111111111111111011111100111111101111011011111111011111111011', 
                False, 
                winner)
        self.help_get_reward(reversi_logic,
                1, 
                '0100000000000000000100000011000000010000100100000000100000000100', 
                '1011111111111111111011111100111111101111011011111111011111111011', 
                True, 
                winner)

    def test_is_game_over(self):
        '''
        Checks whether the game is over correctly. 
        The game is over if the phase is finished.
        '''
        reversi_logic = ReversiLogic()
        state = reversi_logic.reversi.getState()

        # The game is finished
        state.setCurrentPhase(
                reversi_logic.gateway.jvm.reversi.model.Phase.FINISHED);
        have = reversi_logic.is_game_over()
        self.assertEqual(True, have)

        # The game is still  running
        state.setCurrentPhase(
                reversi_logic.gateway.jvm.reversi.model.Phase.RUNNING);
        have = reversi_logic.is_game_over()
        self.assertEqual(False, have)

    def help_get_board_as_array(self,
            reversi_logic,
            want,
            white,
            black,
            player):
        '''
        Checks whether the board got as array correctly.

        Args:
            reversi_logic: The ReversiLogic instance to test on.
            want: The wanted board as array.
            white: The white board encoded as a long.
            black: The black board encoded as a long.
            player: The current player to the current board.
        '''
        bit_board = reversi_logic.reversi.getState().getField()
        state = reversi_logic.reversi.getState()

        bits_w = struct.unpack('l', struct.pack('P', int(white, 2)))
        bit_board.setBitsW(bits_w[0])
        
        bits_b = struct.unpack('l', struct.pack('P', int(black, 2)))
        bit_board.setBitsB(bits_b[0])

        state.setCurrentPlayer(player)
        
        have = reversi_logic.get_board_as_array()
        self.assertEqual(want, have)


    def test_get_board_as_array(self):
        reversi_logic = ReversiLogic()
        white = reversi_logic.gateway.jvm.reversi.model.Player.WHITE
        black = reversi_logic.gateway.jvm.reversi.model.Player.BLACK

        bits_w = '0111111101100001010110110100110101000101111011110001100111111111' 
        bits_b = '1000000010011110101001001011001010111010000100000010011000000000'
        want = [2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 0, 0, 2, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1] 
        self.help_get_board_as_array(reversi_logic, want, bits_w, bits_b, white)

        want = [2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 0, 0, 2, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0] 
        self.help_get_board_as_array(reversi_logic, want, bits_w, bits_b, black)

    def help_update_board(self, 
            reversi_logic,
            board,
            want_w,
            want_b,
            player):
        '''
        Checks whether the board is updated correctly.

        Args:
            reversi_logic: The ReversiLogic instance to test on.
            board: The board to update. With 2 for black pieces, 
                   1 for white pieces and 0 for no pieces.
            want_w: The wanted white board encoded as a long.
            want_b: The black board encoded as a long.
            player: The player who placed the last piece.
        '''
        reversi_logic.set_array_as_board(board)

        bit_board = reversi_logic.reversi.getState().getField()
        have_w = bit_board.getBitsW() & 0xffffffffffffffff
        have_b = bit_board.getBitsB() & 0xffffffffffffffff

        state = reversi_logic.reversi.getState()
        have_player = state.getCurrentPlayer()
        
        self.assertEqual(want_w, have_w)
        self.assertEqual(want_b, have_b)
        self.assertEqual(player, have_player)
        
    def test_update_board(self):
        reversi_logic = ReversiLogic()
        white = reversi_logic.gateway.jvm.reversi.model.Player.WHITE
        black = reversi_logic.gateway.jvm.reversi.model.Player.BLACK

        want_w = 0b0111111101100001010110110100110101000101111011110001100111111111
        want_b = 0b1000000010011110101001001011001010111010000100000010011000000000

        board = [2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 0, 0, 2, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0] 
        self.help_update_board(reversi_logic, board, want_w, want_b, black)

        board = [2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 0, 0, 2, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1] 
        self.help_update_board(reversi_logic, board, want_w, want_b, white)

    def help_get_possible_moves(self, 
            reversi_logic,
            want, 
            white, 
            black, 
            player):
        '''
        Checks whether the possible moves are found correctly.

        Args:
            reversi_logic: The ReversiLogic instance to test on.
            want: The wanted possible moves.
            white: The white board encoded as a long.
            black: The black board encoded as a long.
            player: The player to get the possible moves for.
        '''
        bit_board = reversi_logic.reversi.getState().getField()

        bits_w = struct.unpack('l',struct.pack('P', int(white, 2)))
        bit_board.setBitsW(bits_w[0])
        
        bits_b = struct.unpack('l',struct.pack('P', int(black, 2)))
        bit_board.setBitsB(bits_b[0])

        have = reversi_logic.get_possible_moves(player=player)
        self.assertEqual(want, have)
        
    def test_get_possible_moves(self):
        reversi_logic = ReversiLogic()
        white = reversi_logic.gateway.jvm.reversi.model.Player.WHITE
        black = reversi_logic.gateway.jvm.reversi.model.Player.BLACK

        self.help_get_possible_moves(reversi_logic,
                [10, 18, 20, 34],
                '0000000000000000000000000000000000001100000000000000000000000000',
                '0000000000000000000100000001100000010000000000000000000000000000',
                white)

        
        self.help_get_possible_moves(reversi_logic,
                [38, 44, 45, 46],
                '0000000000000000000000000000000000001100000000000000000000000000',
                '0000000000000000000100000001100000010000000000000000000000000000',
                black)

        self.help_get_possible_moves(reversi_logic,
                [27, 28],
                '0000000000000000000000000000000000001000000000000000000000000000',
                '0000000000000000000000000000000000010000000000000000000000000000',
                black)

        self.help_get_possible_moves(reversi_logic,
                [27, 28, 35, 36],
                '0000000000000000000000000000000000000000000000000000000000000000',
                '0000000000000000000000000000000000000000000000000000000000000000',
                white)
