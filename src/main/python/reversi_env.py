import random
import numpy as np

from rl.core import Env
from rl.core import Space

from reversi_logic import ReversiLogic
from reversi_space import ActionSpace


class ReversiEnv(Env):
    def __init__(self):
        """
        Initializes the environment by specifying the action space.
        """
        super(ReversiEnv, self).__init__()
        self.action_space = ActionSpace()

    def reset(self):
        """
        Resets the state of the environment and returns an initial observation, 
        i.e. the game board in the first game state.

        Returns:
            The game board in the first game state. This is the initial 
            observation of the space, where the initial reward 
            is assumed to be 0.
        """
        self.ai_first = random.random() > .5

        self.reversi_logic = ReversiLogic()

        if not self.ai_first:
            self._place_best()
        
        return self.reversi_logic.get_board_as_array()

    def render(self, mode='human', close=False):
        pass

    def close(self):
        pass

    def _place_random(self):
        """
        Places a random action.
        """
        random_action = self.reversi_logic.get_random_action()
        self.reversi_logic.place(random_action)

    def _place_best(self):
        """
        Places the best action.
        """
        best_action = self.reversi_logic.get_best_action()
        self.reversi_logic.place(best_action)

    def step(self, action):
        """
        Runs one timestep of the environment's dynamics.
        Accepts an action.

        Args:
            action: An action provided by the environment.

        Returns:
            Returns a 4-Tupel containing:
            * an observation of the game state which is the current environment.
            * amount of reward returned after the previous action.
            * whether the episode has ended.
            * info containing auxiliary diagnostic information if necessary
              for debugging or learning.
        """
        self.reversi_logic.place(action)
        observation = self.reversi_logic.get_board_as_array()
        if self.reversi_logic.is_game_over():
            return observation, self.reversi_logic.get_reward(self.ai_first), True, {}

        self._place_best()
        observation = self.reversi_logic.get_board_as_array()
        if self.reversi_logic.is_game_over():
            return observation, self.reversi_logic.get_reward(self.ai_first), True, {}

        return self.reversi_logic.get_board_as_array(), 0, False, {}
