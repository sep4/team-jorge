from rl.agents.dqn import DQNAgent

from reversi_logic import ReversiLogic
from reversi_policy import ReversiPolicy
from reversi_test_policy import ReversiTestPolicy


class ReversiDQNAgent(DQNAgent):
    def __init__(self, *args, **kwargs):
        """
        Initializes the policy and the test_policy for reversi.
        A policy helps with the selection of an action to take on an 
        environment, in this case the environment is the game state.
        """
        super(ReversiDQNAgent, self).__init__(*args, **kwargs)
        self.policy = ReversiPolicy(eps=.05)
        self.test_policy = ReversiTestPolicy()

    def forward(self, observation):
        """
        Do a forward pass.
        We override this function to monkey-patch the observation into the
        policies. This is needed since we need to return a valid move from the
        Q values that are given for all (incl. illegal) moves.

        Args:
            observation: The current state.

        Returns:
            The return type of the overriden forward function.
        """
        self.policy.observation = observation
        self.test_policy.observation = observation

        return super(ReversiDQNAgent, self).forward(observation)
