from reversi_env import ReversiEnv
from reversi_model import ReversiModel
from rl.callbacks import Callback

from tensorflow.python.framework.ops import disable_eager_execution
disable_eager_execution()


class CustomCallback(Callback):
    """
    Custom callback class for summing up all episode rewards
    Used for debugging.
    """
    def __init__(self):
        self.total_reward = 0

    def on_episode_end(self, episode, logs):
        self.total_reward += logs['episode_reward']


def train():
    """
    Simply trains that model.
    verbose=2 shows losses per episode
    verbose=1 shows reward
    """
    env = ReversiEnv()
    model = ReversiModel()
    # model.dqn.fit(env, nb_steps=30000, verbose=2)
    # model.dqn.load_weights('models/weights24.h5f')
    model.dqn.fit(env, nb_steps=101000, verbose=1)
    model.dqn.save_weights('models/weights32.h5f', overwrite=True)
    cc = CustomCallback()
    model.dqn.test(env, callbacks=[cc], nb_episodes=100)
    print("Total rewards:", cc.total_reward)


if __name__ == '__main__':
    train()
