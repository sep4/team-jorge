import numpy as np

from rl.policy import EpsGreedyQPolicy

from reversi_logic import ReversiLogic


class ReversiPolicy(EpsGreedyQPolicy):
    """
    The epsilon greedy q policy from keras-rl either:
    * takes a random action with probability epsilon
    * takes current best action with probability (1 - epsilon)
    """
    def __init__(self, *args, **kwargs):
        """
        Initializes a reversi instance from ReversiLogic.
        """
        super(ReversiPolicy, self).__init__(*args, **kwargs)
        self.reversi = ReversiLogic()

    def select_action(self, q_values):
        """
        Selects an action according to the q-values.

        Args:
            q_values: The q-values for an action given by the learnt neural 
                      network.

        Returns: 
            Returns the selected action with the best q-value.
        """
        self.reversi.set_array_as_board(self.observation)
        candidates = self.reversi.get_possible_moves()

        if not candidates:
            action = -1
        elif np.random.random() < self.eps:
            # We do exploration.
            action = self.reversi.get_random_action()
        else:                            
            candidates_q_values = q_values[candidates]
            action = candidates[np.argmax(candidates_q_values)]
        
        return action
