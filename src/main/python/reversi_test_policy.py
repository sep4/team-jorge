import numpy as np
from rl.policy import Policy

from reversi_logic import ReversiLogic


class ReversiTestPolicy(Policy):
    def __init__(self):
        """
        Initializes a reversi instance of ReversiLogic.
        """
        super(ReversiTestPolicy, self).__init__()
        self.reversi = ReversiLogic()

    def select_action(self, q_values):
        """
        Selects an action according to the q-values.

        Args:
            q_values: The q-values for an action given by the learnt neural
                      network.

        Returns:
            Returns the selected action with the best q-value.
        """
        self.reversi.set_array_as_board(self.observation)
        candidates = self.reversi.get_possible_moves()

        if not candidates:
            action = -1
        else:
            candidates_q_values = q_values[candidates]
            action = candidates[np.argmax(candidates_q_values)]

        return action
