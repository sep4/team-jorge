package server.view;

import static java.util.Objects.requireNonNull;

import java.awt.BorderLayout;
import java.awt.Dimension;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import java.util.Collection;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import reversi.model.Player;

import server.model.Game;
import server.model.Server;

/**
 * The view class of the server.
 */
public class ServerView extends JFrame implements PropertyChangeListener {

  private static final long serialVersionUID = 2806623066348997897L;

  private static final int MINIMUM_FRAME_HEIGHT = 500;
  private static final int MINIMUM_FRAME_WIDTH = 500;

  private Server server;

  private JLabel lobbyLabel;
  private JLabel runningGames;
  private JLabel waitingPlayers;

  /**
   * Creates a view with all elements necessary for displaying the current state of the server.
   *
   * @param server The server whose status will be shown.
   */
  ServerView(Server server) {
    super("Reversi Server");

    this.server = requireNonNull(server);
    server.subscribe(this);

    setMinimumSize(new Dimension(MINIMUM_FRAME_WIDTH, MINIMUM_FRAME_HEIGHT));
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    pack();

    createView();
    updateView();
  }

  private void createView() {
    setLayout(new BorderLayout());

    lobbyLabel = new JLabel();
    runningGames = new JLabel();
    waitingPlayers = new JLabel();

    JPanel lobbyPanel = new JPanel();
    lobbyPanel.add(lobbyLabel);
    add(lobbyPanel, BorderLayout.WEST);

    JPanel gamePanel = new JPanel();
    gamePanel.add(runningGames);
    add(gamePanel, BorderLayout.CENTER);

    JPanel waitingPanel = new JPanel();
    waitingPanel.add(waitingPlayers);
    add(waitingPanel, BorderLayout.EAST);
  }

  /**
   * Shows the graphical user interface of the reversi server.
   */
  void showView() {
    setLocationByPlatform(true);
    pack();
    setVisible(true);
  }

  /**
   * Shows an error to the user.
   *
   * @param header The header of the error message.
   * @param message The error message.
   */
  void showError(String header, String message) {
    JOptionPane.showMessageDialog(this, message, header, JOptionPane.ERROR_MESSAGE);
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    updateView();
  }

  private void updateView() {
    Collection<String> lobby = server.getLobby();
    StringBuilder lobbyBuilder = new StringBuilder("<html>Users currently<br>in the lobby:<br>");
    if (lobby != null) {
      for (String name : lobby) {
        lobbyBuilder.append("<br>").append(name);
      }
    }
    lobbyBuilder.append("</html>");
    lobbyLabel.setText(lobbyBuilder.toString());

    Collection<Game> games = server.getGames();
    StringBuilder gamesBuilder = new StringBuilder("<html>Currently running games:<br>");
    StringBuilder waitingBuilder =
        new StringBuilder("<html>Users currently waiting<br>for an opponent:<br>");
    if (games != null) {
      for (Game game : games) {
        String creator = game.getCreator();
        Player creatorPlayer = game.getCreatorPlayer();
        String creatorString = creator + " (" + creatorPlayer + ")";
        if (game.isRunning()) {
          String joiner = game.getOpponent(creator);
          Player joinerPlayer = game.getCreatorPlayer().getOtherPlayer();
          String joinerString = joiner + " (" + joinerPlayer + ")";
          String gameString = creatorString + " vs. " + joinerString;
          gamesBuilder.append("<br>").append(gameString);
        } else {
          waitingBuilder.append("<br>").append(creatorString);
        }
      }
    }
    gamesBuilder.append("</html>");
    waitingBuilder.append("</html>");
    runningGames.setText(gamesBuilder.toString());
    waitingPlayers.setText(waitingBuilder.toString());
    repaint();
  }

  @Override
  public void dispose() {
    server.unsubscribe(this);
    super.dispose();
  }
}
