package server.view;

import java.io.IOException;

import server.model.Server;

/**
 * A controller class. Its main purpose is to set up the view and server and the communication
 * between them and clean it up upon the disposal of the application.
 */
public class ServerController {

  private ServerView view;
  private Server server;

  /**
   * Creates a new controller object.
   */
  public ServerController() {
    server = new Server();
    view = new ServerView(server);
  }

  /**
   * Starts the server and shows the user interface.
   */
  public void start() {
    view.showView();
    try {
      server.start();
    } catch (IOException e) {
      view.showError(
          "Error starting server",
          "Starting the server failed. The following error occurred: " + e.getMessage());
    }
  }
}
