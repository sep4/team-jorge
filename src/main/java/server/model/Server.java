package server.model;

import static java.util.Objects.requireNonNull;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.io.Closeable;
import java.io.IOException;

import java.net.ServerSocket;
import java.net.Socket;

import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONObject;

import reversi.model.Cell;
import reversi.model.Player;

/**
 * The main component of the network functionality. The server manages the lobby, the open and the
 * running games by handling the input from clients, validating it and then sending respective
 * information to the clients. On one server multiple games can be created and hosted.
 */
public class Server implements NetworkConstants, Closeable {

  private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

  private ServerSocket socket;
  private ArrayList<ClientHandler> clients = new ArrayList<>();

  private ArrayList<String> lobby = new ArrayList<>();
  private ArrayList<Game> games = new ArrayList<>();

  /**
   * Starts the server so it can accept client connections and handle the communication between
   * clients.
   *
   * @throws IOException if a server socket is already bound to {@link NetworkConstants#PORT}
   */
  public void start() throws IOException {
    socket = new ServerSocket(PORT);
    Thread connectorThread = new Thread(() -> acceptConnections(socket));
    connectorThread.setDaemon(true);
    connectorThread.start();
  }

  private void acceptConnections(ServerSocket socket) {
    try {
      while (true) {
        Socket newConnection = socket.accept();
        if (newConnection != null) {
          clients.add(new ClientHandler(this, newConnection));
        }
      }
    } catch (IOException e) {
      // may happen if socket is closed pre-maturely and is thus excpected
    }
  }

  /**
   * Processes the input from clients depending on the key and takes the according action
   * afterwards.
   *
   * @param client The client that sent the input.
   * @param key The key of the sent {@link JSONObject}.
   * @param jsonObject The {@link JSONObject} that was sent by the client.
   */
  void handleInput(ClientHandler client, String key, JSONObject jsonObject) {
    switch (key) {
      case NAME:
        handleName(client, jsonObject);
        break;

      case CREATE:
        handleCreate(client, jsonObject);
        break;

      case JOIN:
        handleJoin(client, jsonObject);
        break;

      case MOVE:
        handleMove(client, jsonObject);
        break;

      case QUIT:
        handleQuit(client, jsonObject);
        break;

      default:
        // The client sent something with an unknown key and therefore can't be a working reversi
        // client and therefore should be disconnected from the server.
        client.cleanUpConnection();
    }
  }

  private void handleName(ClientHandler client, JSONObject receivedObject) {
    String name = receivedObject.getString(NAME);

    // sleep a short moment to give the receiving thread of the client time to start up
    try {
      Thread.sleep(200);
    } catch (InterruptedException e) {
      // ignore
    }

    if (isUniqueName(name)) {
      client.setUsername(name);
      lobby.add(name);
      JSONObject json = new JSONObject();
      json.put(NAME_APPROVED, JSONObject.NULL);
      client.sendJson(json);
      broadcastUpdate();
    } else {
      JSONObject json = new JSONObject();
      json.put(NAME_REJECTED, JSONObject.NULL);
      client.sendJson(json);
    }
  }

  private boolean isUniqueName(String name) {
    for (Game game : games) {
      if (game.getPlayers().contains(name)) {
        return false;
      }
    }
    return !lobby.contains(name);
  }

  private void handleCreate(ClientHandler client, JSONObject jsonObject) {
    if (!client.isApproved()) {
      return;
    }
    String name = client.getUsername();
    String playerString = jsonObject.getString(CREATE);
    Player player;
    switch (playerString) {
      case BLACK:
        player = Player.BLACK;
        break;
      case WHITE:
        player = Player.WHITE;
        break;
      default:
        throw new AssertionError("Unknown player: " + name);
    }
    games.add(new Game(name, player));
    lobby.remove(name);
    broadcastUpdate();
  }

  private void handleJoin(ClientHandler client, JSONObject jsonObject) {
    if (!client.isApproved()) {
      return;
    }
    String creator = jsonObject.getString(JOIN);
    String joiner = client.getUsername();
    for (Game game : games) {
      if (!game.getCreator().equals(creator)) {
        continue;
      }
      if (game.join(joiner)) {
        lobby.remove(joiner);
        broadcastUpdate();
        startGame(game);
      } else {
        JSONObject rejected = new JSONObject();
        rejected.put(JOIN_REJECTED, JSONObject.NULL);
      }
      break;
    }
  }

  private void startGame(Game game) {
    String creator = game.getCreator();
    String joiner = game.getOpponent(creator);

    JSONObject creatorGame = new JSONObject();
    creatorGame.put(OPPONENT, joiner);
    creatorGame.put(PLAYER, getPlayerToString(game.getCreatorPlayer()));
    JSONObject creatorJson = new JSONObject();
    creatorJson.put(START_GAME, creatorGame);

    JSONObject joinerGame = new JSONObject();
    joinerGame.put(OPPONENT, creator);
    joinerGame.put(PLAYER, getPlayerToString(game.getCreatorPlayer().getOtherPlayer()));
    JSONObject joinerJson = new JSONObject();
    joinerJson.put(START_GAME, joinerGame);

    sendJsonObject(creator, creatorJson);
    sendJsonObject(joiner, joinerJson);
  }

  private String getPlayerToString(Player player) {
    if (player == Player.BLACK) {
      return BLACK;
    }
    if (player == Player.WHITE) {
      return WHITE;
    }
    throw new AssertionError("Unhandled player: " + player);
  }

  private void handleMove(ClientHandler client, JSONObject jsonObject) {
    JSONObject object = jsonObject.getJSONObject(MOVE);
    int row = object.getInt(ROW);
    int column = object.getInt(COLUMN);
    String name = client.getUsername();
    for (Game game : games) {
      if (!game.getPlayers().contains(name)) {
        continue;
      }
      if (game.place(new Cell(column, row), name)) {
        sendJsonObject(name, jsonObject);
        sendJsonObject(game.getOpponent(name), jsonObject);
      }
      break;
    }
  }

  private synchronized void handleQuit(ClientHandler client, JSONObject jsonObject) {
    String name = client.getUsername();
    Game quittedGame = null;
    for (Game game : games) {
      if (game.getPlayers().contains(client.getUsername())) {
        quittedGame = game;
        String opponent = game.getOpponent(name);
        if (opponent != null) {
          sendJsonObject(opponent, jsonObject);
          lobby.add(opponent);
        }
        lobby.add(name);
        break;
      }
    }
    if (quittedGame != null) {
      games.remove(quittedGame);
    }
    broadcastUpdate();
  }

  private synchronized void broadcastUpdate() {
    notifyListeners();
    JSONObject json = new JSONObject();
    json.put(LOBBY, lobby);
    ArrayList<JSONObject> openGames = new ArrayList<>();
    for (Game game : games) {
      if (!game.isRunning()) {
        JSONObject gameJson = new JSONObject();
        gameJson.put(CREATOR, game.getCreator());
        String playerString = getPlayerToString(game.getCreatorPlayer());
        gameJson.put(CREATOR_PLAYER, playerString);
        openGames.add(gameJson);
      }
    }
    json.put(GAMES, new JSONArray(openGames));
    JSONObject update = new JSONObject();
    update.put(UPDATE, json);
    for (ClientHandler client : clients) {
      if (client.isApproved()) {
        client.sendJson(update);
      }
    }
  }

  private void sendJsonObject(String name, JSONObject jsonObject) {
    for (ClientHandler client : clients) {
      if (name.equals(client.getUsername())) {
        client.sendJson(jsonObject);
        break;
      }
    }
  }

  void removeUser(ClientHandler client) {
    String name = client.getUsername();
    lobby.remove(name);
    clients.remove(client);
    Game quittedGame = null;
    for (Game game : games) {
      if (game.getPlayers().contains(name)) {
        quittedGame = game;
        String opponent = game.getOpponent(name);
        if (opponent != null) {
          JSONObject opponentJson = new JSONObject();
          opponentJson.put(DISCONNECT, true);
          sendJsonObject(opponent, opponentJson);
          lobby.add(opponent);
        }
        break;
      }
    }
    if (quittedGame != null) {
      games.remove(quittedGame);
    }
    broadcastUpdate();
  }

  /**
   * Returns the current lobby of the server.
   *
   * @return The current lobby.
   */
  public Collection<String> getLobby() {
    return lobby;
  }

  /**
   * Returns the games that are currently hosted by the server.
   *
   * @return The currently hosted games.
   */
  public Collection<Game> getGames() {
    return games;
  }

  private void notifyListeners() {
    pcs.firePropertyChange("Changes", null, this);
  }

  /**
   * Adds a {@link PropertyChangeListener} to the server that will be notified about changes in the
   * hosted games and the lobby.
   *
   * @param pcl The listener.
   */
  public void subscribe(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    pcs.addPropertyChangeListener(pcl);
  }

  /**
   * Removes a listener from the server, which will then no longer be notified about any changes.
   *
   * @param pcl The former listener that then no longer receives notifications from the server.
   */
  public void unsubscribe(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    pcs.removePropertyChangeListener(pcl);
  }


  @Override
  public void close() throws IOException {
    for (ClientHandler client : clients) {
      client.close();
    }
    if (socket != null && !socket.isClosed()) {
      socket.close();
    }
  }
}
