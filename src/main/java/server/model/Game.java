package server.model;

import java.util.HashSet;

import reversi.model.BitBoardReversi;
import reversi.model.Cell;
import reversi.model.Model;
import reversi.model.Player;

/**
 * A data structure that contains all necessary attributes to store a network game with all
 * information needed by the server.
 */
public class Game {

  private String creator;
  private String joiner;

  private Player creatorPlayer;

  private Model model;

  /**
   * Creates a new game instance where only creator and creator player are stored yet and the game
   * isn't yet running. The joiner and model have still to be set when a user joins the game and the
   * game is started by the server.
   *
   * @param creator The username of the creator of the game.
   * @param creatorPlayer The color the creator of the game chose to play.
   */
  Game(String creator, Player creatorPlayer) {
    this.creator = creator;
    this.creatorPlayer = creatorPlayer;
  }

  /**
   * Returns the username of the creator of the game.
   *
   * @return The username of the creator.
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Returns the username of the opponent of the user with the given username.
   *
   * @param name The username of the user to get the opponent of.
   * @return The opponent of the given user or <code>null</code> if the given user isn't part of
   *     this game or if it's the name of the creator and no one has joined the game yet.
   */
  public String getOpponent(String name) {
    if (name.equals(creator)) {
      return joiner;
    }
    if (name.equals(joiner)) {
      return creator;
    }
    return null;
  }

  /**
   * Returns the set of players in this game.
   *
   * @return The set of players in this game.
   */
  HashSet<String> getPlayers() {
    HashSet<String> players = new HashSet<>();
    players.add(creator);
    if (joiner != null) {
      players.add(joiner);
    }
    return players;
  }

  /**
   * Returns the color that the creator of the game chose to play.
   *
   * @return The color the creator of the game chose to play.
   */
  public Player getCreatorPlayer() {
    return creatorPlayer;
  }

  /**
   * Returns the player of the given user.
   *
   * @param user The user to get the player of.
   * @return The player of the given user.
   */
  private Player getPlayer(String user) {
    if (user.equals(creator)) {
      return creatorPlayer;
    }
    if (user.equals(joiner)) {
      return creatorPlayer.getOtherPlayer();
    }
    throw new AssertionError(user + " is not part of this game");
  }

  /**
   * Lets the user with the given username join the game and starts a game.
   *
   * @param joiner The username of the joining user.
   * @return <code>true</code> if joining was possible, <code>false</code> otherwise.
   */
  boolean join(String joiner) {
    if (isRunning()) {
      return false;
    }
    this.joiner = joiner;
    model = new BitBoardReversi();
    return true;
  }

  /**
   * Returns whether the game is running or not.
   *
   * @return <code>true</code> if the game is running, <code>false</code> otherwise.
   */
  public boolean isRunning() {
    return joiner != null;
  }

  /**
   * Places a stone on the given cell if the game is currently running and if it is a possible move
   * for the user wanting to make the move.
   *
   * @param cell The cell to place the stone on.
   * @param user The user that wants to make the move.
   * @return Whether the move was successful or not.
   */
  boolean place(Cell cell, String user) {
    if (!isRunning()) {
      return false;
    }
    if (model.getState().getCurrentPlayer() != getPlayer(user)) {
      return false;
    }
    return model.place(cell);
  }
}
