package server.model;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.net.Socket;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A class that handles a single socket for the server. Its main purpose is to manage the input and
 * output streams and forward any client input to the server.
 */
public class ClientHandler implements Closeable {

  private String username;
  private boolean approved = false;

  private Server server;
  private Socket socket;
  private ObjectOutputStream outputStream;

  /**
   * Creates a new client handler which handles the given socket and forwards any userinput to the
   * server.
   *
   * @param server The server the given socket is connected to.
   * @param connection The socket to manage.
   * @throws IOException if the socket has been closed or the streams can't be set up.
   */
  ClientHandler(Server server, Socket connection) throws IOException {
    this.server = server;
    socket = connection;
    outputStream = new ObjectOutputStream(connection.getOutputStream());
    Thread receivingThread = new Thread(() -> receiveStates(connection));
    receivingThread.setDaemon(true);
    receivingThread.start();
  }

  private void receiveStates(Socket from) {
    try (ObjectInputStream inputStream = new ObjectInputStream(from.getInputStream())) {
      Object received = inputStream.readObject();
      while (received instanceof String) {
        try {
          JSONObject receivedJson = new JSONObject((String) received);
          Iterator<String> keys = receivedJson.keys();
          while (keys.hasNext()) {
            server.handleInput(this, keys.next(), receivedJson);
          }
        } catch (JSONException e) {
          // The client sent something not in JSON-protocol, so it can't be a working reversi-client
          // and therefore the client should be disconnected from the server
          cleanUpConnection();
        }
        received = inputStream.readObject();
      }
    } catch (IOException e) {
      // client connection broke off, clean up
      cleanUpConnection();
    } catch (ClassNotFoundException e) {
      throw new AssertionError(e);
    }
  }

  /**
   * Sends a {@link JSONObject} to the client that is handled by the client handler.
   *
   * @param json The {@link JSONObject} to be send.
   */
  void sendJson(JSONObject json) {
    try {
      outputStream.writeObject(json.toString());
    } catch (IOException e) {
      // client connection broke off, clean up
      cleanUpConnection();
    }
  }

  /**
   * Returns the username of the client that is handled by the client handler. This method returns
   * null if the username hasn't been set yet.
   *
   * @return The username, if it has been set yet, <code>null</code> otherwise
   */
  String getUsername() {
    return username;
  }

  /**
   * Sets the username of the client that is handled by the client handler.
   *
   * @param username The username of the client.
   * @throws AssertionError If the username is already set.
   */
  void setUsername(String username) {
    if (this.username != null) {
      throw new AssertionError("Username is already set");
    }
    this.username = username;
    approved = true;
  }

  /**
   * Returns whether the client is approved or not. A client is approved as soon as it has a unique
   * username.
   *
   * @return <code>true</code> if the client is approved, <code>false</code> otherwise.
   */
  boolean isApproved() {
    return approved;
  }

  /**
   * Cleans up the connection of the server to this client.
   */
  void cleanUpConnection() {
    try {
      close();
    } catch (IOException e) {
      throw new AssertionError(e);
    }
    server.removeUser(this);
  }

  @Override
  public void close() throws IOException {
    if (!socket.isClosed()) {
      socket.close();
    }
  }
}
