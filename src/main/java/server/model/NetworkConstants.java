package server.model;

/**
 * An interface which contains all constants needed for the network functionality.
 */
public interface NetworkConstants {

  int PORT = 43200;

  int MINIMUM_USERNAME_LENGTH = 3;
  int MAXIMUM_USERNAME_LENGTH = 15;

  String NAME = "name";
  String NAME_REJECTED = "nameRejected";
  String NAME_APPROVED = "nameApproved";

  String UPDATE = "update";
  String LOBBY = "lobby";
  String GAMES = "games";

  String CREATE = "create";
  String CREATOR = "creator";
  String CREATOR_PLAYER = "creatorPlayer";
  String BLACK = "black";
  String WHITE = "white";

  String JOIN = "join";
  String JOIN_REJECTED = "joinRejected";

  String START_GAME = "startGame";
  String OPPONENT = "opponent";
  String PLAYER = "player";

  String MOVE = "move";
  String ROW = "row";
  String COLUMN = "column";

  String QUIT = "quit";
  String DISCONNECT = "disconnect";
  String LOST_CONNECTION = "Lost connection";
}
