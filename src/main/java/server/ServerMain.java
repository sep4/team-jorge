package server;

import javax.swing.SwingUtilities;

import server.view.ServerController;

/**
 * Main class of the server application. Its only purpose is to start the server application.
 */
public class ServerMain {

  /**
   * Invokes the starting of a {@link ServerController} on the <code>AWT event dispatching thread
   * </code>. This will then start the server and show the view.
   *
   * @param args The command line arguments.
   */
  public static void main(String[] args) {
    SwingUtilities.invokeLater(() -> new ServerController().start());
  }
}
