package reversi.model;

import java.util.Map;
import java.util.Optional;

/**
 * An interface for classes whose sole responsibility is the management of the stones
 * on the reversi board. As such it provides the data structure that allows to check 
 * and manipulate each entry on the board accordingly.
 */
public interface GameField {

  int SIZE = 8;

  /**
   * Returns an {@link Optional} that may contain a {@link Stone}, depending on if there is one
   * positioned on the cell.
   *
   * @param cell The cell to be checked.
   * @return An {@link Optional optional} containing the respective stone in case of success.
   */
  Optional<Stone> get(Cell cell);

  /**
   * Returns all {@link Cell cells} that are currently occupied by a stone.
   *
   * @return A map with all cells that have a stone on them.
   */
  Map<Cell, Player> getCellsOccupiedWithStones();

  /**
   * Checks a {@link Cell cell} whether it is occupied by a stone and in case of success, whether it
   * belongs to the respective {@link Player player}.
   *
   * @param player The player to be checked.
   * @param cell The cell that can contain a stone.
   * @return <code>true</code> if the player has a stone on the cell, <code>false</code> otherwise.
   */
  boolean isCellOfPlayer(Player player, Cell cell);

  /**
   * Checks a {@link Cell} if its column- and row-value is within the bounds. The valid range is
   * from 0 to {@link GameField#SIZE} for each.
   *
   * @param cell The cell to be checked.
   * @return <code>true</code> if within the bounds, <code>false</code> otherwise.
   */
  static boolean isWithinBounds(Cell cell) {
    return cell.getColumn() >= 0
        && cell.getColumn() < SIZE
        && cell.getRow() >= 0
        && cell.getRow() < SIZE;
  }
}
