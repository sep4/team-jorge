package reversi.model;

/** 
 * An enum class representing all different players in the reversi game. 
 */
public enum Player {

  WHITE("White"),
  BLACK("Black");

  private final String playerName;

  /**
   * Creates a new <code>Player</code>-object that takes a string argument for the internal
   * representation.
   *
   * @param playerName The string-representation of the constant.
   */
  Player(String playerName) {
    this.playerName = playerName;
  }

  /**
   * Returns the other player compared to this player.
   *
   * @return The other player.
   */
  public Player getOtherPlayer() {
    switch (this) {
      case WHITE:
        return BLACK;
      case BLACK:
        return WHITE;
      default:
        throw new AssertionError("Unknown player: " + this);
    }
  }

  @Override
  public String toString() {
    return playerName;
  }
}

