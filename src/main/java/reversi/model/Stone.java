package reversi.model;

/** 
 * A class representing a single stone of the reversi game including its corresponding color. 
 */
public class Stone {

  private final Player player;

  /**
   * Creates a new <code>Stone</code>-object that is owned by the specified player.
   *
   * @param player The owner of the stone.
   */
  Stone(Player player) {
    this.player = player;
  }

  /**
   * Returns the {@link Player} that is the owner of this <code>Stone</code>.
   *
   * @return The owning player.
   */
  public Player getPlayer() {
    return player;
  }
}
