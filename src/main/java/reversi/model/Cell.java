package reversi.model;

import java.util.Objects;

/**
 * A cell class that contains all necessary information for being represented on a 2D-board.
 */
public class Cell {

  private final int column;
  private final int row;

  /**
   * Create a new Cell with coordinates given in a numeric format.
   *
   * @param column The x-value of the board.
   * @param row The y-value of the board.
   */
  public Cell(int column, int row) {
    this.column = column;
    this.row = row;
  }

  /**
   * Returns the column of this cell as integer index.
   *
   * @return The column value of this cell, as integer index.
   */
  public int getColumn() {
    return column;
  }

  /**
   * Returns the row of this cell as integer index.
   *
   * @return The row of this cell, as integer index.
   */
  public int getRow() {
    return row;
  }

  @Override
  public int hashCode() {
    return Objects.hash(column, row);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof Cell)) {
      return false;
    }

    Cell other = (Cell) obj;
    return Objects.equals(column, other.column) && Objects.equals(row, other.row);
  }

  @Override
  public String toString() {
    return "(" + column + "," + row + ")";
  }
}

