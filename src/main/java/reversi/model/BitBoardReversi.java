package reversi.model;

import static java.util.Objects.requireNonNull;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.util.HashSet;
import java.util.Set;


/**
 * Implementation of a reversi class based on a bit board that provides all methods
 * necessary for an user-interface to play a game successfully.
 */
public class BitBoardReversi implements Model {
  private BitBoardState state;
  private final PropertyChangeSupport support = new PropertyChangeSupport(this);

  /**
   * Initializes a new Reversi-Game with an empty field.
   * The game is ready to be played immediately after.
   */
  public BitBoardReversi() {
    newGame();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public BitBoardState getState() {
    return this.state;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setState(GameState state) {
    if (!(state instanceof BitBoardState)) {
      throw new IllegalArgumentException(
          "Tried to set a non-BitBoardState on a BitBoardReversi instance.");
    }
    this.state = (BitBoardState) state;
    notifyListeners();
  }

  /**
   * Places the stone of the current player and flips all enclosed stones of the opponent.
   * Checks whether the move is possible and whether the game is over.
   *
   * @param cell The cell to place the stone.
   * @return <code>true</code> if the move can be executed successfully,
   *         <code>false</code> otherwise.
   */
  @Override
  public boolean place(Cell cell) {
    if (state.getCurrentPhase() != Phase.RUNNING) {
      return false;
    }

    if (!GameField.isWithinBounds(cell)) {
      return false;
    }

    int index = BitBoard.getIndexOfCell(cell);
    long moves = state.getField().getMoves(state.getCurrentPlayer());

    if (state.getField().getBit(moves, index) == 1) {
      state.getField().update(state.getCurrentPlayer(), index);

      if (checkWinningCondition()) {
        state.setCurrentPhase(Phase.FINISHED);
        state.updateWinner();
      } else {
        state.switchPlayer();
      }

      notifyListeners();
      return true;
    }

    return false;
  }

  /**
   * Checks whether a winning condition is fulfilled.
   * The game is over if a player can not make any move.
   *
   * @return <code>true</code> if the game is over, <code>false</code> otherwise
   */
  boolean checkWinningCondition() {
    Player otherPlayer = state.getCurrentPlayer().getOtherPlayer();
    return state.getField().getMoves(otherPlayer) == 0L;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void addPropertyChangeListener(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    support.addPropertyChangeListener(pcl);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removePropertyChangeListener(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    support.removePropertyChangeListener(pcl);
  }

  /**
   * Invokes the firing of an event, such that any attached observer (i.e., {@link
   * PropertyChangeListener}) is notified that a change happened to this model.
   */
  protected void notifyListeners() {
    String propertyName;
    switch (state.getCurrentPhase()) {
      case RUNNING:
        propertyName = STATE_CHANGED;
        break;
      case FINISHED:
        propertyName = GAME_ENDED;
        break;
      default:
        throw new AssertionError("Unhandled phase: " + state.getCurrentPhase());
    }
    support.firePropertyChange(propertyName, null, this);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<Cell> getPossibleCellsForPlayer(Player player) {
    HashSet<Cell> cells = new HashSet<>();
    long moves = state.getField().getMoves(player);

    for (int row = 0; row < BitBoard.REQUIRED_SIZE; ++row) {
      for (int column = 0; column < BitBoard.REQUIRED_SIZE; ++column) {
        Cell cell = new Cell(column, row);
        if (state.getField().getBit(moves, BitBoard.getIndexOfCell(cell)) != 0L) {
          cells.add(cell);
        }
      }
    }

    return cells;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void newGame() {
    state = new BitBoardState();
    notifyListeners();
  }
}
