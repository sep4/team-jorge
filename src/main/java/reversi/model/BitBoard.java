package reversi.model;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Optional;

/**
 * Implementation of a bitBord class that handles the logic for the reversi game. 
 * This class provides an highly efficient data structure that allows to check and 
 * manipulate each entry on the board accordingly.
 * Following variables are used throughout this class:
 * bitsW    = stones belonging to White encoded as a long.
 * bitsB    = stones belonging to Black encoded as a long.
 */
public class BitBoard implements GameField, Cloneable {
  /*
   * Example. White's Board:
   * 00000000
   * 00000000
   * 00000000
   * 00010000
   * 00001000
   * 00000000
   * 00000000
   * 00000000
   * Obviously that's 64 bits, so a long can be used.
   * The stones in the middle can be represented by the following long:
   * long bitsW = 0000000000000000000000000001000000001000000000000000000000000000
   */
  private long bitsW = 0L;
  private long bitsB = 0L;

  /** The bitBoard only works for a game field with size 8. */
  public static final int REQUIRED_SIZE = 8;

  /**
   * After accounting for the enclosing stones of the current player, and already doing one shift,
   * the amount of further shifts possible. A shift checks for whether an opponent's stone is in the
   * direction of the shift. In the case of the game field having a size of 8, that results in a
   * maximum of 6 shifts as only a maximum of 6 opponent stones can be enclosed by two stones of the
   * player.
   */
  private static final int MAX_SHIFTS = REQUIRED_SIZE - 2;

  private static final int BEGINNING_PIECES = 4;

  /**
   * Returns the white bit board.
   *
   * @return The white bit board.
   */
  public long getBitsW() {
    return bitsW;
  }

  /**
   * Returns the black bit board.
   *
   * @return The black bit board.
   */
  public long getBitsB() {
    return bitsB;
  }

  /**
   * Sets the white bit board.
   *
   * @param bitsW The white board encoded as along.
   */
  public void setBitsW(long bitsW) {
    this.bitsW = bitsW;
  }

  /**
   * Sets the black bit board.
   *
   * @param bitsB The black board encoded as a long.
   */
  public void setBitsB(long bitsB) {
    this.bitsB = bitsB;
  }

  /*
   * The masks prevent line shifts.
   * As example the west mask prevents that the leftmost bits of the board are
   * shifted in the next line, so the leftmost bit is a zero.
   *
   * WEST_MASK   EAST_MASK
   * 01111111    11111110
   * 01111111    11111110
   * 01111111    11111110
   * 01111111    11111110
   * 01111111    11111110
   * 01111111    11111110
   * 01111111    11111110
   * 01111111    11111110
   */
  private static final long WEST_MASK =
      0b0111111101111111011111110111111101111111011111110111111101111111L;
  private static final long EAST_MASK =
      0b1111111011111110111111101111111011111110111111101111111011111110L;

  /*
   * The MIDDLE_MASK is needed for the first four moves.
   * MIDDLE_MASK
   * 00000000
   * 00000000
   * 00000000
   * 00011000
   * 00011000
   * 00000000
   * 00000000
   * 00000000
   */
  private static final long MIDDLE_MASK =
      0b0000000000000000000000000001100000011000000000000000000000000000L;

  /**
   * Cache all shift functions. Mapping of direction to its respective shift function, e.g. North
   * maps to shifting upwards (North).
   */
  final Hashtable<Direction, IShift> shiftTable = new Hashtable<>();

  /** Creates a new BitBoard and fills the shiftTable. */
  public BitBoard() {
    if (GameField.SIZE != REQUIRED_SIZE) {
      throw new IllegalStateException(
          String.format(
              "The BitBoard game field only works with size %s. Current size is %s.",
              REQUIRED_SIZE, GameField.SIZE));
    }

    shiftTable.put(Direction.N, l -> l << REQUIRED_SIZE);
    shiftTable.put(Direction.S, l -> l >>> REQUIRED_SIZE);
    shiftTable.put(Direction.E, l -> (EAST_MASK & l) >>> 1);
    shiftTable.put(Direction.W, l -> (WEST_MASK & l) << 1);
    shiftTable.put(
        Direction.NE, l -> shiftTable.get(Direction.N).call(shiftTable.get(Direction.E).call(l)));
    shiftTable.put(
        Direction.NW, l -> shiftTable.get(Direction.N).call(shiftTable.get(Direction.W).call(l)));
    shiftTable.put(
        Direction.SE, l -> shiftTable.get(Direction.S).call(shiftTable.get(Direction.E).call(l)));
    shiftTable.put(
        Direction.SW, l -> shiftTable.get(Direction.S).call(shiftTable.get(Direction.W).call(l)));
  }

  /**
   * Flips a bit at the index the player wants to set his stone. As example the player wants to set
   * a stone at Cell(3,4), then the bit at index 8 * 4 + 3 = 35 in the long will be flipped from 0
   * to 1.
   *
   * @param player Current player.
   * @param index The index where the bit has to be flipped.
   * @return the flipped board.
   */
  public long flipBit(Player player, int index) {
    if (player == Player.WHITE) {
      return bitsW = (bitsW ^ (1L << ((int) (Math.pow(REQUIRED_SIZE, 2) - 1) - index)));
    } else {
      return bitsB = (bitsB ^ (1L << ((int) (Math.pow(REQUIRED_SIZE, 2) - 1) - index)));
    }
  }

  /**
   * Checks given direction for possible moves. Therefore the bits/stones of the current player with
   * opponent bits/stones in one direction, i.e. North, are regarded. It will be stopped if there is
   * an empty cell after the opponents. This empty cell is a possible candidate for a move. All
   * possible moves in one direction are encoded as a long, where 1 represents a candidate.
   *
   * @param bitsP Stones belonging to current player.
   * @param bitsO Stones belonging to their opponent.
   * @param direction Given direction.
   * @return possible moves.
   */
  long lineCapMoves(long bitsP, long bitsO, Direction direction) {
    long empty = ~(bitsP | bitsO);
    long moves = 0L;

    /*
     * Check immediate neighbor of each stone of the player (bitsP) in the given direction,
     * whether a stone of the opponent is there.
     * This marks the start for the search of a coordinate that would enclose one or more
     * stones of the opponent with a stone of the player.
     */
    long candidates = shiftTable.get(direction).call(bitsP) & bitsO;

    /*
     * Continue in the same manner as above until no candidates are left.
     * Add all candidate moves to the moves long.
     */
    while (candidates != 0L) {
      moves |= shiftTable.get(direction).call(candidates) & empty;
      candidates = shiftTable.get(direction).call(candidates) & bitsO;
    }

    return moves;
  }

  /**
   * For getting all possible moves all directions have to be checked for candidates. A special case
   * is given at the beginning of the game where players are only allowed to place stones in the
   * middle of the game board. This case is checked also here.
   *
   * @param player The current Player.
   * @return moves.
   */
  public long getMoves(Player player) {
    long bitsP = this.getBitsW();
    long bitsO = this.getBitsB();

    if (player == Player.BLACK) {
      bitsP = this.getBitsB();
      bitsO = this.getBitsW();
    }

    if ((countPieces(Player.WHITE) + countPieces(Player.BLACK)) < BEGINNING_PIECES) {
      return ~(bitsP | bitsO) & MIDDLE_MASK;
    }

    long moves = 0L;

    for (Direction direction : Direction.values()) {
      moves |= lineCapMoves(bitsP, bitsO, direction);
    }

    return moves;
  }

  /**
   * Shifts the bits by index to the right. So the bit at index is the right most bit. bits =
   * 00000000 00010010 index = 4 bits >> index = 00000000 00000001 00000000 00000001 & 00000000
   * 00000001 = 1
   *
   * @param bits The current bits.
   * @param index The index for right shifting.
   * @return 1 if the bit is set, otherwise 0.
   */
  public long getBit(long bits, int index) {
    return ((bits >> ((int) (Math.pow(REQUIRED_SIZE, 2) - 1) - index)) & 1L);
  }

  /**
   * Changes inplace the boards and flips the opponent bits of last move in one direction.
   *
   * @param player Current player.
   * @param cell Cell of the last move.
   * @param direction Direction in which has to be flipped.
   */
  void flipEnemyNeighboursOfLastMove(Player player, Cell cell, Direction direction) {
    flipEnemyNeighboursOfLastMove(player, getIndexOfCell(cell), direction);
  }

  /**
   * Changes inplace the boards and flips the opponent bits of last move in one direction.
   *
   * @param player Current player.
   * @param index Index of the last move.
   * @param direction Direction in which has to be flipped.
   */
  void flipEnemyNeighboursOfLastMove(Player player, int index, Direction direction) {
    long bitsM = (1L << ((int) (Math.pow(REQUIRED_SIZE, 2) - 1) - index));

    long bitsP = this.getBitsW();
    long bitsO = this.getBitsB();

    if (player == Player.BLACK) {
      bitsP = this.getBitsB();
      bitsO = this.getBitsW();
    }

    bitsP |= bitsM;

    long flip = shiftTable.get(direction).call(bitsM) & bitsO;

    // Since we've already done the first shift above, only loop for MAX_SHIFTS - 1.
    for (int i = 0; i < (MAX_SHIFTS - 1); i++) {
      flip |= shiftTable.get(direction).call(flip) & bitsO;
    }

    if ((shiftTable.get(direction).call(flip) & bitsP) != 0L) {
      bitsO &= ~flip;
      bitsP |= flip;
    }

    if (player == Player.WHITE) {
      this.setBitsW(bitsP);
      this.setBitsB(bitsO);
    } else {
      this.setBitsW(bitsO);
      this.setBitsB(bitsP);
    }
  }

  /**
   * Flips opponent stones of all eight directions of the cell.
   *
   * @param player Current player.
   * @param cell Cell of last move.
   */
  void flipInAllDirections(Player player, Cell cell) {
    flipInAllDirections(player, getIndexOfCell(cell));
  }

  /**
   * Flips opponent stones of all eight directions of the cell.
   *
   * @param player Current player.
   * @param index Index of last move.
   */
  void flipInAllDirections(Player player, int index) {
    for (Direction direction : Direction.values()) {
      flipEnemyNeighboursOfLastMove(player, index, direction);
    }
  }

  /**
   * Counts the pieces (stones) a given player has on the board.
   *
   * @param player Given player.
   * @return Number of pieces of given player.
   */
  public int countPieces(Player player) {
    if (player == Player.WHITE) {
      return Long.bitCount(bitsW);
    } else {
      return Long.bitCount(bitsB);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<Stone> get(Cell cell) {
    long bitW = getBit(bitsW, getIndexOfCell(cell));
    long bitB = getBit(bitsB, getIndexOfCell(cell));

    if (bitW != 0L) {
      return Optional.of(new Stone(Player.WHITE));
    } else if (bitB != 0L) {
      return Optional.of(new Stone(Player.BLACK));
    } else {
      return Optional.empty();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Map<Cell, Player> getCellsOccupiedWithStones() {
    Map<Cell, Player> cellToPlayer = new HashMap<>();

    for (int row = 0; row < REQUIRED_SIZE; ++row) {
      for (int column = 0; column < REQUIRED_SIZE; ++column) {
        Cell cell = new Cell(column, row);
        Optional<Stone> optionalStone = get(cell);
        if (optionalStone.isPresent()) {
          Stone stone = optionalStone.get();
          cellToPlayer.put(cell, stone.getPlayer());
        }
      }
    }

    return cellToPlayer;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isCellOfPlayer(Player player, Cell cell) {
    Optional<Stone> optionalStone = get(cell);
    if (optionalStone.isPresent()) {
      Stone stone = optionalStone.get();
      return stone.getPlayer() == player;
    }

    return false;
  }

  /**
   * Converts the board coordinates to an index. As example the Cell(5, 4) would be at index 3 * 4 +
   * 5 = 17 of the bit board encoded as a long.
   *
   * @param cell The cell.
   * @return The index for flipping.
   */
  public static int getIndexOfCell(Cell cell) {
    return REQUIRED_SIZE * cell.getRow() + cell.getColumn();
  }

  /**
   * Convert an index to a cell.
   *
   * @param index The index.
   * @return The Cell.
   */
  public static Cell getCellAtIndex(int index) {
    int row = index / REQUIRED_SIZE;
    int column = index % REQUIRED_SIZE;
    return new Cell(column, row);
  }

  /**
   * Updates a field by flipping the bit of a given cell to the color of a given player. Also, flips
   * all neighbors.
   *
   * @param player The given player.
   * @param index The index of a given cell.
   */
  public void update(Player player, int index) {
    this.flipBit(player, index);
    this.flipInAllDirections(player, index);
  }

  @Override
  public BitBoard clone() {
    try {
      super.clone();
    } catch (CloneNotSupportedException ex) {
      System.out.println("Error: Could not clone BitBoard.");
    }
    BitBoard bitBoard = new BitBoard();
    bitBoard.setBitsB(this.bitsB);
    bitBoard.setBitsW(this.bitsW);
    return bitBoard;
  }
}
