package reversi.model;

import java.util.Optional;
import java.util.Set;

/**
 * An interface for a data structure that contains all necessary attributes in order to successfully
 * play a game.
 */
public interface GameState {

  /**
   * Returns the current phase of the game.
   *
   * @return The current phase.
   */
  Phase getCurrentPhase();

  /**
   * Returns the {@link GameField game field} that stores the data for each cell of the
   * reversi-board.
   *
   * @return The current game field.
   */
  GameField getField();

  /**
   * Returns the player that is currently allowed to make a move.
   *
   * @return The current player.
   */
  Player getCurrentPlayer();

  /**
   * Returns the winner of the current game. This method may only be called if the current game is
   * finished.
   *
   * @return {@link Optional#empty()} if the game's a draw. Otherwise an optional that contains the
   *     winner.
   */
  Optional<Player> getWinner();

  /**
   * Returns all {@link Cell cells} of the current reversi board that belong to the requested
   * player.
   *
   * @param player The player whose cells are to be retrieved.
   * @return A set of cells on which the player has currently stones upon.
   */
  Set<Cell> getAllCellsOfPlayer(Player player);
}
