package reversi.model;

/** 
 * An enum class that provides constants that represent the possible stages of this chess game. 
 */
public enum Phase {
  WAITING,
  RUNNING,
  FINISHED,
  ;
}
