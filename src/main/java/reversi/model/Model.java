package reversi.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import java.util.Set;

/**
 * The main interface of the reversi model. It provides all necessary methods for accessing and
 * manipulating the data such that a game can be played successfully.
 *
 * <p>When something changes in the model, the model notifies its observers by firing a {@link
 * PropertyChangeEvent change-event}.
 */
public interface Model {

  /** A property name used to notify the listeners when the state changed. */
  String STATE_CHANGED = "State changed";
  /** A property name used to notify the listener when the game has ended. */
  String GAME_ENDED = "Game ended";

  /**
   * Adds a {@link PropertyChangeListener} to the model that will be notified about the changes made
   * to the reversi board.
   *
   * @param pcl The listener.
   */
  void addPropertyChangeListener(PropertyChangeListener pcl);

  /**
   * Removes a listener from the model, which will then no longer be notified about any events in
   * the model.
   *
   * @param pcl The former listener that then no longer receives notifications from the model.
   */
  void removePropertyChangeListener(PropertyChangeListener pcl);

  /**
   * Returns the {@link GameState} specific to this class.
   *
   * @return The <code>GameState</code>-object.
   */
  GameState getState();

  /**
   * Overwrite the current state of the game with the given one.
   *
   * @param state The new state of the calling model instance.
   */
  void setState(GameState state);

  /** Resets the game so that everything is in its initial state afterwards. */
  void newGame();

  /**
   * Places a stone on the given cell and deals with the consequences. Placing a stone only works if
   * there is currently a game running and if placing the stone on the cell is a valid reversi move.
   * After the move, it will be the turn of the next player, unless he can't place a stone on any
   * cell and the game is over.
   *
   * @param cell The cell to set the stone on.
   * @return <code>true</code> if the stone was successfully placed, <code>false</code> otherwise.
   */
  boolean place(Cell cell);

  /**
   * Computes all cells the given player can set his next stone on.
   *
   * @param player The {@link Player player} that is up next to place a stone.
   * @return A set of all possible cells for the given player to place a stone on.
   */
  Set<Cell> getPossibleCellsForPlayer(Player player);
}

