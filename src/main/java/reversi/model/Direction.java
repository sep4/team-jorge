package reversi.model;

/**
 * Provides the direction of the eight neighbours around a cell.
 * North, East, South, West, NorthEast, SouthEast, SouthWest, NorthWest
 */
public enum Direction {
  N,
  E,
  S,
  W,
  NE,
  SE,
  SW,
  NW,
}
