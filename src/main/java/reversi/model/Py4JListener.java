package reversi.model;

import reversi.model.BitBoardState;

public interface Py4JListener {
  int notify(BitBoardState source);
}
