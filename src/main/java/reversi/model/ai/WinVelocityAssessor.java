package reversi.model.ai;

import reversi.model.BitBoardState;
import reversi.model.Phase;
import reversi.model.Player;

/**
 * Assessment class that rates the current {@link BitBoardState} by evaluating whether any of the
 * players have won the game.
 *
 * <p>If the game is not yet over, a neutral value of zero points is returned. Otherwise, a constant
 * value of {@link WinVelocityAssessor#WINNING_SCORE 10000} will be taken and divided by the
 * amount of steps necessary in order to reach this state. Depending on the player, the value is
 * positive for {@link BitBoardAI#AI_PLAYER} and negative for the human player.
 *
 * {@link WinVelocityAssessor#WINNING_SCORE 10000} is chosen arbitrarily since it is large
 * enough, such that the tree's inner nodes have utilities that fall within this range. It is also
 * the largest reward since winning carries more weight than the other factors.</p>
 */
public class WinVelocityAssessor implements BitBoardStateAssessor {
  private static final double WINNING_SCORE = 10000.0;

  /**
   * {@inheritDoc}
   */
  @Override
  public double computeValue(BitBoardState state, int depth) {
    if (state.getCurrentPhase() != Phase.FINISHED) {
      return 0;
    }

    Player winner = state.getWinner().orElse(null);

    if (winner == BitBoardAI.AI_PLAYER.getOtherPlayer()) {
      return -MODIFIER * WINNING_SCORE / depth;
    } else if (winner == BitBoardAI.AI_PLAYER) {
      return WINNING_SCORE / depth;
    } else {
      return (WINNING_SCORE / 2) / depth;
    }
  }
}
