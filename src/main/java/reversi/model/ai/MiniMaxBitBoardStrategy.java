package reversi.model.ai;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import reversi.model.BitBoard;
import reversi.model.BitBoardReversi;
import reversi.model.BitBoardState;
import reversi.model.Cell;
import reversi.model.GameState;
import reversi.model.Model;
import reversi.model.Phase;
import reversi.model.Player;

/**
 * A BitBoard-specific implementation of the minimax algorithm. It allows to compute a move for
 * reversi, in which the potential loss in a worst-case-scenario is minimized. This means for the
 * ai-player ({@link Player#WHITE}) that it takes the best possible move out of all potential moves
 * that the opponent ({@link Player#BLACK} has forced upon the ai-player.
 */
public class MiniMaxBitBoardStrategy implements Strategy {

  private final int lookAhead;

  private final BitBoardStateAssessor assessor = new ReversiAssessor();

  private final Model model = new BitBoardReversi();
  private Cell savedCell;
  private final boolean randomized;

  /**
   * Creates a new instance of the {@link MiniMaxBitBoardStrategy}.
   *
   * @param lookAhead An int value indicating how many moves the algorithm looks ahead.
   * @param randomized A boolean value indicating whether the algorithm will choose randomly or
   *     deterministic if there are multiple best moves. If <code>true</code> it chooses randomly.
   */
  public MiniMaxBitBoardStrategy(int lookAhead, boolean randomized) {
    if (lookAhead <= 0) {
      throw new IllegalArgumentException("The look-ahead has to be greater than 0");
    }
    this.lookAhead = lookAhead;
    this.randomized = randomized;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Cell calculateMove(GameState state) {
    if (!(state instanceof BitBoardState)) {
      throw new IllegalArgumentException(
          "The method calculateMove in the BitBoard-specific class MiniMaxBitBoard "
              + "was called with a non-BitBoardState");
    }
    if (state.getCurrentPhase() == Phase.FINISHED) {
      throw new IllegalArgumentException(
          "The method calculateMove was called although the game wasn't running anymore");
    }
    Player player = state.getCurrentPlayer();
    if (player != BitBoardAI.AI_PLAYER) {
      throw new IllegalArgumentException(
          "The method calculateMove was called although it wasn't the computer's turn");
    }
    savedCell = null;
    miniMax((BitBoardState) state, 0);
    return savedCell;
  }

  /**
   * Computes the best next move for the computer in the given {@link BitBoardState state} at the
   * given depth of the imaginary game tree. This method simulates a real game tree by using
   * recursion and saves the best move when at depth 0.
   *
   * @param state The state to evaluate.
   * @param depth The depth in the game tree.
   * @return The value of the given state using the min-max-algorithm.
   */
  private double miniMax(BitBoardState state, int depth) {
    double currentHeuristic =
        assessor.computeValue(state, depth);
    if (state.getCurrentPhase() == Phase.FINISHED || depth >= lookAhead) {
      return currentHeuristic;
    }
    Player player = state.getCurrentPlayer();
    boolean isMax = (player == BitBoardAI.AI_PLAYER);

    double value;
    if (isMax) {
      value = -Double.MAX_VALUE;
    } else {
      value = Double.MAX_VALUE;
    }
    List<Cell> currentBestCells = new ArrayList<>();
    long possibleMoves = state.getField().getMoves(player);
    for (int i = 0; i < Math.pow(BitBoard.REQUIRED_SIZE, 2); i++) {
      BitBoardState copiedState = state.clone();
      model.setState(copiedState);
      if (state.getField().getBit(possibleMoves, i) == 1) {
        Cell currentCell = BitBoard.getCellAtIndex(i);
        model.place(currentCell);
        double score = miniMax(copiedState, depth + 1) + currentHeuristic;
        if (isMax && Double.compare(score, value) > 0) {
          value = score;
          currentBestCells.clear();
          currentBestCells.add(currentCell);
        } else if (!isMax && Double.compare(score, value) < 0) {
          value = score;
          currentBestCells.clear();
          currentBestCells.add(currentCell);
        } else if (randomized && Double.compare(score, value) == 0) {
          currentBestCells.add(currentCell);
        }
      }
    }
    Cell chosenCell;
    if (randomized) {
      Random random = new Random();
      chosenCell = currentBestCells.get(random.nextInt(currentBestCells.size()));
    } else {
      if (currentBestCells.size() != 1) {
        throw new IllegalStateException("Deterministic MiniMax calculated multiple best cells");
      }
      chosenCell = currentBestCells.get(0);
    }
    if (depth == 0) {
      savedCell = chosenCell;
    }
    return value;
  }
}
