package reversi.model.ai;

import java.util.ArrayList;
import java.util.List;

import py4j.GatewayServer;

import reversi.model.BitBoard;
import reversi.model.BitBoardReversi;
import reversi.model.BitBoardState;
import reversi.model.Cell;
import reversi.model.GameState;
import reversi.model.Py4JListener;


public class DeepQlearningStrategy implements Strategy {
  private List<Py4JListener> listeners = new ArrayList<>();

  /**
   * The deep reinforcement learning strategy.
   *
   * @param listeners The listeners for the Python connection.
   */
  public DeepQlearningStrategy(List<Py4JListener> listeners) {
    this.listeners = listeners;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Cell calculateMove(GameState state) {
    BitBoardState bitBoardState = ((BitBoardState) state).clone();
    for (Py4JListener listener : listeners) {
      int returnValue = listener.notify(bitBoardState);
      return BitBoard.getCellAtIndex(returnValue);
    }
    return null;
  }
}
