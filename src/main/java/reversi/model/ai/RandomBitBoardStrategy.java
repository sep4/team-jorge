package reversi.model.ai;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import reversi.model.BitBoard;
import reversi.model.BitBoardReversi;
import reversi.model.BitBoardState;
import reversi.model.Cell;
import reversi.model.GameState;
import reversi.model.Model;
import reversi.model.Player;

/**
 * An implementation of a random strategy where the cell in which a stone is to be placed
 * is picked at random from a set of possible moves.
 */
public class RandomBitBoardStrategy implements Strategy {
  private final Model model = new BitBoardReversi();

  /**
   {@inheritDoc}
   */
  @Override
  public Cell calculateMove(GameState state) {
    BitBoardState bitBoardState = ((BitBoardState) state).clone();
    model.setState(bitBoardState);
    Random random = new Random();

    List<Cell> possibleMoves = getPossibleMoves(bitBoardState);

    return possibleMoves.get(random.nextInt(possibleMoves.size()));
  }

  private List<Cell> getPossibleMoves(BitBoardState bitBoardState) {
    Player player = bitBoardState.getCurrentPlayer();
    long moves = bitBoardState.getField().getMoves(player);

    List<Cell> possibleMoves = new ArrayList<>();
    for (int index = 0; index < Math.pow(BitBoard.REQUIRED_SIZE, 2); ++index) {
      if (bitBoardState.getField().getBit(moves, index) == 1) {
        Cell cell = BitBoard.getCellAtIndex(index);
        possibleMoves.add(cell);
      }
    }

    return possibleMoves;
  }
}
