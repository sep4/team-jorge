package reversi.model.ai;

import reversi.model.BitBoard;
import reversi.model.BitBoardState;
import reversi.model.Cell;
import reversi.model.GameState;

/**
 * An implementation of the Alpha-Beta-Pruning algorithm.
 */
public class AlphaBetaBitBoardStrategy implements Strategy {
  private int cachedBestMoveIdx;
  private static final int DEPTH = 7;

  private final BitBoardStateAssessor assessor = new ReversiAssessor();

  /**
   * Initializes an Alpha-Beta BitBoard.
   */
  public AlphaBetaBitBoardStrategy() {
  }

  /**
   * Calculates the best move in the given position using the Alpha-Beta pruning algorithm.
   * Then, executes the move on the reversi object.
   *
   * @return The move this strategy executed.
   */
  public Cell calculateMove(GameState state) {
    BitBoardState bitBoardState = ((BitBoardState) state).clone();
    alphabeta(bitBoardState, DEPTH, -Float.MAX_VALUE, Float.MAX_VALUE, true);

    return BitBoard.getCellAtIndex(cachedBestMoveIdx);
  }

  /**
   * Returns whether a given game state is terminal, i.e. it is a win or a remis.
   *
   * @param node The game state.
   * @return Whether the state is terminal.
   */
  private boolean isNodeTerminal(BitBoardState node) {
    return node.getField().getMoves(node.getCurrentPlayer()) == 0L;
  }

  /**
   * Calculates Alpha-Beta-Pruning.
   *
   * @param node The game state.
   * @param depth How deep to calculate for.
   * @param alpha The first parameter for the algorithm.
   * @param beta The second parameter for the algorithm.
   * @param maximizingPlayer Whether the AI is currently being maximized or the player.
   * @return The value of the best move.
   */
  private double alphabeta(
      BitBoardState node,
      int depth,
      double alpha,
      double beta,
      boolean maximizingPlayer) {
    if (depth == 0 || isNodeTerminal(node)) {
      return assessor.computeValue(node, depth);
    }

    if (maximizingPlayer) {
      double value = alpha;
      long moves = node.getField().getMoves(node.getCurrentPlayer());

      for (int index = 0; index < Math.pow(BitBoard.REQUIRED_SIZE, 2); ++index) {
        final BitBoardState cachedState = node.clone();
        if (node.getField().getBit(moves, index) != 1) {
          continue;
        }

        node.getField().update(node.getCurrentPlayer(), index);
        node.switchPlayer();

        double currValue = alphabeta(node.clone(), depth - 1, alpha, beta, false);
        if (currValue > value) {
          value = currValue;

          if (depth == DEPTH) {
            cachedBestMoveIdx = index;
          }
        }

        alpha = Math.max(alpha, value);

        /*
         * Resets BitBoardState so the next iteration of the current loop works on a clean state.
         */
        node = cachedState;

        if (alpha >= beta) {
          break;
        }
      }

      return value;
    } else {
      double value = beta;
      long moves = node.getField().getMoves(node.getCurrentPlayer());

      for (int index = 0; index < Math.pow(BitBoard.REQUIRED_SIZE, 2); ++index) {
        final BitBoardState cachedState = node.clone();
        if (node.getField().getBit(moves, index) != 1) {
          continue;
        }

        node.getField().update(node.getCurrentPlayer(), index);
        node.switchPlayer();

        double currValue = alphabeta(node.clone(), depth - 1, alpha, beta, true);
        if (currValue < value) {
          value = currValue;
        }

        beta = Math.min(beta, value);

        /*
         * Resets BitBoardState so the next iteration of the current loop works on a clean state.
         */
        node = cachedState;

        if (alpha > beta) {
          break;
        }
      }

      return value;
    }
  }
}
