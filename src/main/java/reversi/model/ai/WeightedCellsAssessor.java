package reversi.model.ai;

import reversi.model.BitBoardState;

/**
 * Assessment class that rates the {@link BitBoardState} by evaluating the number of corner-,
 * X- and C-cells owned by each of the players.
 *
 * <p>The best possible score can be achieved by {@link BitBoardAI#AI_PLAYER} owning more corners
 *  and less X and C cells, and the human player owning fewer corners and more X and C cells.
 */
public class WeightedCellsAssessor implements BitBoardStateAssessor {
  /*
   * The masks help to identify corner-, x - and c-cells.
   * Corner cells are the most important since the stones placed here are stable i.e
   * cannot be flipped.
   * X and C cells should be avoided, since they give the opponent the opportunity to occupy
   * the corner squares.
   *
   * CORNER_MASK   X_MASK      C_MASK
   *  10000001    00000000    01000010
   *  00000000    01000010    10000001
   *  00000000    00000000    00000000
   *  00000000    00000000    00000000
   *  00000000    00000000    00000000
   *  00000000    00000000    00000000
   *  00000000    01000010    10000001
   *  10000001    00000000    01000010
   */
  private static final long CORNER_MASK =
      0b1000000100000000000000000000000000000000000000000000000010000001L;
  private static final long X_MASK =
      0b0000000001000010000000000000000000000000000000000100001000000000L;
  private static final long C_MASK =
      0b0100001010000001000000000000000000000000000000001000000101000010L;
  private static final double CORNER_WEIGHT = 100.0;
  private static final double X_WEIGHT = -20.0;
  private static final double C_WEIGHT = -40.0;

  /**
   * {@inheritDoc}
   */
  @Override
  public double computeValue(BitBoardState state, int depth) {
    long bitsW = state.getField().getBitsW();
    long bitsB = state.getField().getBitsB();

    double aiCorners = Long.bitCount(bitsW & CORNER_MASK) * CORNER_WEIGHT;
    double humanCorners = Long.bitCount(bitsB & CORNER_MASK) * CORNER_WEIGHT;

    double aiXcells = Long.bitCount(bitsW & X_MASK) * X_WEIGHT;
    double humanXcells = Long.bitCount(bitsB & X_MASK) * X_WEIGHT;

    double aiCcells = Long.bitCount(bitsW & C_MASK) * C_WEIGHT;
    double humanCcells = Long.bitCount(bitsB & C_MASK) * C_WEIGHT;

    double weightedAiBits = aiCorners + aiXcells + aiCcells;
    double weightedHumanBits = humanCorners + humanXcells + humanCcells;

    return weightedAiBits - MODIFIER * weightedHumanBits;
  }
}
