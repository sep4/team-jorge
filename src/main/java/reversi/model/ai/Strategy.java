package reversi.model.ai;

import reversi.model.Cell;
import reversi.model.GameState;

/**
 * The interface for AI-strategies so that the reversi game can use different AIs. 
 * This helps to implement the Strategy-pattern.
 */
public interface Strategy {
  /**
   * Calculates the best move for the AI in the given state.
   * This method only works if the game is running and it is the AI's turn.
   *
   * @param state The game state to calculate the best move for.
   * @return The best cell for the next move according to the calculation.
   */
  Cell calculateMove(GameState state);
}
