package reversi.model.ai;

import py4j.GatewayServer;
import reversi.model.Player;

/**
 * The entry point for Py4J.
 */
public class Entry {
  /**
   * Main method.
   */
  public static void main(String[] args) {
    Entry app = new Entry();
    GatewayServer server = new GatewayServer(app);
    server.start();
  }
}
