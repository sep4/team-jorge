package reversi.model.ai;

import reversi.model.BitBoard;
import reversi.model.BitBoardState;

/**
 * Assessment class that rates the {@link BitBoardState} by evaluating the number of pawns on the
 * {@link BitBoard}.
 *
 * <p>In this implementation, the best score can be achieved by {@link BitBoardAI#AI_PLAYER}
 * (the ai-player) having as many stones as possible on the field, and the human player
 * simultaneously having as few as stones as possible.
 */
public class StoneCountAssessor implements BitBoardStateAssessor {

  /**
   * {@inheritDoc}
   */
  @Override
  public double computeValue(BitBoardState state, int depth) {
    int humanPieces = state.getField().countPieces(BitBoardAI.AI_PLAYER.getOtherPlayer());
    int aiPieces = state.getField().countPieces(BitBoardAI.AI_PLAYER);

    return aiPieces - MODIFIER * humanPieces;
  }
}

