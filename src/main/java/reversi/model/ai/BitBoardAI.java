package reversi.model.ai;

import reversi.model.BitBoardReversi;
import reversi.model.Cell;
import reversi.model.Phase;
import reversi.model.Player;

/**
 * Implements a single player-version of the game. It includes an AI strategy that can be specified
 * by the user. An example could be MiniMax or Alpha-Beta Pruning.
 */
public class BitBoardAI extends BitBoardReversi {
  Strategy strategy;

  /**
   * The white player is the AI.
   */
  public static final Player AI_PLAYER = Player.WHITE;

  /**
   * Initializes the model with a given AI strategy.
   *
   * @param strategy The strategy chosen by the user.
   */
  public BitBoardAI(Strategy strategy) {
    super();
    this.strategy = strategy;
  }

  /**
   * {@inheritDoc}
   * This method immediately computes and executes the ai-player's move after a successful move of
   * the user if the game is still running.
   */
  @Override
  public boolean place(Cell cell) {
    if (!super.place(cell)) {
      return false;
    }

    if (this.getState().getCurrentPhase() == Phase.RUNNING) {
      if (!super.place(strategy.calculateMove(this.getState()))) {
        throw new IllegalStateException(
            "AI-move " + strategy.calculateMove(this.getState()) + " wasn't valid");
      }
    }

    return true;
  }
}
