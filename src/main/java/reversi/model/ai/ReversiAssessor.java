package reversi.model.ai;

import java.util.Arrays;
import java.util.List;

import reversi.model.BitBoardState;

/**
 * A composite assessor class that allows to compute the score for a given {@link
 * BitBoardState}-instance. The class consists of several other assessor classes that do the actual
 * work of rating the various parts of the state.
 *
 * @see StoneCountAssessor
 * @see WinVelocityAssessor
 * @see WeightedCellsAssessor
 * @see MobilityAssessor
 */
public class ReversiAssessor implements BitBoardStateAssessor {

  private static final List<BitBoardStateAssessor> ASSESSORS =
      Arrays.asList(
      new StoneCountAssessor(),
      new MobilityAssessor(),
      new WeightedCellsAssessor(),
      new WinVelocityAssessor()
    );

  /**
   * {@inheritDoc}
   */
  @Override
  public double computeValue(BitBoardState state, int depth) {
    double result = 0;
    for (BitBoardStateAssessor assessor : ASSESSORS) {
      result += assessor.computeValue(state, depth);
    }
    return result;
  }
}
