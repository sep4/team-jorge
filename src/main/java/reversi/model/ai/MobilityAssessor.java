package reversi.model.ai;

import reversi.model.BitBoardState;

/**
 * Assessment class that rates the {@link BitBoardState} by evaluating the number of valid
 * moves for each player.
 *
 * <p>In this implementation, the best possible score can be achieved by
 * {@link BitBoardAI#AI_PLAYER} having as many moves as possible, and the human player
 * having as few moves left as possible.
 */
public class MobilityAssessor implements BitBoardStateAssessor {

  /**
   * {@inheritDoc}
   */
  @Override
  public double computeValue(BitBoardState state, int depth) {
    double aiMobility
        = Long.bitCount(state.getField().getMoves(BitBoardAI.AI_PLAYER));
    double humanMobility
        = Long.bitCount(state.getField().getMoves(BitBoardAI.AI_PLAYER.getOtherPlayer()));

    return aiMobility - MODIFIER * humanMobility;
  }
}
