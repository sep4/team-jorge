package reversi.model.ai;

import reversi.model.BitBoardState;
import reversi.model.Player;

/**
 * Assessment interface used to rate the current {@link BitBoardState} of a running reversi
 * application. This rating can then be used for computing the best possible move for an
 * AI Strategy.
 *
 * {@link BitBoardStateAssessor#MODIFIER} is a constant value that is chosen arbitrarily in order to
 * ensure that a higher reward is always given to the AI. By multiplying the utility of
 * the ai-player by the modifier, we ensure that the difference between the players' utilities
 * is larger and is a double value.
 */
public interface BitBoardStateAssessor {
  double MODIFIER = 1.5;

  /**
   * Rates the current {@link BitBoardState} and returns the computed score afterwards. Independent
   * of the current {@link Player}, it always computes the best possible value for
   * the ai-player, while keeping the score for the human player at a minimal value.
   *
   * @param state The current {@link BitBoardState} that is to be rated.
   * @param depth The amount of moves already made for reaching this state.
   * @return a double-value that is the rating of the current state.
   */
  double computeValue(BitBoardState state, int depth);

}
