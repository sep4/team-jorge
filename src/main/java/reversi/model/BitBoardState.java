package reversi.model;

import static java.util.Objects.requireNonNull;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A class for a data structure that contains all necessary attributes in order to successfully
 * play a game with a bit board.
 */
public class BitBoardState implements GameState, Cloneable {

  private BitBoard bitBoard;
  private Phase currentPhase;
  private Player currentPlayer;
  private Player winner;

  /**
   * Constructs a new <Code>GameState</code>. The state begins in a clear state, which means that
   * 1.) no stones are on the board initially.
   * 2.) player white is set as the beginning player, and
   * 3.) the game has its {@link Phase} set to a running state.
   */
  public BitBoardState() {
    this.bitBoard = new BitBoard();
    this.currentPlayer = Player.BLACK;
    this.currentPhase = Phase.RUNNING;
    this.winner = null;
  }

  /**
   * Overwrites the current {@link Phase phase} of the game with the new one.
   *
   * @param phase The new phase.
   */
  public void setCurrentPhase(Phase phase) {
    currentPhase = requireNonNull(phase);
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public Phase getCurrentPhase() {
    return currentPhase;
  }
  
  /**
   * Returns the {@link BitBoard bitBoard} that stores the data for each cell as a bit.
   *
   * @return The current bitBoard.
   */
  public BitBoard getField() {
    return bitBoard;
  }

  /**
   * Overrides the {@link BitBoard bitBoard} that stores the data for each cell as a bit.
   *
   * @param bitBoard The new BitBoard instance.
   */
  private void setField(BitBoard bitBoard) {
    this.bitBoard = bitBoard;
  }

  /**
   * Checks for the winner. The player with the most stones is the winner.
   */
  void updateWinner() {
    if (currentPhase != Phase.FINISHED) {
      throw new IllegalStateException(
          String.format(
              "Expected current phase to be %s, but instead it is %s",
              Phase.FINISHED, currentPhase));
    }

    if (bitBoard.countPieces(Player.WHITE) > bitBoard.countPieces(Player.BLACK)) {
      winner = Player.WHITE;
    } else if (bitBoard.countPieces(Player.BLACK) > bitBoard.countPieces(Player.WHITE)) {
      winner = Player.BLACK;
    } else {
      winner = null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<Player> getWinner() {
    if (currentPhase != Phase.FINISHED) {
      throw new IllegalStateException(
          String.format(
              "Expected current phase to be %s, but instead it is %s",
              Phase.FINISHED, currentPhase));
    }

    return Optional.ofNullable(winner);
  }

  public void setWinner(Player player) {
    this.winner = player;
  }

  /**
   * Switches the player.
   */
  public void switchPlayer() {
    currentPlayer = currentPlayer.getOtherPlayer();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Player getCurrentPlayer() {
    return currentPlayer;
  }

  /**
   * Overrides the current player with a new player.
   *
   * @param newPlayer The new player.
   */
  public void setCurrentPlayer(Player newPlayer) {
    currentPlayer = requireNonNull(newPlayer);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<Cell> getAllCellsOfPlayer(Player player) {
    return bitBoard 
        .getCellsOccupiedWithStones()
        .entrySet()
        .stream()
        .filter(x -> player == x.getValue())
        .map(Map.Entry::getKey)
        .collect(Collectors.toCollection(HashSet::new));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public BitBoardState clone() {
    try {
      super.clone();
    } catch (CloneNotSupportedException ex) {
      System.out.println("Error: Could not clone GameState.");
    }
    BitBoardState gameState = new BitBoardState();
    gameState.setField(this.bitBoard.clone());
    gameState.setCurrentPlayer(this.currentPlayer);
    gameState.setCurrentPhase(this.currentPhase);
    gameState.setWinner(winner);
    return gameState;
  }

}
