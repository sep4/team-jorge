package reversi.model.network;

import reversi.model.BitBoardReversi;
import reversi.model.Phase;
import reversi.model.Player;

/**
 * Model for playing reversi with another player over the network.
 */
public class NetworkReversi extends BitBoardReversi {

  /**
   * Username of the opponent in the game.
   */
  private String opponent;

  /**
   * Color of the local player.
   */
  private Player assignedPlayer;

  /**
   * Creates a new network game. This constructor does not automatically start the network game, but
   * {@link #newGame()} must be called to start the game.
   */
  public NetworkReversi() {
    getState().setCurrentPhase(Phase.WAITING);
  }

  /**
   * Returns the {@link Player} which is assigned to this instance.
   *
   * @return The assigned player.
   */
  public Player getAssignedPlayer() {
    return assignedPlayer;
  }

  /**
   * Sets the assigned player of the user for the game.
   *
   * @param player The assigned player.
   */
  void setAssignedPlayer(Player player) {
    assignedPlayer = player;
  }

  /**
   * Returns the opponent of the user.
   *
   * @return The opponent.
   */
  public String getOpponent() {
    return opponent;
  }

  /**
   * Sets the opponent for the game.
   *
   * @param name The opponents name.
   */
  void setOpponent(String name) {
    opponent = name;
  }
}
