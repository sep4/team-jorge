package reversi.model.network;

import static java.util.Objects.requireNonNull;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.net.InetAddress;
import java.net.Socket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import reversi.model.Cell;
import reversi.model.Player;

import server.model.NetworkConstants;

/**
 * A client which handles the communication with a server. It lets the user create and join games on
 * a server.
 */
public class Client implements NetworkConstants, Closeable {

  private String username;
  private boolean approved = false;

  private InetAddress serverAddress;
  private Socket clientSocket;
  private ObjectOutputStream outputStream;

  private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

  private NetworkReversi reversi;

  private ArrayList<String> lobby = new ArrayList<>();
  private HashMap<String, Player> openGames = new HashMap<>();

  /**
   * Creates a client used to communicate with the server at the given address. The created client
   * is differentiated from other clients using the given username.
   *
   * @param username The username of the created client.
   * @param serverAddress The address of the server the client will try to connect to.
   */
  public Client(String username, InetAddress serverAddress) {
    this.username = username;
    this.serverAddress = serverAddress;
  }

  /**
   * Starts the communication between this client and the server at the saved address.
   *
   * @throws IOException If the connection fails.
   */
  public void start() throws IOException {
    clientSocket = new Socket(serverAddress, PORT);
    outputStream = new ObjectOutputStream(clientSocket.getOutputStream());

    Thread receiverThread =
        new Thread(
            () -> {
              try (ObjectInputStream inputStream =
                  new ObjectInputStream(clientSocket.getInputStream())) {
                handleInputStream(inputStream);
              } catch (ClassNotFoundException e) {
                throw new AssertionError(e);
              } catch (IOException e) {
                // expected if server closes connection
                cleanUpConnection();
              }
            });
    receiverThread.setDaemon(true);
    receiverThread.start();

    sendName();
  }

  private void handleInputStream(ObjectInputStream inputStream)
      throws IOException, ClassNotFoundException {
    Object received = inputStream.readObject();
    while (received instanceof String) {
      try {
        JSONObject jsonObject = new JSONObject((String) received);
        Iterator<String> keys = jsonObject.keys();
        while (keys.hasNext()) {
          String key = keys.next();
          handleKey(key, jsonObject);
        }
      } catch (JSONException e) {
        // The server sent something not in JSON-protocol, so it can't be a working reversi-server
        // and therefore the client should be disconnected from the server
        cleanUpConnection();
      }
      received = inputStream.readObject();
    }
  }

  private void handleKey(String key, JSONObject jsonObject) {
    switch (key) {
      case NAME_REJECTED:
        notifyListeners(NAME_REJECTED);
        break;

      case NAME_APPROVED:
        approved = true;
        notifyListeners(UPDATE);
        break;

      case UPDATE:
        handleUpdate(jsonObject);
        break;

      case JOIN_REJECTED:
        notifyListeners(JOIN_REJECTED);
        break;

      case START_GAME:
        handleStartGame(jsonObject);
        break;

      case MOVE:
        handleMove(jsonObject);
        break;

      case QUIT:
        notifyListeners(QUIT);
        break;

      case DISCONNECT:
        notifyListeners(DISCONNECT);
        break;

      default:
        // The server sent something with an unknown key and therefore can't be a working reversi
        // server and therefore the client should be disconnected from the server.
        cleanUpConnection();
    }
  }

  private void handleUpdate(JSONObject jsonObject) {
    JSONObject update = jsonObject.getJSONObject(UPDATE);
    JSONArray lobbyArray = update.getJSONArray(LOBBY);
    lobby = toLobby(lobbyArray);
    JSONArray gameArray = update.getJSONArray(GAMES);
    openGames = toOpenGames(gameArray);
    notifyListeners(UPDATE);
  }

  private ArrayList<String> toLobby(JSONArray lobbyArray) {
    ArrayList<String> list = new ArrayList<>();
    if (lobbyArray != null) {
      for (int i = 0; i < lobbyArray.length(); i++) {
        list.add((String) lobbyArray.get(i));
      }
    }
    return list;
  }

  private HashMap<String, Player> toOpenGames(JSONArray gameArray) {
    HashMap<String, Player> map = new HashMap<>();
    if (gameArray != null) {
      for (int i = 0; i < gameArray.length(); i++) {
        JSONObject jsonObject = (JSONObject) gameArray.get(i);
        Player player = getPlayerFromString(jsonObject.getString(CREATOR_PLAYER));
        map.put(jsonObject.getString(CREATOR), player);
      }
    }
    return map;
  }

  private Player getPlayerFromString(String playerString) {
    Player player;
    switch (playerString) {
      case BLACK:
        player = Player.BLACK;
        break;
      case WHITE:
        player = Player.WHITE;
        break;
      default:
        throw new AssertionError(
            "Unhandled player " + playerString);
    }
    return player;
  }

  private void handleStartGame(JSONObject jsonObject) {
    JSONObject gameJson = (JSONObject) jsonObject.get(START_GAME);
    String opponent = gameJson.getString(OPPONENT);
    Player player = getPlayerFromString(gameJson.getString(PLAYER));
    reversi.setAssignedPlayer(player);
    reversi.setOpponent(opponent);
    reversi.newGame();
  }

  private void handleMove(JSONObject jsonObject) {
    JSONObject move = jsonObject.getJSONObject(MOVE);
    int row = move.getInt(ROW);
    int column = move.getInt(COLUMN);
    reversi.place(new Cell(column, row));
  }

  /**
   * Sends the current username to the server as a suggestion for the username of this client.
   */
  private void sendName() {
    JSONObject jsonName = new JSONObject();
    jsonName.put(NAME, username);
    sendJson(jsonName);
  }

  /**
   * Suggests a new name for this client to the server if the server rejected the last name.
   *
   * @param name The suggested name.
   */
  public void suggestName(String name) {
    username = name;
    sendName();
  }

  /**
   * Sends a message to the server that this user wants to create a game.
   *
   * @param player The color that the creator of the game wants to play.
   * @return The created game.
   */
  public NetworkReversi createGame(Player player) {
    reversi = new NetworkReversi();
    JSONObject json = new JSONObject();
    if (player == Player.BLACK) {
      json.put(CREATE, BLACK);
    } else {
      json.put(CREATE, WHITE);
    }
    sendJson(json);
    return reversi;
  }

  /**
   * Sends a message to the server that this user wants to join the given creator's game.
   *
   * @param creator The creator who's game will be joined.
   * @return The joined game.
   */
  public NetworkReversi joinGame(String creator) {
    reversi = new NetworkReversi();
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(JOIN, creator);
    sendJson(jsonObject);
    return reversi;
  }

  /**
   * Sends a message to the server that the user wants to place a stone.
   *
   * @param cell The cell on which the user wants to place the stone.
   */
  public void place(Cell cell) {
    JSONObject move = new JSONObject();
    move.put(ROW, cell.getRow());
    move.put(COLUMN, cell.getColumn());
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(MOVE, move);
    sendJson(jsonObject);
  }

  /**
   * Lets a player leave a network game by sending the server a respective message.
   */
  public void quitGame() {
    JSONObject json = new JSONObject();
    json.put(QUIT, JSONObject.NULL);
    sendJson(json);
    reversi = null;
  }

  private void sendJson(JSONObject jsonObject) {
    try {
      outputStream.writeObject(jsonObject.toString());
      outputStream.flush();
    } catch (IOException e) {
      // expected if server closes connection
      cleanUpConnection();
    }
  }

  /**
   * Returns the username of this client.
   *
   * @return The username.
   */
  public String getUsername() {
    return username;
  }

  /**
   * Returns whether the client and especially its username has been approved by the server.
   *
   * @return <code>true</code> if the server approved the client, <code>false</code> otherwise.
   */
  public boolean isApproved() {
    return approved;
  }

  /**
   * Returns the list of players that are currently in the game lobby.
   *
   * @return The list of players in the lobby.
   */
  public ArrayList<String> getLobby() {
    return lobby;
  }

  /**
   * Returns a map of the open games.
   *
   * @return A map of the open games.
   */
  public HashMap<String, Player> getOpenGames() {
    return openGames;
  }

  private void notifyListeners(String propertyName) {
    pcs.firePropertyChange(propertyName, null, null);
  }

  /**
   * Adds a {@link PropertyChangeListener} to the client that will be notified about changes in
   * connections and lobby.
   *
   * @param pcl The listener.
   */
  public void subscribe(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    pcs.addPropertyChangeListener(pcl);
  }

  /**
   * Removes a listener from the client, which will then no longer be notified about any changes.
   *
   * @param pcl The former listener that then no longer receives notifications from the client.
   */
  public void unsubscribe(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    pcs.removePropertyChangeListener(pcl);
  }

  private void cleanUpConnection() {
    try {
      close();
      clientSocket = null;
      outputStream = null;
    } catch (IOException e) {
      throw new AssertionError(e);
    }
    notifyListeners(LOST_CONNECTION);
  }

  @Override
  public void close() throws IOException {
    if (clientSocket != null) {
      clientSocket.close();
    }
  }
}
