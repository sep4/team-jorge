package reversi.model;

/**
 * Function wrapper for shifting procedures on BitBoards. 
 */
public interface IShift {
  /**
   * Applies shifting procedure on l.
   *
   * @param l Bitboard represented bei long l.
   */
  long call(long l);
}
