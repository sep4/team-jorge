package reversi.view;

import java.net.InetAddress;

import reversi.model.Cell;
import reversi.model.Player;

/**
 * The main controller interface of the reversi game. It takes the actions from the user and handles
 * them accordingly. This is by either invoking the necessary model-methods, or by directly telling
 * the view to change its graphical user-interface.
 */
public interface Controller {

  /**
   * Strings used to forward the difficulty of the AI chosen by the user from the view to the
   * controller.
   */
  String BEGINNER_AI = "Beginner";
  String TRAINEE_AI = "Trainee";
  String ADVANCED_AI = "Advanced";
  String EXPERT_AI = "Expert";

  /**
   * Initializes and starts the user interface.
   */
  void start();

  /**
   * Suggests a different username to the server when the server rejected the last name.
   *
   * @param name The suggested username.
   */
  void suggestName(String name);

  /**
   * Creates an online game such that it will be shown in the game lobby afterwards.
   *
   * @param player The color the user will play in the game.
   */
  void createOnlineGame(Player player);

  /**
   * Joins the online game created by the user with the given name.
   *
   * @param creatorName User name of the creator of the game.
   */
  void joinOnlineGame(String creatorName);

  /** Resets a game such that the game is in its initial state afterwards. */
  void resetGame();

  /** Quits a network game and returns to the game lobby. */
  void quitNetworkGame();

  /** Sets up the start screen on which the user can select between different game modes. */
  void showStartView();

  /** Sets up the game lobby where the user can create a game or join an existing one. */
  void showGameLobby();

  /** Sets a hotseat game up that the user can afterwards play on. */
  void hotseatGameSelected();

  /**
   * Sets up a single player game that allows the user to play against an ai player.
   *
   * @param difficulty The difficulty of the AI chosen by the user.
   */
  void aiGameSelected(String difficulty);

  /**
   * Sets up a client that allows to enter the game lobby and potentially play against other players
   * online. Tries to connect to the server at the given address.
   *
   * @param userName Name of the client.
   * @param serverAddress Server address to connect to.
   */
  void networkGameSelected(String userName, InetAddress serverAddress);

  /**
   * Validates the input and in case of success asks the model to execute a step on the reversi
   * board.
   *
   * @param cell The {@link Cell cell} to place the next stone on.
   * @return <code>true</code> if validating the input was successful, <code>false</code> otherwise.
   */
  boolean place(Cell cell);

  /**
   * Cancels any running animations.
   */
  void cancelAnimation();

  /**
   * Disposes any remaining resources.
   */
  void dispose();
}
