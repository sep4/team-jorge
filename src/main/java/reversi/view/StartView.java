package reversi.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.net.InetAddress;
import java.net.UnknownHostException;

import java.util.Objects;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import server.model.NetworkConstants;

/**
 * Start view of the game that provides buttons for choosing between the different game modes of the
 * game.
 */
public class StartView extends JPanel {

  private static final long serialVersionUID = 8435107157674744226L;

  private static final int PREFERRED_FRAME_WIDTH = 500;
  private static final int PREFERRED_FRAME_HEIGHT = 500;

  private final Controller controller;

  private JButton hotseatGameButton;
  private JButton singleplayerGameButton;
  private JButton networkGameButton;

  /**
   * Creates a new starting view in which all required elements are set up accordingly.
   *
   * @param controller The controller used to forward any events registered by the buttons.
   */
  StartView(Controller controller) {
    this.controller = Objects.requireNonNull(controller);

    setPreferredSize(new Dimension(PREFERRED_FRAME_WIDTH, PREFERRED_FRAME_HEIGHT));

    createContent();
    configureActionListeners();
  }

  private void createContent() {
    setLayout(new GridBagLayout());

    JPanel panel = new JPanel();
    panel.setPreferredSize(new Dimension(PREFERRED_FRAME_WIDTH, PREFERRED_FRAME_HEIGHT));

    GridBagConstraints c = new GridBagConstraints();
    c.anchor = GridBagConstraints.CENTER;
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1.0f;
    c.weighty = 1.0f;
    add(panel, c);

    panel.setLayout(new GridBagLayout());
    c = new GridBagConstraints();
    c.gridx = 0;
    c.ipadx = 30;
    c.ipady = 14;
    c.insets = new Insets(25, 10, 10, 10);

    hotseatGameButton = new JButton("Hotseat Game");
    hotseatGameButton.setToolTipText("Start an offline game");
    hotseatGameButton.setEnabled(true);
    panel.add(hotseatGameButton, c);

    singleplayerGameButton = new JButton("SinglePlayer Game");
    singleplayerGameButton.setToolTipText("Starts an offline game with an AI");
    singleplayerGameButton.setEnabled(true);
    panel.add(singleplayerGameButton, c);

    networkGameButton = new JButton("Network Game");
    networkGameButton.setToolTipText("Join the game lobby to play online");
    networkGameButton.setEnabled(true);
    panel.add(networkGameButton, c);
  }

  private void configureActionListeners() {
    hotseatGameButton.addActionListener(e -> controller.hotseatGameSelected());
    singleplayerGameButton.addActionListener(e -> letUserChooseAiDifficulty());
    networkGameButton.addActionListener(e -> askForNetworkInformation());
  }

  private void letUserChooseAiDifficulty() {
    String[] aiDifficulties = {
      Controller.BEGINNER_AI, Controller.TRAINEE_AI, Controller.ADVANCED_AI, Controller.EXPERT_AI
    };
    int chosenDifficulty =
        JOptionPane.showOptionDialog(
            this,
            "Please choose the difficulty of the single player game:",
            "Choose difficulty",
            JOptionPane.DEFAULT_OPTION,
            JOptionPane.PLAIN_MESSAGE,
            null,
            aiDifficulties,
            null);
    switch (chosenDifficulty) {
      case 0: // user chose beginner
        controller.aiGameSelected(Controller.BEGINNER_AI);
        break;
      case 1: // user chose trainee
        controller.aiGameSelected(Controller.TRAINEE_AI);
        break;
      case 2: // user chose advanced
        controller.aiGameSelected(Controller.ADVANCED_AI);
        break;
      case 3: // user chose expert
        controller.aiGameSelected(Controller.EXPERT_AI);
        break;
      default:
        // do nothing if the user does something else
    }
  }

  private void askForNetworkInformation() {
    String username = askForUsername(this);
    // do nothing and return to start menu it the user didn't enter anything
    if (username == null) {
      return;
    }
    String serverIp =
        JOptionPane.showInputDialog(
            this,
            "Please enter the server address here:",
            "Enter server address",
            JOptionPane.QUESTION_MESSAGE);

    // return to the start menu otherwise as the user didn't enter anything
    if (serverIp != null && !serverIp.equals("")) {
      try {
        InetAddress address = InetAddress.getByName(serverIp);
        controller.networkGameSelected(username, address);
      } catch (UnknownHostException e) {
        JOptionPane.showMessageDialog(
            this, "Invalid server address: " + serverIp, "Error", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  /**
   * Asks the user to enter a name. If the user doesn't enter anything, the method returns null. If
   * the user enters a name of inappropriate length, a respective error message is shown and the
   * user is asked to enter a name again.
   *
   * @param parentComponent The parent component of the used pop-up windows.
   * @return A username of appropriate length or null, if the user didn't enter anything.
   */
  static String askForUsername(Component parentComponent) {
    String username =
        JOptionPane.showInputDialog(
            parentComponent,
            "Please enter a name here:",
            "Enter name",
            JOptionPane.QUESTION_MESSAGE);
    if (username == null || username.equals("")) {
      return null;
    }
    if (!hasAppropriateLength(username)) {
      JOptionPane.showMessageDialog(
          parentComponent,
          "<html>The username "
              + username
              + " you chose has an inappropriate length.<br>"
              + "Please choose another name having between "
              + NetworkConstants.MINIMUM_USERNAME_LENGTH
              + " and "
              + NetworkConstants.MAXIMUM_USERNAME_LENGTH
              + " characters!</html>",
          "Inappropriate length of username",
          JOptionPane.ERROR_MESSAGE);
      username = askForUsername(parentComponent);
    }
    return username;
  }

  private static boolean hasAppropriateLength(String name) {
    return name.length() >= NetworkConstants.MINIMUM_USERNAME_LENGTH
        && name.length() <= NetworkConstants.MAXIMUM_USERNAME_LENGTH;
  }
}
