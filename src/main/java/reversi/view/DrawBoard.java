package reversi.view;

import static java.util.Objects.requireNonNull;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import reversi.model.Cell;
import reversi.model.GameField;
import reversi.model.Model;
import reversi.model.Phase;
import reversi.model.Player;
import reversi.model.ai.BitBoardAI;
import reversi.model.network.NetworkReversi;

/**
 * A custom painting class that is responsible for drawing the complete reversi board along with all
 * necessary information on it. This includes a proper display of each single reversi field as well
 * as the stones already placed on the board. Besides, all possible moves for a player are painted
 * should the user have the hints-checkbox selected.
 */
public class DrawBoard extends JPanel implements PropertyChangeListener {

  private static final long serialVersionUID = 7101440272429739521L;

  /** The share that the field padding makes up of the draw board width at least. */
  private static final double FIELD_PADDING_SHARE = 1 / 20.0;
  /** The share that the cell padding makes up of the cell width. */
  private static final double CELL_PADDING_SHARE = 1 / 10.0;

  private static final Color BACKGROUND_COLOR = Color.LIGHT_GRAY;
  private static final Color USUAL_CELL_COLOR = Color.GRAY;
  private static final Color HIGHLIGHT_CELL_COLOR = Color.ORANGE;
  private static final Color CELL_EDGE_COLOR = Color.DARK_GRAY;
  private static final Color WHITE_STONE_COLOR = Color.WHITE;
  private static final Color BLACK_STONE_COLOR = Color.BLACK;
  private static final Color STONE_LINING_COLOR = Color.BLACK;

  private final Model model;
  private final Controller controller;

  private Set<Cell> possibleCellsForCurrentPlayer;
  private boolean placingHintsShown = false;

  private int cellSize;

  private int horizontalOffset;
  private int verticalOffset;

  /**
   * Creates a draw board used to draw the reversi field. It requires a model for being able to
   * retrieve the placed stones and the possible moves of the players, and a controller for
   * forwarding the user-interactions done on this board.
   *
   * @param model The model for getting the necessary information about the placed stones.
   * @param controller A controller to forward the interactions of the user.
   */
  DrawBoard(Model model, Controller controller) {
    this.model = requireNonNull(model);
    this.controller = requireNonNull(controller);

    model.addPropertyChangeListener(this);
    possibleCellsForCurrentPlayer =
        model.getPossibleCellsForPlayer(model.getState().getCurrentPlayer());

    setBackground(BACKGROUND_COLOR);
    configureActionListeners();
  }

  private void configureActionListeners() {
    addComponentListener(
        new ComponentAdapter() {
          @Override
          public void componentResized(ComponentEvent e) {
            repaint();
          }
        });

    addMouseListener(new DrawBoardListener());
  }

  @Override
  protected void paintComponent(Graphics g) {
    Graphics2D g2D = (Graphics2D) g;
    // optional rendering options that are used for preference when drawing the image-icons
    g2D.setRenderingHint(
        RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

    // paint the background
    g2D.setColor(BACKGROUND_COLOR);
    g2D.fillRect(0, 0, getWidth(), getHeight());

    updateMeasurements();
    drawBoard(g2D);
    drawAllStones(g2D);
  }

  /**
   * Updates the measurements of the field according to the current size of the draw board.
   */
  private void updateMeasurements() {
    int centerWidth = Math.min(getWidth(), getHeight());
    double fieldPadding = centerWidth * FIELD_PADDING_SHARE;
    int fieldWidth = (int) (centerWidth - 2 * fieldPadding);
    cellSize = fieldWidth / GameField.SIZE;
    horizontalOffset = (getWidth() - fieldWidth) / 2;
    verticalOffset = (getHeight() - fieldWidth) / 2;
  }

  /**
   * Draws the game field without the stones on top using the variable measurements. The method
   * distinguishes between the cells, that are possible moves for the current player, and the rest
   * of the cells. It uses the color constants declared above to fill and highlight the cells.
   *
   * @param g The {@link Graphics} object that allows to draw on the board.
   */
  private void drawBoard(Graphics g) {
    for (int row = 0; row < GameField.SIZE; row++) {
      for (int column = 0; column < GameField.SIZE; column++) {
        Cell cell = new Cell(column, row);
        Color color = USUAL_CELL_COLOR;
        if (shouldCellBeHighlighted(cell)) {
          color = HIGHLIGHT_CELL_COLOR;
        }

        drawCell(g, cell, color);
      }
    }
  }

  /**
   * Checks whether the given cell should be highlighted.
   *
   * @param cell The cell to be checked for highlighting.
   * @return whether the given cell should be highlighted.
   */
  private boolean shouldCellBeHighlighted(Cell cell) {
    if (!placingHintsShown || !isPlacingPossible()) {
      return false;
    }
    return possibleCellsForCurrentPlayer.contains(cell);
  }

  /**
   * Checks whether placing is possible in the current situation for the current player. In this
   * method game mode, current phase and assigned players are taken into account.
   *
   * @return <code>true</code> if placing is currently possible, <code>false</code> otherwise.
   */
  private boolean isPlacingPossible() {
    if (model.getState().getCurrentPhase() == Phase.FINISHED) {
      return false;
    }
    if (model instanceof BitBoardAI
        && model.getState().getCurrentPlayer() == BitBoardAI.AI_PLAYER) {
      return false;
    }
    if (model instanceof NetworkReversi
        && model.getState().getCurrentPlayer() != ((NetworkReversi) model).getAssignedPlayer()) {
      return false;
    }
    return true;
  }

  /**
   * Draws and fills the given cell with the given color.
   *
   * @param g The {@link Graphics} object to draw with.
   * @param cell The cell to draw.
   * @param cellColor The color to fill the cell with.
   */
  private void drawCell(Graphics g, Cell cell, Color cellColor) {
    int column = cell.getColumn();
    int row = cell.getRow();

    g.setColor(cellColor);
    g.fillRect(
        horizontalOffset + column * cellSize, verticalOffset + row * cellSize, cellSize, cellSize);

    g.setColor(CELL_EDGE_COLOR);
    g.drawRect(
        horizontalOffset + column * cellSize, verticalOffset + row * cellSize, cellSize, cellSize);
  }

  /**
   * Draws all placed stones on top of the game field using the variable measurements.
   *
   * @param g The {@link Graphics} object that allows to draw on the board.
   */
  private void drawAllStones(Graphics g) {
    GameField field = model.getState().getField();
    Map<Cell, Player> occupiedCells = field.getCellsOccupiedWithStones();

    for (Map.Entry<Cell, Player> entry : occupiedCells.entrySet()) {
      int row = entry.getKey().getRow();
      int column = entry.getKey().getColumn();
      drawStone(
          entry.getValue(),
          g,
          (int) (cellSize * CELL_PADDING_SHARE),
          horizontalOffset + column * cellSize,
          verticalOffset + row * cellSize);
    }
  }

  /**
   * Draws a stone on the current selected cell.
   *
   * @param player The player that owns the cell.
   * @param g The {@link Graphics} object that allows to draw on the board.
   * @param padding Used to determine the gap-size between the stone and the border of the cell.
   * @param x The coordinate marking the left point of the cell.
   * @param y The coordinate marking the upper point of the cell.
   */
  private void drawStone(Player player, Graphics g, int padding, int x, int y) {
    switch (player) {
      case WHITE:
        g.setColor(WHITE_STONE_COLOR);
        break;
      case BLACK:
        g.setColor(BLACK_STONE_COLOR);
        break;
      default:
        throw new RuntimeException("Unhandled player: " + player);
    }

    g.fillOval(x + padding, y + padding, cellSize - 2 * padding, cellSize - 2 * padding);
    g.setColor(STONE_LINING_COLOR);
    g.drawOval(x + padding, y + padding, cellSize - 2 * padding, cellSize - 2 * padding);
  }

  @Override
  public void propertyChange(PropertyChangeEvent event) {
    SwingUtilities.invokeLater(() -> handlePropertyChange(event));
  }

  /**
   * Reacts when the model announces that it has changed its state. The method will thus repaint the
   * board such that the display will show the latest state again.
   *
   * @param event The event that has been fired by the model.
   */
  private void handlePropertyChange(PropertyChangeEvent event) {
    possibleCellsForCurrentPlayer =
        model.getPossibleCellsForPlayer(model.getState().getCurrentPlayer());
    repaint();
  }

  /**
   * Disposes the draw board by unsubscribing it from the model.
   */
  void dispose() {
    model.removePropertyChangeListener(this);
  }

  /**
   * Overwrites the {@link MouseListener}-interface so that any clicks made by the user on the
   * reversi board are registered and handled accordingly here.
   */
  class DrawBoardListener extends MouseAdapter {

    @Override
    public void mouseClicked(MouseEvent e) {
      if (e.getButton() == MouseEvent.BUTTON2 || e.getButton() == MouseEvent.BUTTON3) {
        // user used middle-click or right-click --> do nothing
        return;
      }

      if (!isPlacingPossible()) {
        return;
      }

      Optional<Cell> cellOpt = screenToBoardCoords(e.getPoint());
      if (cellOpt.isEmpty()) {
        // user clicked on the light-gray area outside of the game field
        return;
      }

      if (possibleCellsForCurrentPlayer.contains(cellOpt.get())) {
        // The clicked cell is amongst the possible moves of the current player
        // --> pass the request on to the controller
        if (!controller.place(cellOpt.get())) {
          throw new IllegalStateException("Hinted placing was wrong");
        }

      } else {
        // Current player clicked on a cell that does not belong to him
        // --> Do nothing
      }
    }
  }

  /**
   * Checks if the registered mouse-click event was made on the painted cells of the drawing board.
   *
   * <p>In this case an {@link Optional} with the actual {@link Cell} is returned.
   *
   * @param p the point of the registered mouse click event.
   * @return An {@link Optional} containing the x- and y-coordinate of the cell.
   */
  private Optional<Cell> screenToBoardCoords(Point p) {
    // check if registered click is on one of the drawn cells of the board
    if (p.x <= horizontalOffset
        || p.y <= verticalOffset
        || p.x >= getWidth() - horizontalOffset
        || p.y >= getHeight() - verticalOffset
        || cellSize == 0) {
      return Optional.empty();
    }

    // turn point to the actual cell coordinate
    int column = (p.x - horizontalOffset) / cellSize;
    int row = (p.y - verticalOffset) / cellSize;
    Cell cell = new Cell(column, row);

    if (!GameField.isWithinBounds(cell)) {
      return Optional.empty();
    }
    return Optional.of(cell);
  }

  /**
   * Shows the placing hints for the current player. The draw board gets newly painted afterwards.
   */
  void showPlacingHints() {
    placingHintsShown = true;
    repaint();
  }

  /**
   * Removes all placing hints and repaints the board in case they were previously shown.
   */
  void removePlacingHints() {
    placingHintsShown = false;
    repaint();
  }
}
