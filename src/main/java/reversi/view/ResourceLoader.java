package reversi.view;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import java.net.URL;

import java.util.Optional;

import javax.swing.ImageIcon;

/**
 * Class used to load user-specific icons or images.
 */
class ResourceLoader {

  private ResourceLoader() {
    // private constructor that prevents the instantiation from outside of this class.
  }

  /**
   * Returns an optional containing an image with the given size. The optional is empty in case of
   * loading the image failed.
   *
   * @param fileName The name of the file.
   * @param width The width of the image.
   * @param height The height of the image.
   * @return An optional containing the image in case of success.
   */
  static Optional<ImageIcon> loadScaledImageIcon(String fileName, int width, int height) {
    Optional<ImageIcon> imgIconOpt = loadImageIcon(fileName);
    if (imgIconOpt.isEmpty()) {
      return imgIconOpt;
    }

    BufferedImage resizedImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    Graphics2D g2 = resizedImg.createGraphics();
    g2.setRenderingHint(
        RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    g2.drawImage(imgIconOpt.get().getImage(), 0, 0, width, height, null);
    g2.dispose();

    return Optional.of(new ImageIcon(resizedImg));
  }

  /**
   * Creates an {@link ImageIcon} from the file with the given name.
   *
   * @param fileName The name of the file.
   * @return An optional containing an ImageIcon in case of success.
   */
  private static Optional<ImageIcon> loadImageIcon(String fileName) {
    URL resource = ResourceLoader.class.getResource('/' + fileName);
    ImageIcon imageIcon = new ImageIcon(resource);
    return Optional.of(imageIcon);
  }
}
