package reversi.view;

import reversi.model.Model;
import reversi.model.network.Client;

/**
 * The main interface of the view. It gets the state it displays directly from the {@link Model}.
 */
public interface View {

  /**
   * Shows the graphical user interface of the reversi game.
   */
  void showView();

  /**
   * Shows a custom reversi animation to the user.
   */
  void showAnimation();

  /**
   * Removes an animation-view if there is currently one shown.
   */
  void removeAnimation();


  /**
   * Shows the start menu to the user.
   */
  void showStartMenu();

  /**
   * Shows the game lobby to the user.
   *
   * @param client The client that provides the necessary information.
   */
  void showLobby(Client client);

  /**
   * Removes a lobby-view if there is currently one shown.
   */
  void removeLobby();

  /**
   * Shows the game view to the user.
   *
   * @param model The game to be shown.
   */
  void showGame(Model model);

  /**
   * Removes a game-view if there is currently one shown.
   */
  void removeGame();

  /**
   * Sets the current step of the running animation to the given step.
   *
   * @param step The new current step of the running animation.
   * @throws IllegalStateException if there is no animation running.
   */
  void setAnimationStep(int step);

  /**
   * Draws the given step of the animation.
   *
   * @param step The step to draw.
   * @throws IllegalStateException if there is no animation running.
   */
  void drawAnimation(int step);

  /**
   * Displays an error message to the user.
   *
   * @param message The message to be displayed.
   */
  void showErrorMessage(String message);

  /**
   * Displays an information message to the user.
   *
   * @param header The header of the message.
   * @param message The message to be displayed.
   */
  void showInformationMessage(String header, String message);
}
