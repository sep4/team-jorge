package reversi.view;

import static java.util.Objects.requireNonNull;

import static reversi.view.AnimationView.WAITING_TIME;

import java.io.IOException;

import java.net.InetAddress;

import java.util.ArrayList;

import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import py4j.GatewayServer;

import reversi.model.BitBoardReversi;
import reversi.model.Cell;
import reversi.model.GameField;
import reversi.model.Model;
import reversi.model.Player;
import reversi.model.Py4JListener;
import reversi.model.ai.AlphaBetaBitBoardStrategy;
import reversi.model.ai.BitBoardAI;
import reversi.model.ai.DeepQlearningStrategy;
import reversi.model.ai.MiniMaxBitBoardStrategy;
import reversi.model.ai.RandomBitBoardStrategy;
import reversi.model.ai.Strategy;
import reversi.model.network.Client;
import reversi.model.network.NetworkReversi;

/**
 * Implementation of the controller interface which handles the interaction between the main-model-
 * and view-classes. Its main purpose is to check the validity of all incoming requests and to take
 * the appropriate actions afterwards.
 */
public class ReversiController implements Controller {

  private Client client;
  private Model model;
  private View view;

  private SwingWorker<Boolean, Void> moveWorker;
  SwingWorker<Void, Integer> animationWorker;

  private GatewayServer gatewayServer;
  private List<Py4JListener> listeners = new ArrayList<>();

  /**
   * Creates a new controller object.
   */
  public ReversiController() {
    view = new BasicView(this);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void start() {
    view.showAnimation();
    triggerAnimation();
    view.showView();
    startGateway();
  }

  private void startGateway() {
    this.gatewayServer = new GatewayServer(this);
    this.gatewayServer.start();
  }

  /**
   * Registers a new listener for Py4J.
   *
   * @param listener The new listener.
   */
  public void registerListener(Py4JListener listener) {
    listeners.add(listener);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void suggestName(String name) {
    if (client == null) {
      throw new IllegalStateException(
          "Tried to change the name of the client although there was no client running");
    }
    client.suggestName(name);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void createOnlineGame(Player player) {
    if (client == null) {
      throw new IllegalStateException(
          "Tried to create an online game although there was no client running");
    }
    model = client.createGame(player);
    view.showGame(model);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void joinOnlineGame(String creatorName) {
    if (client == null) {
      throw new IllegalStateException(
          "Tried to join an online game although there was no client running");
    }
    model = client.joinGame(creatorName);
    view.showGame(model);
  }

  private void triggerAnimation() {
    if (animationWorker != null) {
      throw new IllegalStateException("There is already an animation running");
    }
    animationWorker =
        new SwingWorker<>() {
          @Override
          protected Void doInBackground() throws Exception {
            for (int step = 1; step <= 6; step++) {
              Thread.sleep(WAITING_TIME);
              view.setAnimationStep(step);
              publish(step);
            }
            Thread.sleep(WAITING_TIME);
            showStartView();
            return null;
          }

          @Override
          protected void process(List<Integer> tasks) {
            for (int step : tasks) {
              view.drawAnimation(step);
            }
          }

          @Override
          protected void done() {
            animationWorker = null;
          }
        };
    animationWorker.execute();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void resetGame() {
    model.newGame();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void quitNetworkGame() {
    if (client == null) {
      throw new IllegalStateException(
          "Tried to quit a network game although there was no client running");
    }
    client.quitGame();
    showGameLobby();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void showStartView() {
    view.removeAnimation();
    leaveCurrentGame();
    closeClient();
    view.removeGame();
    view.removeLobby();
    view.showStartMenu();
  }

  private void closeClient() {
    if (client != null) {
      try {
        client.close();
        client = null;
      } catch (IOException e) {
        showError("Leaving network game failed. The following error occurred: " + e.getMessage());
      }
    }
  }

  private void leaveCurrentGame() {
    if (model != null) {
      model = null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void showGameLobby() {
    if (client == null) {
      throw new IllegalStateException(
          "Tried to show the game lobby although there was no client running");
    }
    view.removeGame();
    leaveCurrentGame();
    view.removeLobby();
    view.showLobby(client);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void hotseatGameSelected() {
    model = new BitBoardReversi();
    view.showGame(model);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void aiGameSelected(String difficulty) {
    Strategy strategy;
    switch (difficulty) {
      case Controller.BEGINNER_AI:
        strategy = new RandomBitBoardStrategy();
        break;
      case Controller.TRAINEE_AI:
        view.showInformationMessage(
            "Attention",
            "<html>You chose the trainee-AI, which uses reinforcement learning.<br>"
                + "It only works when run_model.py is running in the background.<br>"
                + "For further information please refer to the README-file.</html>");
        strategy = new DeepQlearningStrategy(listeners);
        break;
      case Controller.ADVANCED_AI:
        strategy = new MiniMaxBitBoardStrategy(4, true);
        break;
      case Controller.EXPERT_AI:
        strategy = new AlphaBetaBitBoardStrategy();
        break;
      default:
        throw new AssertionError("Unhandled difficulty: " + difficulty);
    }
    model = new BitBoardAI(strategy);
    view.showGame(model);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void networkGameSelected(String userName, InetAddress serverAddress) {
    client = new Client(userName, serverAddress);
    try {
      client.start();
      view.showLobby(client);
    } catch (IOException e) {
      showError("Creating network game failed. The following error occurred: " + e.getMessage());
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean place(Cell cell) {
    if (moveWorker != null) {
      showError("Move is already in progress");
      return false;
    }
    if (!GameField.isWithinBounds(cell)) {
      showError("Cell is out of bounds: " + cell);
      return false;
    }

    // SwingWorker is designed to be executed only once. Executing it more often will
    // not result in invoking the doInBackground method for another time.
    // Due to this, we are using null references here in order to check if there are
    // any pending moves.
    moveWorker =
        new SwingWorker<>() {
          @Override
          protected Boolean doInBackground() {
            if (model instanceof NetworkReversi) {
              if (client == null) {
                throw new IllegalStateException("Playing network game without a client");
              }
              client.place(cell);
              return true;
            }
            return model.place(cell);
          }

          @Override
          protected void done() {
            moveWorker = null;
            try {
              if (!get()) {
                showError("Failed to execute the move");
              }
            } catch (InterruptedException e) {
              // We should not have to wait in the first place
              throw new IllegalStateException(e.getMessage(), e);
            } catch (ExecutionException e) {
              throw new RuntimeException(e.getMessage(), e);
            } catch (CancellationException e) {
              // moveWorker got interrupted, probably intentionally by the user.
              // --> do nothing here
            }
          }
        };
    moveWorker.execute();

    return true;
  }

  private void showError(String message) {
    view.showErrorMessage(message);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void cancelAnimation() {
    if (animationWorker != null) {
      // cancel the animation
      animationWorker.cancel(true);
    }
    showStartView();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void dispose() {
    if (moveWorker != null) {
      // cancel the pending move
      moveWorker.cancel(true);
    }
    if (animationWorker != null) {
      // cancel the animation
      animationWorker.cancel(true);
    }
    this.gatewayServer.shutdown();
  }
}
