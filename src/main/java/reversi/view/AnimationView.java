package reversi.view;

import static java.util.Objects.requireNonNull;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.AbstractAction;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import reversi.model.GameField;
import reversi.model.Player;

/** View class that is responsible for showing a custom reversi animation. */
public class AnimationView extends JPanel {

  private static final long serialVersionUID = -1055776067900461523L;

  /** The share that the field padding makes up of the draw board width at least. */
  private static final double FIELD_PADDING_SHARE = 1 / 20.0;
  /** The share that the cell padding makes up of the cell width. */
  private static final double CELL_PADDING_SHARE = 1 / 8.0;

  private static final Color BACKGROUND_COLOR = Color.DARK_GRAY;
  private static final Color CELL_EDGE_COLOR = Color.WHITE;
  private static final Color WHITE_STONE_COLOR = Color.WHITE;
  private static final Color BLACK_STONE_COLOR = Color.BLACK;

  static long WAITING_TIME = 500;

  private static String CANCEL_ANIMATION = "cancelAnimation";

  private Controller controller;
  private int currentStep = 0;

  private int cellSize;
  private int horizontalOffset;
  private int verticalOffset;

  /**
   * Creates a new animation view which immediately starts the animation.
   *
   * @param controller The controller that will be notified when the animation is cancelled.
   */
  AnimationView(Controller controller) {
    this.controller = requireNonNull(controller);
    setBackground(BACKGROUND_COLOR);
    configureActionListeners();
  }

  private void configureActionListeners() {
    addComponentListener(
        new ComponentAdapter() {
          @Override
          public void componentResized(ComponentEvent e) {
            repaint();
          }
        });
    getInputMap().put(KeyStroke.getKeyStroke("ENTER"), CANCEL_ANIMATION);
    getInputMap().put(KeyStroke.getKeyStroke("SPACE"), CANCEL_ANIMATION);
    getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), CANCEL_ANIMATION);
    getInputMap().put(KeyStroke.getKeyStroke("Q"), CANCEL_ANIMATION);
    getActionMap().put(CANCEL_ANIMATION, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        controller.cancelAnimation();
      }
    });
  }

  /**
   * Sets the current step of the animation to the given step.
   *
   * @param step The new current step.
   */
  void setCurrentStep(int step) {
    this.currentStep = step;
  }

  @Override
  protected void paintComponent(Graphics g) {
    Graphics2D g2D = (Graphics2D) g;
    // optional rendering options that are used for preference when drawing the image-icons
    g2D.setRenderingHint(
        RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

    // paint the background
    g.setColor(BACKGROUND_COLOR);
    g.fillRect(0, 0, getWidth(), getHeight());

    updateMeasurements();

    int step = 1;
    while (step <= currentStep) {
      drawAnimationStep(step);
      step++;
    }
  }

  /**
   * Draws the given step of the animation.
   *
   * @param step The step to draw.
   */
  void drawAnimationStep(int step) {
    Graphics g = getGraphics();

    switch (step) {
      case 1:
        g.setColor(CELL_EDGE_COLOR);
        g.drawLine(
            horizontalOffset + 4 * cellSize,
            verticalOffset + 3 * cellSize,
            horizontalOffset + 4 * cellSize,
            verticalOffset + 5 * cellSize);
        break;
      case 2:
        g.setColor(CELL_EDGE_COLOR);
        g.drawLine(
            horizontalOffset + 3 * cellSize,
            verticalOffset + 4 * cellSize,
            horizontalOffset + 5 * cellSize,
            verticalOffset + 4 * cellSize);
        break;
      case 3:
        drawStone(
            Player.BLACK,
            g,
            (int) (cellSize * CELL_PADDING_SHARE),
            horizontalOffset + 3 * cellSize,
            verticalOffset + 3 * cellSize);
        break;
      case 4:
        drawStone(
            Player.WHITE,
            g,
            (int) (cellSize * CELL_PADDING_SHARE),
            horizontalOffset + 4 * cellSize,
            verticalOffset + 3 * cellSize);
        break;
      case 5:
        drawStone(
            Player.BLACK,
            g,
            (int) (cellSize * CELL_PADDING_SHARE),
            horizontalOffset + 4 * cellSize,
            verticalOffset + 4 * cellSize);
        break;
      case 6:
        drawStone(
            Player.WHITE,
            g,
            (int) (cellSize * CELL_PADDING_SHARE),
            horizontalOffset + 3 * cellSize,
            verticalOffset + 4 * cellSize);
        break;
      default:
        throw new AssertionError("There is only the steps 1 to 6.");
    }
  }

  /** Updates the measurements according to the current size of the animation view. */
  private void updateMeasurements() {
    int centerWidth = Math.min(getWidth(), getHeight());
    double fieldPadding = centerWidth * FIELD_PADDING_SHARE;
    int fieldWidth = (int) (centerWidth - 2 * fieldPadding);
    cellSize = fieldWidth / GameField.SIZE;
    horizontalOffset = (getWidth() - fieldWidth) / 2;
    verticalOffset = (getHeight() - fieldWidth) / 2;
  }

  /**
   * Draws a stone on the current selected cell.
   *
   * @param player The player that owns the cell.
   * @param g The {@link Graphics} object that allows to draw on the board.
   * @param padding Used to determine the gap-size between the stone and the border of the cell.
   * @param x The coordinate marking the left point of the cell.
   * @param y The coordinate marking the upper point of the cell.
   */
  private void drawStone(Player player, Graphics g, int padding, int x, int y) {
    switch (player) {
      case WHITE:
        g.setColor(WHITE_STONE_COLOR);
        break;
      case BLACK:
        g.setColor(BLACK_STONE_COLOR);
        break;
      default:
        throw new RuntimeException("Unhandled player: " + player);
    }
    g.fillOval(x + padding, y + padding, cellSize - 2 * padding, cellSize - 2 * padding);
  }
}
