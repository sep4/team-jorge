package reversi.view;

import static java.util.Objects.requireNonNull;

import static server.model.NetworkConstants.DISCONNECT;
import static server.model.NetworkConstants.JOIN_REJECTED;
import static server.model.NetworkConstants.LOST_CONNECTION;
import static server.model.NetworkConstants.NAME_REJECTED;
import static server.model.NetworkConstants.QUIT;
import static server.model.NetworkConstants.UPDATE;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import reversi.model.Player;
import reversi.model.network.Client;

/**
 * The view of the game lobby when the user is playing in network mode. It provides and connects all
 * graphical elements that are necessary for displaying the game lobby. More specifically, it allows
 * the user to see which player are currently in the lobby and which open games there are, that one
 * could join. It also provides an adequate possibility for creating a new game and for quitting the
 * lobby to get back to the start menu.
 */
public class LobbyView extends JPanel implements PropertyChangeListener {

  private static final long serialVersionUID = -929251882012137345L;

  private Client client;
  private Controller controller;

  private JLabel infoLabel;
  private JLabel playerList;
  private JLabel captionLabel;
  private JPanel gamePanel;
  private GridBagConstraints constraints;
  private JPanel quitPanel;
  private JButton createButton;
  private JButton quitButton;

  /**
   * Creates a view with all elements necessary for displaying the game lobby.
   *
   * @param client The client the lobby will be shown to.
   */
  LobbyView(Client client, Controller controller) {
    this.client = requireNonNull(client);
    this.controller = requireNonNull(controller);

    initializeWidgets();
    createView();
    updateLobby();

    client.subscribe(this);
  }

  private void initializeWidgets() {
    int fontsize = 14;

    infoLabel = new JLabel();
    infoLabel.setForeground(Color.BLACK);
    infoLabel.setFont(new Font(infoLabel.getFont().getFontName(), Font.PLAIN, fontsize));

    playerList = new JLabel();

    captionLabel = new JLabel();

    gamePanel = new JPanel();
    gamePanel.setLayout(new GridBagLayout());

    constraints = new GridBagConstraints();
    constraints.gridx = 0;
    constraints.ipadx = 30;
    constraints.ipady = 14;
    constraints.insets = new Insets(25, 10, 10, 10);

    quitPanel = new JPanel(new FlowLayout());

    createButton = new JButton("Create a new game");
    createButton.addActionListener(e -> handleCreateButtonClick());
    createButton.setEnabled(true);

    quitButton = new JButton("Back to the menu");
    quitButton.addActionListener(e -> handleQuitButtonClick());
    quitButton.setEnabled(true);
  }

  private void handleCreateButtonClick() {
    String[] playerOptions = {"Black", "White"};
    int chosenPlayer =
        JOptionPane.showOptionDialog(
            this,
            "What color do you want to play? It's Black's turn first.",
            "Choose your color",
            JOptionPane.DEFAULT_OPTION,
            JOptionPane.PLAIN_MESSAGE,
            null,
            playerOptions,
            null);
    switch (chosenPlayer) {
      case 0: // user chose Black
        controller.createOnlineGame(Player.BLACK);
        break;
      case 1: // user chose White
        controller.createOnlineGame(Player.WHITE);
        break;
      default:
        // do nothing if the user does something else
    }
  }

  private void handleQuitButtonClick() {
    int result =
        JOptionPane.showConfirmDialog(
            this,
            "Do you really want to return to the start menu?",
            "Please confirm your choice",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE);

    // do nothing if user didn't click on the 'yes'-option
    if (result == JOptionPane.YES_OPTION) {
      dispose();
      controller.showStartView();
    }
  }

  private void createView() {
    setLayout(new BorderLayout());
    JPanel infoPanel = new JPanel(new FlowLayout());
    infoPanel.add(infoLabel);
    add(infoPanel, BorderLayout.NORTH);

    GridBagConstraints c = new GridBagConstraints();
    c.anchor = GridBagConstraints.CENTER;
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1.0f;
    c.weighty = 1.0f;

    JPanel playerPanel = new JPanel();
    playerPanel.setLayout(new GridBagLayout());
    playerPanel.setBackground(Color.LIGHT_GRAY);
    playerPanel.add(playerList, constraints);

    JPanel sidePanel = new JPanel();
    sidePanel.setLayout(new GridBagLayout());
    sidePanel.add(playerPanel, c);
    add(sidePanel, BorderLayout.WEST);

    JPanel centerPanel = new JPanel();
    centerPanel.setLayout(new GridBagLayout());
    centerPanel.add(gamePanel, c);
    add(centerPanel, BorderLayout.CENTER);

    quitPanel.add(quitButton);
    quitPanel.add(createButton);
    add(quitPanel, BorderLayout.SOUTH);
  }

  @Override
  public void propertyChange(PropertyChangeEvent event) {
    SwingUtilities.invokeLater(() -> handleChangeEvent(event));
  }

  private void handleChangeEvent(PropertyChangeEvent event) {
    switch (event.getPropertyName()) {
      case NAME_REJECTED:
        JOptionPane.showMessageDialog(
            this,
            "<html>The username you chose is already in use."
                + "<br>Please choose a different one.</html>",
            "Username rejected",
            JOptionPane.ERROR_MESSAGE);
        String username = StartView.askForUsername(this);
        // return to the start menu if the user didn't enter anything
        if (username == null) {
          dispose();
          controller.showStartView();
        } else {
          controller.suggestName(username);
        }
        break;
      case UPDATE:
        updateLobby();
        break;
      case JOIN_REJECTED:
        JOptionPane.showMessageDialog(
            this,
            "Joining this game wasn't possible",
            "Join not possibele",
            JOptionPane.ERROR_MESSAGE);
        break;
      case QUIT:
        JOptionPane.showMessageDialog(
            this,
            "Your opponent headed back to the lobby",
            "Opponent left game",
            JOptionPane.INFORMATION_MESSAGE);
        controller.showGameLobby();
        break;
      case DISCONNECT:
        JOptionPane.showMessageDialog(
            this,
            "Your opponent got disconnected from the server",
            "Opponent disconnected",
            JOptionPane.INFORMATION_MESSAGE);
        controller.showGameLobby();
        break;
      case LOST_CONNECTION:
        JOptionPane.showMessageDialog(
            this,
            "The connection to the server got lost",
            "Lost connection",
            JOptionPane.INFORMATION_MESSAGE);
        controller.showStartView();
        break;
      default:
        // do nothing
    }
  }

  private void updateLobby() {
    updateToApproval();
    ArrayList<String> lobby = client.getLobby();
    StringBuilder connectedPlayers =
        new StringBuilder("<html>Users currently<br>in the lobby:<br>");
    if (lobby != null) {
      for (String name : lobby) {
        connectedPlayers.append("<br>").append(name);
      }
    }
    connectedPlayers.append("</html>");
    playerList.setText(connectedPlayers.toString());

    gamePanel.removeAll();
    gamePanel.add(captionLabel, constraints);
    captionLabel.setText("There are no open games at the moment");
    HashMap<String, Player> openGames = client.getOpenGames();
    if (openGames.size() > 0) {
      captionLabel.setText("Waiting players:");
      for (Map.Entry<String, Player> openGame : openGames.entrySet()) {
        String creator = openGame.getKey();
        Player creatorPlayer = openGame.getValue();
        JButton button = new JButton(creator + " as " + creatorPlayer);
        gamePanel.add(button, constraints);
        button.addActionListener(e -> controller.joinOnlineGame(creator));
      }
    }
    this.repaint();
  }

  private void updateToApproval() {
    if (client.isApproved()) {
      infoLabel.setText("Hello " + client.getUsername() + ", welcome to the Reversi-Lobby!");
      playerList.setVisible(true);
      gamePanel.setVisible(true);
      createButton.setVisible(true);
    } else {
      infoLabel.setText("Connecting to the server ...");
      playerList.setVisible(false);
      gamePanel.setVisible(false);
      createButton.setVisible(false);
    }
  }

  /**
   * Disposes any remaining resources.
   */
  void dispose() {
    client.unsubscribe(this);
  }
}
