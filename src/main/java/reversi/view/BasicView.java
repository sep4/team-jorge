package reversi.view;

import static java.util.Objects.requireNonNull;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import reversi.model.Model;
import reversi.model.network.Client;

/**
 * The main-view class. Consists of a card-layout, such that depending on the user input the
 * requested frame can be simply exchanged by a new one.
 */
public class BasicView extends JFrame implements View {

  private static final long serialVersionUID = 5754931946258656786L;

  private static final int MINIMUM_FRAME_HEIGHT = 400;
  private static final int MINIMUM_FRAME_WIDTH = 400;

  private static final String START_VIEW = "Start View";
  private static final String LOBBY_VIEW = "Lobby View";
  private static final String GAME_VIEW = "Game View";
  private static final String ANIMATION_VIEW = "Animation View";

  private final Controller controller;

  private final StartView startView;
  private LobbyView lobbyView;
  private ReversiView reversiView;
  private AnimationView animationView;

  private Container contentPane;
  private CardLayout cardLayout;

  /**
   * Creates a basic view which is easily able to switch between concrete views, according to the
   * situation.
   *
   * @param controller The {@link Controller} that validates and forwards any user input.
   */
  BasicView(Controller controller) {
    super("Reversi");
    this.controller = requireNonNull(controller);

    setMinimumSize(new Dimension(MINIMUM_FRAME_WIDTH, MINIMUM_FRAME_HEIGHT));
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    startView = new StartView(controller);
    pack();

    animationView = new AnimationView(controller);
    pack();

    initializeCardPanel();
  }

  private void initializeCardPanel() {
    setBackground(Color.LIGHT_GRAY);

    cardLayout = new CardLayout();
    setLayout(cardLayout);

    contentPane = getContentPane();
    contentPane.add(startView, START_VIEW);
    contentPane.add(animationView, ANIMATION_VIEW);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void showView() {
    setLocationByPlatform(true);
    pack();
    setVisible(true);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void showAnimation() {
    cardLayout.show(contentPane, ANIMATION_VIEW);
    pack();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeAnimation() {
    if (animationView != null) {
      contentPane.remove(animationView);
      animationView = null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void showStartMenu() {
    cardLayout.show(contentPane, START_VIEW);
    pack();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void showLobby(Client client) {
    lobbyView = new LobbyView(client, controller);
    contentPane.add(lobbyView, LOBBY_VIEW);

    cardLayout.show(getContentPane(), LOBBY_VIEW);
    pack();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void showGame(Model model) {
    reversiView = new ReversiView(model, controller);
    contentPane.add(reversiView, GAME_VIEW);

    cardLayout.show(getContentPane(), GAME_VIEW);
    pack();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeLobby() {
    if (lobbyView != null) {
      contentPane.remove(lobbyView);
      lobbyView.dispose();
      lobbyView = null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeGame() {
    if (reversiView != null) {
      contentPane.remove(reversiView);
      reversiView.dispose();
      reversiView = null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setAnimationStep(int step) {
    if (animationView == null) {
      throw new IllegalStateException("There is currently no animation running");
    }
    animationView.setCurrentStep(step);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void drawAnimation(int step) {
    if (animationView == null) {
      throw new IllegalStateException("There is currently no animation running");
    }
    animationView.drawAnimationStep(step);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void dispose() {
    removeLobby();
    removeAnimation();
    removeGame();
    controller.dispose();
    super.dispose();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void showErrorMessage(String message) {
    if (reversiView != null) {
      reversiView.showErrorMessage(message);
    } else {
      JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void showInformationMessage(String header, String message) {
    JOptionPane.showMessageDialog(this, message, header, JOptionPane.INFORMATION_MESSAGE);
  }
}
