package reversi.view;

import static java.util.Objects.requireNonNull;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import java.util.Optional;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import reversi.model.GameState;
import reversi.model.Model;
import reversi.model.Phase;
import reversi.model.Player;
import reversi.model.network.NetworkReversi;

/**
 * The main view of the graphical user interface. It provides and connects all graphical elements
 * that are necessary for playing a reversi game. More specifically, it allows two players to
 * alternately place stones on the reversi board, and provides an adequate possibility for resetting
 * the game at any given time.
 */
public class ReversiView extends JPanel implements PropertyChangeListener {

  private static final long serialVersionUID = 4873000664215587511L;

  private static final int FONTSIZE = 14;
  private static final int ICON_WIDTH = 60;
  private static final int ICON_HEIGHT = 80;

  private static final Color INFO_TEXT_COLOR = Color.BLACK;
  private static final Color ERROR_TEXT_COLOR = Color.RED;

  private final DrawBoard drawBoard;

  private Model model;
  private Controller controller;

  private JPanel resetPanel;
  private JCheckBox hintsCheckBox;
  private JButton resetButton;
  private JButton quitGameButton;
  private JLabel infoLabel;
  private JLabel errorLabel;

  /**
   * Creates a view with all elements necessary for playing an interactive reversi game.
   *
   * @param model The {@link Model} that handles the logic of the game.
   * @param controller The {@link Controller} that validates and forwards any user input.
   */
  ReversiView(Model model, Controller controller) {
    this.model = requireNonNull(model);
    this.controller = requireNonNull(controller);

    drawBoard = new DrawBoard(model, controller);

    initializeWidgets();
    createView();
    updateToPhase();

    model.addPropertyChangeListener(this);
  }

  private void initializeWidgets() {
    infoLabel = new JLabel();
    infoLabel.setForeground(INFO_TEXT_COLOR);
    infoLabel.setFont(new Font(infoLabel.getFont().getFontName(), Font.PLAIN, FONTSIZE));

    resetPanel = new JPanel(new FlowLayout());

    hintsCheckBox = new JCheckBox("Show hints");
    hintsCheckBox.setToolTipText(
        "Selecting will highlight the cells that the current player can place a stone on");
    hintsCheckBox.addActionListener(new ReversiViewListener());
    hintsCheckBox.setEnabled(true);

    quitGameButton = new JButton("Quit");
    quitGameButton.setToolTipText("Return to the start menu");
    quitGameButton.addActionListener(new ReversiViewListener());
    quitGameButton.setEnabled(true);

    resetButton = new JButton("Reset");
    resetButton.setToolTipText("Reset the current game");
    resetButton.addActionListener(new ReversiViewListener());
    resetButton.setEnabled(true);

    errorLabel = new JLabel();
    errorLabel.setForeground(ERROR_TEXT_COLOR);
    errorLabel.setFont(new Font(errorLabel.getFont().getFontName(), Font.PLAIN, FONTSIZE));

    updatePlayerTextInDisplay();
  }

  private void createView() {
    setLayout(new BorderLayout());
    JPanel panel = new JPanel(new FlowLayout());
    panel.add(infoLabel);
    add(panel, BorderLayout.NORTH);

    add(drawBoard, BorderLayout.CENTER);

    resetPanel.add(hintsCheckBox);
    resetPanel.add(quitGameButton);

    if (! (model instanceof NetworkReversi)) {
      resetPanel.add(resetButton);
    }
    resetPanel.add(errorLabel);
    add(resetPanel, BorderLayout.SOUTH);
  }

  /**
   * Updates the label text that informs the user about the current player and the number of stones
   * each player currently has placed on the game field.
   */
  private void updatePlayerTextInDisplay() {
    GameState state = model.getState();
    setInfoLabelText(
        "Current player: "
            + state.getCurrentPlayer()
            + " --- "
            + "# White stones: "
            + state.getAllCellsOfPlayer(Player.WHITE).size()
            + ", # Black Stones: "
            + state.getAllCellsOfPlayer(Player.BLACK).size());
  }

  /**
   * Displays a message in the view.
   *
   * @param message The message to be displayed.
   */
  private void setInfoLabelText(String message) {
    infoLabel.setText(message);
  }

  /**
   * Displays an error message in the view.
   *
   * @param message The message to be displayed.
   */
  void showErrorMessage(String message) {
    errorLabel.setText(message);
  }

  /**
   * Hides an error message that was previously displayed to the user.
   */
  private void hideErrorMessage() {
    errorLabel.setText("");
  }

  @Override
  public void propertyChange(PropertyChangeEvent event) {
    SwingUtilities.invokeLater(() -> handleChangeEvent(event));
  }

  /**
   * Reacts when the observable (= model) has just published that it has changed its state. The GUI
   * needs to be updated accordingly here.
   *
   * @param event The event that has been fired by the model.
   */
  private void handleChangeEvent(PropertyChangeEvent event) {
    if (event.getPropertyName().equals(Model.STATE_CHANGED)) {
      updatePlayerTextInDisplay();
      updateToPhase();
    } else if (event.getPropertyName().equals(Model.GAME_ENDED)) {
      showEndOfGameDialog();
    }
    hideErrorMessage();
  }

  private void updateToPhase() {
    if (model.getState().getCurrentPhase() == Phase.WAITING) {
      drawBoard.setVisible(false);
      hintsCheckBox.setVisible(false);
      setInfoLabelText("Waiting for an opponent ...");
    } else {
      drawBoard.setVisible(true);
      hintsCheckBox.setVisible(true);
    }
  }

  /**
   * Shows a dialog with a respective message to the user when the game has ended. The message
   * contains the winner if the game didn't end with a tie.
   *
   * @throws IllegalStateException if the game hasn't ended.
   */
  private void showEndOfGameDialog() {
    if (model.getState().getCurrentPhase() != Phase.FINISHED) {
      throw new IllegalStateException(
          "The end-of-game-dialog can't be shown when the game isn't finished.");
    }

    Optional<Player> playerOpt = model.getState().getWinner();
    if (playerOpt.isPresent()) {
      Player player = playerOpt.get();
      setInfoLabelText("Player " + player + " has won. Click reset to start a new game.");
      showDialogWindow("Finished!", "There's a winner: Player " + player, true);
    } else {
      setInfoLabelText("Game ended as a tie. Click reset to start a new game.");
      showDialogWindow("Finished!", "Game is over. It's a tie!", false);
    }
  }

  /**
   * Shows a message pane that displays the outcome of the game.
   *
   * @param header The header text.
   * @param message An elaborate message why the game has ended.
   * @param trophyShown <code>true</code> to show a winner trophy, <code>false</code> otherwise.
   */
  private void showDialogWindow(String header, String message, boolean trophyShown) {
    Optional<ImageIcon> iconOpt =
        ResourceLoader.loadScaledImageIcon("winner_trophy.png", ICON_WIDTH, ICON_HEIGHT);
    JOptionPane.showMessageDialog(
        this,
        message,
        header,
        JOptionPane.INFORMATION_MESSAGE,
        iconOpt.isPresent() && trophyShown ? iconOpt.get() : null);
  }

  /**
   * Disposes any remaining resources.
   */
  void dispose() {
    model.removePropertyChangeListener(this);
    drawBoard.dispose();
  }

  private class ReversiViewListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      if (e.getSource() == resetButton) {
        handleResetButtonClick();
      } else if (e.getSource() == quitGameButton) {
        handleQuitGameButtonClick();
      } else if (e.getSource() == hintsCheckBox) {
        handleHintsCheckBoxClick();
      }
    }

    private void handleResetButtonClick() {
      int result =
          JOptionPane.showConfirmDialog(
              ReversiView.this,
              "Do you really want to start a new game?",
              "Please confirm your choice",
              JOptionPane.YES_NO_OPTION,
              JOptionPane.QUESTION_MESSAGE);

      // do nothing if user didn't click on the 'yes'-option
      if (result == JOptionPane.YES_OPTION) {
        controller.resetGame();
      }
    }

    private void handleQuitGameButtonClick() {
      int result =
          JOptionPane.showConfirmDialog(
              ReversiView.this,
              "Do you really want to abort this game?",
              "Please confirm your choice",
              JOptionPane.YES_NO_OPTION,
              JOptionPane.QUESTION_MESSAGE);

      // do nothing if user didn't click on the 'yes'-option
      if (result == JOptionPane.YES_OPTION) {
        if (model instanceof NetworkReversi) {
          controller.quitNetworkGame();
        } else {
          controller.showStartView();
        }
      }
    }

    private void handleHintsCheckBoxClick() {
      if (hintsCheckBox.isSelected()) {
        drawBoard.showPlacingHints();
      } else {
        drawBoard.removePlacingHints();
      }
    }
  }
}
