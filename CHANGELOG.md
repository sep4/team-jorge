# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Abgabe 1] - 2019-12-22

### Added
- Simple GUI for Hotseat Mode.

## [Abgabe 2] - 2020-01-12

### Added
- Single Player Mode.
- Two Difficulty Levels (Normal & Hard) in Single Player Mode.

## [Abgabe 3] - 2020-01-26

### Added
- Start animation which is cancelled upon pressing Enter, Space, Escape or Q.
- Two more Difficulty Levels (Beginner & Trainee) in Single Player Mode.
- Network Mode with access to the Reversi Lobby and the possibility to create and join games.
- Server application which provides a simple GUI and starts a Reversi Server needed for the network functionality.

### Changed
- New Names for the two old Difficulty Levels (Normal -> Advanced, Hard -> Expert).
