# Test Report

This document explains the various activities performed as part of the Testing of the Reversi Application.

### Testing Scope
* In Scope:  
  Functional testing for the entire Reversi package to ensure that the application meets the specifications
  of the project.
* Out of Scope

* Items not Tested  
  Playing a game through using an IP Address other than the localhost.
  
### Metrics

Branch coverage was used such that, for each branch in the program (e.g., if statements, loops), each branch was 
executed at least once during testing.
Cyclomatic complexity metric was also used as a guideline to create several test cases in order to cover all the 
possible paths in the code.

### Types of Testing Performed

+ Integration Testing  
Each time a new feature was added, integration tests were done to ensure that the
newly added feature worked well with the already existing modules.  

+ Unit testing  
Different test cases were written on the reversi-logic to ensure that the functions and modules worked
as intended. 

+ Manual testing  
  The GUI and network modules, as well as the rl-ai on the GUI, relied on manual testing to ensure easy navigation 
  throughout the application. 
  Several test cases were tried out by the members of the team. 
  
  + Example testing protocol for the Network Model:  
    + connecting without server: error message + return in StartView
    + player connects: connect works, player shown in lobby 
    + backToTheMenu button: returns to the
    random string as server address: error message and return to lobby
    + wrong length for name: error message + enter name again
    + player with existing name connects: error message + enter name again
    + while player with existing name is connecting third player connects: works as expected
    + create game for both colors: works
    + join game: works
    + legal moves: works
    + move with wrong color: not possible
    + show hints: shows moves only for current player
    + quit game: error message for other player, returns both player to the lobby
    + close window in game: returns other player to the lobby
    + close window in lobby: removes player from the lobby
    + finish game: show winner message for both and they hove to quit
    + close server while clients connected: returns players to the startview

### Results

This provides a [summary](https://gitlab.com/sep4/team-jorge/-/jobs/539800576/artifacts/file/build/reports/tests/test/index.html)
of the test report results for the reversi model & AI packages.